import Logger from './src/middleware/LogFile';
import { port } from './config';
import app from './src/app';

app.listen(port, () => {
    console.log(`Approval BE app listening at http://localhost:${port}`);
    Logger.info(`Approval BE app listening at http://localhost:${port}`)
  })
  .on('error', (e) => Logger.error(e));
