import express, { Request, Response, NextFunction } from 'express';
import { corsUrl, environment } from '../config';
import { db } from '../config';
import { Neogma } from 'neogma';

export const neogma = new Neogma(
    {
        url: db.db_url,
        username: db.db_user,
        password: db.db_pw,
    },
    {
        encrypted: true,
    },
);

import Logger from './middleware/LogFile';
import bodyParser from 'body-parser';
import cors from 'cors';
import { NotFoundError, ApiError, InternalError } from './middleware/ApiError';
import routes from './routes';
const helmet = require("helmet");

const app = express();
  
app.use(helmet());
app.use(bodyParser.json({ limit: '10mb' }));
app.use(bodyParser.urlencoded({ limit: '10mb', extended: true, parameterLimit: 50000 }));
app.use(cors({ origin: corsUrl, optionsSuccessStatus: 200 }));

app.use('/api', routes);

app.use((req, res, next) => next(new NotFoundError()));

app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
    if (err instanceof ApiError) {
      ApiError.handle(err, res);
    } else {
      if (environment === 'development') {
        Logger.error(err);
        return res.status(500).send(err.message);
      }
      ApiError.handle(new InternalError(), res);
    }
});
  
export default app;
