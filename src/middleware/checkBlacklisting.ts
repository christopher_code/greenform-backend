import { Tokens } from "../database/models/TokenBlacklist";
import GeneralService from "../database/entityService/GeneralService";

export async function checkIfOnBlacklist(token: string): Promise<boolean> {
    let node = await GeneralService.findNodeByToken(Tokens, token);
    if (node) {
        return true
    } else {
        return false;
    }
}