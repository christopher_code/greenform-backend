import { Response } from 'express';
import {
  InternalErrorResponse,
  BadRequestResponse,
  NotFoundResponse,
  AuthFailureResponse,
  ForbiddenResponse,
  NotAccetableResponse,
} from './ApiResponse';

enum ErrorType {
  BAD_REQUEST = 'BadRequestError',
  NOT_FOUND = 'NotFoundError',
  INTERNAL = 'InternalError',
  NO_ENTRY = 'NoEntryError',
  NO_DATA = 'NoDataError',
  NOT_ACCEPTABLE = 'NotAcceptableError',
  UNAUTHORIZED = 'AuthFailureError',
  BAD_TOKEN = 'BadTokenError',
  ALREADY_EXISTS = 'DataAlredayExistsError',
  FORBIDDEN = 'ForbiddenError',
}

export abstract class ApiError extends Error {
  constructor(public type: ErrorType, public message: string = 'error') {
    super(type);
  }

  public static handle(err: ApiError, res: Response): Response {
    switch (err.type) {
        case ErrorType.BAD_REQUEST:
            return new BadRequestResponse(err.message).send(res);
        case ErrorType.ALREADY_EXISTS:
        case ErrorType.NOT_ACCEPTABLE:
            return new NotAccetableResponse(err.message).send(res);
        case ErrorType.NO_ENTRY:
        case ErrorType.NO_DATA:
        case ErrorType.NOT_FOUND:
            return new NotFoundResponse(err.message).send(res);
        case ErrorType.INTERNAL:
            return new InternalErrorResponse(err.message).send(res);
        case ErrorType.UNAUTHORIZED:
        case ErrorType.BAD_TOKEN:
            return new AuthFailureResponse(err.message).send(res);
        case ErrorType.FORBIDDEN:
            return new ForbiddenResponse(err.message).send(res);
        default: {
            let message = err.message;
                return new InternalErrorResponse(message).send(res);
      }
    }
  }
}

export class BadRequestError extends ApiError {
    constructor(message = 'Bad Request') {
        super(ErrorType.BAD_REQUEST, message);
  }
}

export class NotFoundError extends ApiError {
    constructor(message = 'Not Found') {
        super(ErrorType.NOT_FOUND, message);
    }
}

export class InternalError extends ApiError {
    constructor(message = 'Internal error') {
      super(ErrorType.INTERNAL, message);
    }
  }

export class AuthFailureError extends ApiError {
    constructor(message = 'Invalid Credentials') {
      super(ErrorType.UNAUTHORIZED, message);
    }
}

export class BadTokenError extends ApiError {
    constructor(message = 'Token is not valid') {
      super(ErrorType.BAD_TOKEN, message);
    }
  }

export class NotAcceptableError extends ApiError {
    constructor(message = 'Provided data is not acceptable.') {
      super(ErrorType.NOT_ACCEPTABLE, message);
    }
  }

export class DataAlredayExistsError extends ApiError {
    constructor(message = 'Provided Data does already exists, please rename it.') {
      super(ErrorType.ALREADY_EXISTS, message);
    }
  }

export class ForbiddenError extends ApiError {
    constructor(message = 'Permission denied') {
      super(ErrorType.FORBIDDEN, message);
    }
  }