import  jwt from 'jsonwebtoken';
import { authToken } from '../../config';
import { ForbiddenError, BadRequestError } from "./ApiError";
import { checkIfOnBlacklist } from "./checkBlacklisting";

export default async function verifyToken(req:any, res:any, next:any): Promise<any> {
    try {
        let token = req.headers.authorization.split(' ')[1];
        if (!token)
          res.status(403).send('No token provided')
        
          let isOnBlacklist = await checkIfOnBlacklist(token);
          if(isOnBlacklist) 
            res.status(403).send('Token expired.')
            
        jwt.verify(token, authToken, function(err:any, decoded:any) {
            if (err)
                res.status(403).send('Failed to authentificate token')

            req.userMail = decoded.theUserMail;   
            next();
        });
    }  catch (error) {
        throw new BadRequestError();
    }
    
}