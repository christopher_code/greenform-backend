import { Request, NextFunction } from 'express';
import { BadRequestError } from './ApiError';
import { Users } from '../database/models/Users';
import GeneralService from "../database/entityService/GeneralService";
import { SourcePropertiesI, Source } from "../database/models/Source";

export async function checkUserVerifiesOnce(req: Request, next: NextFunction): Promise<boolean | any> {
    try {
        let userOriginalAdd: SourcePropertiesI | any = await GeneralService.findNodeAnyRelations(req.params.sourceId,
                                                                                Source, Users,
                                                                                'AddedBy',
                                                                                next);                                                                                                                                     
        let userApprovedFirst: SourcePropertiesI | any = await GeneralService.findNodeAnyRelations(req.params.sourceId,
                                                                                Source, Users,
                                                                                'ApprovedFirstBy',
                                                                                next);                                        
        let userApprovedFinal: SourcePropertiesI | any = await GeneralService.findNodeAnyRelations(req.params.sourceId,
                                                                                Source, Users,
                                                                                'ApprovedFinalBy',
                                                                                next);                                                                         
        let userApprovedNewUser: SourcePropertiesI | any = await GeneralService.findNodeAnyRelations(req.params.sourceId,
                                                                                Source, Users,
                                                                                'ApprovedByNewUser',
                                                                                next);    
        let userNotApprovedFirst: SourcePropertiesI | any = await GeneralService.findNodeAnyRelations(req.params.sourceId,
                                                                                Source, Users,
                                                                                'NotApprovedOnce',
                                                                                next);       
        let userNotApprovedFinal: SourcePropertiesI | any = await GeneralService.findNodeAnyRelations(req.params.sourceId,
                                                                                Source, Users,
                                                                                'NotApprovedTwice',
                                                                                next);   
        let userNotApprovedNewUser: SourcePropertiesI | any = await GeneralService.findNodeAnyRelations(req.params.sourceId,
                                                                                Source, Users,
                                                                                'NotApprovedNewUser',
                                                                                next);                                                                                                                                                                                                                                                                                                                                                                                                                                         
        if (userOriginalAdd[0] && userOriginalAdd[0].id === req.params.userId) {return false}
        else if(userApprovedFirst[0] && userApprovedFirst[0].id === req.params.userId) {return false}
        else if(userApprovedFinal[0] && userApprovedFinal[0].id === req.params.userId) {return false}
        else if(userApprovedNewUser[0] && userApprovedNewUser[0].id === req.params.userId) {return false}
        else if(userNotApprovedFirst[0] && userNotApprovedFirst[0].id === req.params.userId) {return false}
        else if(userNotApprovedFinal[0] && userNotApprovedFinal[0].id === req.params.userId) {return false}
        else if(userNotApprovedNewUser[0] && userNotApprovedNewUser[0].id === req.params.userId) {return false}
        else {
            return true
        }
    }
    catch (err) {
        throw new BadRequestError("Something went wrong");
    }  
}