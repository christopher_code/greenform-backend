import rateLimit from "express-rate-limit";

const rateLimitMiddleware = rateLimit({
    windowMs: 15 * 60 * 1000, 
    max: 30,
    message: 'You have exeeded the limit of requests!', 
    headers: true,
  });

export default rateLimitMiddleware;