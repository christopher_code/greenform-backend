const NodeRSA = require('node-rsa');
import { publicKey } from "../../config"

export let encryptData = async(data: any): Promise<any> => {
    const key = new NodeRSA({b: 512});
    let publicKeyImported = key.importKey(publicKey, 'public');
    const encrypted = publicKeyImported.encrypt(data, 'base64');

    return encrypted;
}