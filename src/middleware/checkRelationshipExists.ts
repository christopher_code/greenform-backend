import { NextFunction } from 'express';
import ProductService from "../database/entityService/ProductService";
import { totalGHGEmissions } from "../database/models/GHG/totalGHG";
import { Categories } from "../database/models/category"
import { Products } from "../database/models/Product"
import { BadRequestError } from "./ApiError";
import GeneralService from "../database/entityService/GeneralService";

export let checkRelationshipExists = async(productId: string, changeId: string, name: string, next: NextFunction): Promise<boolean> => {
    try {
        let resp: any = undefined;
        if (name === 'singleGHG') {
            resp = await ProductService.findRelationshipBetweenTwoGivenNodes(
                productId,
                changeId,
                'EmitsGHG')
        } else if (name === 'reasonGHG') {
            resp = await ProductService.findRelationshipBetweenTwoGivenNodes(
                productId,
                changeId, 
                'EmitsBecauseOf')
        } else if (name === 'alternative') {
            resp = await ProductService.findRelationshipBetweenTwoGivenNodes(
                productId,
                changeId,
                'HasAlternativeProduct')
        } else if (name === 'rawmaterial') {
            resp = await ProductService.findRelationshipBetweenTwoGivenNodes(
                productId,
                changeId,
                'MadeOfRawMaterial')
        } else if (name === 'seller') {
            resp = await ProductService.findRelationshipBetweenTwoGivenNodes(
                productId,
                changeId,
                'CanBeBoughtFrom')
        }  else if (name === 'totalGHG') {
            resp = await GeneralService.findNodeAnyRelations(
                productId,
                Products, totalGHGEmissions, 
                'EmitsInTotal', 
                next);
        } else if (name === 'category') {
            resp = await GeneralService.findNodeAnyRelations(
                productId,
                Products, Categories, 
                'BelongsToCategory', 
                next);
        } 
        if (Object.keys(resp).length === 0) {return false} else {return true}
    } catch (err) {
        throw new BadRequestError("Error while checking if relationship exists");
    }
}