import { Products } from "../database/models/Product";
import { singleGHGEmissions } from "../database/models/GHG/singleGHG";
import { ReasonOfGHGEmsission } from "../database/models/GHG/reasonOfGHG";
import GeneralService from "../database/entityService/GeneralService";
import { BadRequestError, InternalError } from "./ApiError";

export let checkSourceApproved = async(nodeId: string, name: string): Promise<boolean> => {
    try {
        let node: any;
        if (name === 'product' || name === 'alternative' || name === 'rawmaterial') {
            node = await GeneralService.findNodeById(Products, nodeId);
        } else if (name === 'singleGHG') {
            node = await GeneralService.findNodeById(singleGHGEmissions, nodeId);
        } else if (name === 'reasonGHG') {
            node = await GeneralService.findNodeById(ReasonOfGHGEmsission, nodeId);
        } else {
            throw new BadRequestError("Error while checking if source is approved.");
        }

        if (node.dataValues.approvalStatus === 'approvedFinal') {return true} else {return false}
    } catch (err) {
        throw new InternalError("Something went wrong.");
    }
}