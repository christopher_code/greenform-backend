import GeneralService from "../database/entityService/GeneralService";
import { Products } from "../database/models/Product"
import { ReasonOfGHGEmsission } from "../database/models/GHG/reasonOfGHG"
import { singleGHGEmissions } from "../database/models/GHG/singleGHG"
import { totalGHGEmissions } from "../database/models/GHG/totalGHG"
import { Sellers } from "../database/models/Seller"
import { BadRequestError, InternalError } from "./ApiError";

export let sourceRelationship = async(relType: string, relId: string): Promise<any> => {
    try {
        let node: any;
        if (relType === 'product' || relType === 'alternative' || relType === 'rawmaterial') {
            node = GeneralService.findNodeById(Products, relId);
        } else if (relType === 'reason') {
            node = GeneralService.findNodeById(ReasonOfGHGEmsission, relId);
        } else if (relType === 'single') {
            node = GeneralService.findNodeById(singleGHGEmissions, relId);
        } else if (relType === 'total') {
            node = GeneralService.findNodeById(totalGHGEmissions, relId);
        } else if (relType === 'seller') {
            node = GeneralService.findNodeById(Sellers, relId);
        } else {
            throw new BadRequestError("Please provid valid data.")
        }

        return node;
    } catch (err) {
        throw new InternalError("Something went wrong.");
    }
}