import { generateDate } from "./generateDate";
import { checkDate } from "./checkDate";
import { UsersPropertiesI } from "../database/models/Users";

export function checkIsSuperUser(user: UsersPropertiesI | any): boolean {
    let olderThanOneWeek = checkDate(user.createdAt, generateDate()); 
    if (olderThanOneWeek && user.approvals >= 5) {
        return true
    } else {
        return false;
    }
}