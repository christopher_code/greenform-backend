import { BadRequestError } from './ApiError';
import { Users } from '../database/models/Users';
import { Categories } from '../database/models/category';
import { Sellers } from '../database/models/Seller';
import { Products } from '../database/models/Product';

export async function checkUniqueData(input: string, model: string): Promise<boolean | any> {
    try {
        let approve: any;
        if (model === 'User') {
            approve = await Users.findOne({
                where: {
                    email: input
                },
            });
        } else if (model === 'Categories') {
            approve = await Categories.findOne({
                where: {
                    typeOfCategory: input
                },
            });
        } else if (model === 'Sellers') {
            approve = await Sellers.findOne({
                where: {
                    name: input
                },
            });
        } else if (model === 'Products') {
            approve = await Products.findOne({
                where: {
                    name: input
                },
            });
        }
        if (approve) {
            return true;
        } else {
            return false
        }
    }
    catch (err) {
        throw new BadRequestError('Data needs to be unique.');
    }  
}