import express from 'express';
import { addTokenToBlacklist } from "./service";
import { SuccessResponse } from '../../../middleware/ApiResponse';
import asyncHanlder from "../../../helpers/asyncFunc";

const router = express.Router();

router.post('/add/:token',
    asyncHanlder(async (req, res) => {
        await addTokenToBlacklist(req);
        new SuccessResponse('Token added to blacklist successfully.', {}).send(res);
    })
)

export default router;