import { Request } from 'express';
import { Tokens } from "../../../database/models/TokenBlacklist";
import GeneralService from "../../../database/entityService/GeneralService";
import { InternalError } from "../../../middleware/ApiError"

export let addTokenToBlacklist = async (req: Request): Promise<any> => {
    try {
        let input = {
            token: req.params.token
        }
        await GeneralService.createNode(Tokens, input);
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    };
};
