import { Request } from 'express';
import { InternalError } from '../../../middleware/ApiError';
import axios from "axios";
import {adminJWT} from "../../../../config";

export let deleteProductRelationshipSST = async (req: Request): Promise<boolean | any> => {
    try {
        await axios({
            method: 'delete',
            url: 'https://greenformedsstbe-jnzm36w3ca-ew.a.run.app/api/product/delete/rel/' + req.params.productId + "/" + req.params.otherId + "/" + req.params.relType,
            data: {},
            headers: {
                  'Authorization': `Bearer ${adminJWT}` 
            }
          })
        return true;
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};

export let deleteNodeSST = async (req: Request): Promise<boolean | any> => {
    try {
        await axios({
            method: 'delete',
            url: 'https://greenformedsstbe-jnzm36w3ca-ew.a.run.app/api/general/delete/node/' + req.params.modelId + '/' + req.params.model,
            data: {},
            headers: {
                  'Authorization': `Bearer ${adminJWT}` 
            }
          })
        return true;
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};
