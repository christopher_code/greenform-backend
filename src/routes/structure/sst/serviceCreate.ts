import { InternalError } from '../../../middleware/ApiError';
import axios from "axios";
import { adminJWT } from "../../../../config";
import { encryptData } from "../../../middleware/encryptData";

export let transferEncryptedTest = async (req: any): Promise<boolean | any> => {
    try {
        let data = await encryptData({
            id: req.body.id,
            text: req.body.text
        })

        await axios({
            method: 'post',
            url: 'https://greenformedsstbe-jnzm36w3ca-ew.a.run.app/api/test/encrypted',
            data: {data},
            headers: {
                  'Authorization': `Bearer ${adminJWT}` 
            }
          })
        return data;
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};

export let transferNewProductToSST = async (req: any, product: any): Promise<boolean | any> => {
    try {
        let input: any;
        if (req.body.categoryId) {
            input = {
                id: product.dataValues.id,
                name: product.dataValues.name,
                description: product.dataValues.description,
                linkToBuy: product.dataValues.linkToBuy,
                costPerProduct: product.dataValues.costPerProduct,
                createdAt: product.dataValues.createdAt,
                categoryId: req.body.categoryId,
                typeOfCategory: req.body.typeOfCategory,
                categoryCreatedAt: req.body.categoryCreatedAt,
            }
        } else {
            input = {
                id: product.dataValues.id,
                name: product.dataValues.name,
                description: product.dataValues.description,
                linkToBuy: product.dataValues.linkToBuy,
                costPerProduct: product.dataValues.costPerProduct,
                createdAt: product.dataValues.createdAt,
            }
        }
        await axios({
            method: 'post',
            url: 'https://greenformedsstbe-jnzm36w3ca-ew.a.run.app/api/product/transfer/first',
            data: input,
            headers: {
                  'Authorization': `Bearer ${adminJWT}` 
            }
          })
        return true;
    }
    catch (err) {
        throw new InternalError("Something went wrong!" + err);
    }  
};

export let transferNewSellerToSST = async (productId: string, seller: any): Promise<boolean | any> => {
    try {
        await axios({
            method: 'post',
            url: 'https://greenformedsstbe-jnzm36w3ca-ew.a.run.app/api/seller/transfer/first/relate/' + productId,
            data: {
                id: seller.dataValues.id,
                name: seller.dataValues.name,
                link: seller.dataValues.link,
                createdAt: seller.dataValues.createdAt,
            },
            headers: {
                  'Authorization': `Bearer ${adminJWT}` 
            }
          })
        return true;
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};

export let transferNewTotalGHGToSST = async (productId: string, total: any): Promise<boolean | any> => {
    try {
        await axios({
            method: 'post',
            url: 'https://greenformedsstbe-jnzm36w3ca-ew.a.run.app/api/ghg/total/transfer/first/relate/' + productId,
            data: {
                id: total.dataValues.id,
                ghgPerKG: total.dataValues.ghgPerKG,
                createdAt: total.dataValues.createdAt,
            },
            headers: {
                  'Authorization': `Bearer ${adminJWT}` 
            }
          })
        return true;
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};

export let transferNewSingleGHGToSST = async (productId: string, single: any): Promise<boolean | any> => {
    try {
        await axios({
            method: 'post',
            url: 'https://greenformedsstbe-jnzm36w3ca-ew.a.run.app/api/ghg/single/transfer/first/relate/' + productId,
            data: {
                id: single.dataValues.id,
                ghgName: single.dataValues.ghgName,
                emissionPerKG: single.dataValues.emissionPerKG,
                createdAt: single.dataValues.createdAt,
            },
            headers: {
                  'Authorization': `Bearer ${adminJWT}` 
            }
          })
        return true;
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};

export let transferNewReasonGHGToSST = async (productId: string, reason: any): Promise<boolean | any> => {
    try {
        await axios({
            method: 'post',
            url: 'https://greenformedsstbe-jnzm36w3ca-ew.a.run.app/api/ghg/reason/transfer/first/relate/' + productId,
            data: {
                id: reason.dataValues.id,
                ghgName: reason.dataValues.ghgName,
                reasonTitle: reason.dataValues.reasonTitle,
                description: reason.dataValues.description,
                createdAt: reason.dataValues.createdAt,
            },
            headers: {
                  'Authorization': `Bearer ${adminJWT}` 
            }
          })
        return true;
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};