import { Request, Response, NextFunction } from 'express';
import GeneralService from "../../../database/entityService/GeneralService";
import { Products } from "../../../database/models/Product";
import { Source } from "../../../database/models/Source";
import { Sellers } from "../../../database/models/Seller";
import { BadRequestError, InternalError } from '../../../middleware/ApiError';
import axios from "axios";
import {adminJWT} from "../../../../config";
import { totalGHGEmissions } from '../../../database/models/GHG/totalGHG';
import { singleGHGEmissions } from '../../../database/models/GHG/singleGHG';
import { ReasonOfGHGEmsission } from '../../../database/models/GHG/reasonOfGHG';

export let updateFieldsInSST = async (req: Request, res: Response, next: NextFunction): Promise<boolean | any> => {
    try {
        let data: any;
        let sourceProperties: any;
        if (req.params.model === 'product') {
            sourceProperties = await GeneralService.findNodeAnyRelationsWithProperties(req.params.productId, undefined, Products, Source, 'HasUpdateSource', next);
            if (!sourceProperties) {throw new BadRequestError("Source Properties not found.");}
            if (sourceProperties[0].properties.ChangedField === 'description') {data={description: sourceProperties[0].properties.NewValue}}
            else if (sourceProperties[0].properties.ChangedField === 'linkToBuy') {data={linkToBuy: sourceProperties[0].properties.NewValue}}
            else if (sourceProperties[0].properties.ChangedField === 'costPerProduct') {data={costPerProduct: sourceProperties[0].properties.NewValue}}

            await axios({method: 'put', url: 'https://greenformedsstbe-jnzm36w3ca-ew.a.run.app/api/general/update/fields/' + req.params.productId + '/product', data: data,
            headers: {
                  'Authorization': `Bearer ${adminJWT}` 
            }})
            return true;
        } else if (req.params.model === 'seller') {
            sourceProperties = await GeneralService.findNodeAnyRelationsWithProperties(req.params.productId, undefined, Sellers, Source, 'HasUpdateSource', next);
            if (!sourceProperties) {throw new BadRequestError("Source Properties not found.");}
            await axios({method: 'put', url: 'https://greenformedsstbe-jnzm36w3ca-ew.a.run.app/api/general/update/fields/' + req.params.productId + '/seller', data: {link: sourceProperties[0].properties.NewValue},
            headers: {
                  'Authorization': `Bearer ${adminJWT}` 
            }})
        } else if (req.params.model === 'total') {
            sourceProperties = await GeneralService.findNodeAnyRelationsWithProperties(req.params.productId,  undefined, totalGHGEmissions, Source, 'HasUpdateSource', next);
            if (!sourceProperties) {throw new BadRequestError("Source Properties not found.");}
            await axios({method: 'put', url: 'https://greenformedsstbe-jnzm36w3ca-ew.a.run.app/api/general/update/fields/' + req.params.productId + '/total', data: {ghgPerKG: sourceProperties[0].properties.NewValue},
            headers: {
                  'Authorization': `Bearer ${adminJWT}` 
            }})
        } else if (req.params.model === 'single') {
            sourceProperties = await GeneralService.findNodeAnyRelationsWithProperties(req.params.productId,  undefined, singleGHGEmissions, Source, 'HasUpdateSource', next);
            if (!sourceProperties) {throw new BadRequestError("Source Properties not found.");}
            await axios({method: 'put', url: 'https://greenformedsstbe-jnzm36w3ca-ew.a.run.app/api/general/update/fields/' + req.params.productId + '/single', data: {emissionPerKG: sourceProperties[0].properties.NewValue},
            headers: {
                  'Authorization': `Bearer ${adminJWT}` 
            }})
        } else if (req.params.model === 'reason') {
            sourceProperties = await GeneralService.findNodeAnyRelationsWithProperties(req.params.productId, undefined, ReasonOfGHGEmsission, Source, 'HasUpdateSource', next);
            if (!sourceProperties) {throw new BadRequestError("Source Properties not found.");}
            await axios({method: 'put', url: 'https://greenformedsstbe-jnzm36w3ca-ew.a.run.app/api/general/update/fields/' + req.params.productId + '/reason', data: {description: sourceProperties[0].properties.NewValue},
            headers: {
                  'Authorization': `Bearer ${adminJWT}` 
            }})
        }
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};

export let addRelationsInSST = async (productId: string, changeId: string, name: string): Promise<boolean | any> => {
    try {
        await axios({method: 'put', url: 'https://greenformedsstbe-jnzm36w3ca-ew.a.run.app/api/product/update/relationship/' + productId + '/' + changeId + '/' + name,
            headers: {
                'Authorization': `Bearer ${adminJWT}` 
        }})
        return true
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};