import express from 'express';
import { transferEncryptedTest } from "./serviceCreate";
import { SuccessResponse } from '../../../middleware/ApiResponse';
import asyncHandler from "../../../helpers/asyncFunc";;

const router = express.Router();

router.post('/test', 
    asyncHandler(async (req, res) => {
        let data = await transferEncryptedTest(req);
        new SuccessResponse('Single GHG change history received successfuly', {
            data: data
        }).send(res);
        }
    )
)

export default router;