import { Request, Response, NextFunction } from 'express';
import { BadRequestError } from '../../../middleware/ApiError';
import { transferNewProductToSST,
         transferNewSellerToSST,
         transferNewTotalGHGToSST,
         transferNewSingleGHGToSST,
         transferNewReasonGHGToSST,
        } from "./serviceCreate";
import { updateFieldsInSST } from "./serviceUpdate";

export let handleNewTransfer = async (model: string, productId: string, node: any, req: Request): Promise<any> => {
    try {
        let transfer: boolean;
        if (model === 'product') {
            transfer = await transferNewProductToSST(req, node);
            if (transfer) {return transfer};
        } else if (model === 'seller') {
            transfer = await transferNewSellerToSST(productId, node);
            if (transfer) {return transfer};
        } else if (model === 'total') {
            transfer = await transferNewTotalGHGToSST(productId, node);
            if (transfer) {return transfer};
        } else if (model === 'single') {
            transfer = await transferNewSingleGHGToSST(productId, node);
            if (transfer) {return transfer};
        } else if (model === 'reason') {
            transfer = await transferNewReasonGHGToSST(productId, node);
            if (transfer) {return transfer};
        }
    }
    catch (err) {
        throw new BadRequestError(err);
    }  
};

export let handleUpdateSST = async (req: Request, res: Response, next: NextFunction): Promise<any> => {
    try {
        await updateFieldsInSST(req, res, next);
        return true;
    }
    catch (err) {
        throw new BadRequestError(err);
    }  
};
