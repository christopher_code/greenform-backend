import Joi from '@hapi/joi';

export default {
  updateStatus: Joi.object().keys({
    description: Joi.string().required().min(5),
    approval: Joi.boolean().required(),
  }),
};