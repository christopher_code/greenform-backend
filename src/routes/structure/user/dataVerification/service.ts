import { Request, Response, NextFunction } from 'express';
import { BadRequestError, InternalError } from '../../../../middleware/ApiError';
import { generateDate } from "../../../../middleware/generateDate";
import { Users, UsersPropertiesI } from "../../../../database/models/Users";
import { Products } from "../../../../database/models/Product";
import { Sellers } from "../../../../database/models/Seller";
import { SourcePropertiesI, Source } from "../../../../database/models/Source";
import DataVerificationService from "../../../../database/entityService/DataVerificationService";
import { totalGHGEmissions } from '../../../../database/models/GHG/totalGHG';
import { singleGHGEmissions } from '../../../../database/models/GHG/singleGHG';
import { ReasonOfGHGEmsission } from '../../../../database/models/GHG/reasonOfGHG';
import { handleNewTransfer, handleUpdateSST } from "../../sst/controller";
import { deleteNodeSST } from '../../sst/serviceDelete';
import { updateSourceStatus } from "../../source/service";
import GeneralService from "../../../../database/entityService/GeneralService";

export let approveSourceOfModelOfInterest = async (req:Request, res:Response, next: NextFunction): Promise<SourcePropertiesI | any> => {
    try {
        let source:SourcePropertiesI | any = await GeneralService.findNodeById(Source, req.params.sourceId);
        let approvalStatus = await DataVerificationService.updateStatus(req, source);

        //find the node of interest
        let modelOfInterest = await findModelOfInterest(req);
        if (!modelOfInterest) {throw new BadRequestError("Data not found.");}

        // if update source + approved -> writes changes in SST
        if (source.sourceType === 'update' && approvalStatus[1] === 'approvedFinal') {
            await handleUpdateSST(req, res, next)
        }

        //trigger deletion of data if source is approved
        if (source.sourceType === 'delete') {await deletionAfterApproval(req, next, approvalStatus)}

        // update user status 
        if (approvalStatus[1] === 'notApprovedAgain') {await updateUserStatus('declined', req, next)}
        if (approvalStatus[1] === 'approvedFinal') {await updateUserStatus('approved', req, next)}

        //trigger transfer of data to sst 
        if (approvalStatus[1] === 'approvedFinal' && source.sourceType === 'new') {
            let transfer: boolean = await handleNewTransfer(req.params.model, req.params.productId, modelOfInterest, req);
            if (!transfer) {throw new BadRequestError("Transfer failed.")}
        }

        // relationship node with approval source and model of interst is created
        await GeneralService.relateNodesModel(Source, approvalStatus[0], req.params.sourceId, req.params.userId, {
            Date: generateDate(),
            Description: req.body.description,
            ApprovalStatus: req.body.approval,
        });

        //approval status transferred to model of interest
        if (source.sourceType === 'new') {
            modelOfInterest.approvalStatus = approvalStatus[1];
            await modelOfInterest.save();
        }

        await updateSourceStatus(req.params.sourceId, approvalStatus[1]);
        let updatedSource = await GeneralService.findNodeById(Source, req.params.sourceId);

        return updatedSource;
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};

let updateUserStatus = async (result: string, req:Request, next: NextFunction): Promise<any> => {
    try {
        let newUserApproved: SourcePropertiesI | any = await GeneralService.findNodeAnyRelations(req.params.sourceId, Source, Users, 'ApprovedByNewUser', next); 
        if (!newUserApproved) {throw new BadRequestError("Source not found.");}
        let newUserNotApproved: SourcePropertiesI | any = await GeneralService.findNodeAnyRelations(req.params.sourceId, Source, Users, 'NotApprovedNewUser', next); 
        if (!newUserNotApproved) {throw new BadRequestError("Source not found.");}  
        if (newUserApproved[0] || newUserNotApproved[0]) {
            if (result === 'approved') {
                if (newUserApproved[0]) {
                    let newUser:UsersPropertiesI | any = await GeneralService.findNodeById(Users, newUserApproved[0].id);
                    newUser.approvals = newUser.approvals+1;
                    await newUser.save();
                } else if (newUserNotApproved[0]) {
                    let newUser:UsersPropertiesI | any = await GeneralService.findNodeById(Users, newUserNotApproved[0].id);
                    newUser.approvals = newUser.approvals-1;
                    await newUser.save();
                }
            } else if (result === 'declined') {
                if (newUserApproved[0]) {
                    let newUser:UsersPropertiesI | any = await GeneralService.findNodeById(Users, newUserApproved[0].id);
                    newUser.approvals = newUser.approvals-1;
                    await newUser.save();
                } else if (newUserNotApproved[0]) {
                    let newUser:UsersPropertiesI | any = await GeneralService.findNodeById(Users, newUserNotApproved[0].id);
                    newUser.approvals = newUser.approvals+1;
                    await newUser.save();
                }
            }
        }                                                                                     
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};

let findModelOfInterest = async (req:Request): Promise<any> => {
    try {
        let modelOfInterest:any;
        
        if (req.params.model === "product") {
            modelOfInterest = await await GeneralService.findNodeById(Products, req.params.modelId);
        } else if (req.params.model === "total") {
            modelOfInterest = await await GeneralService.findNodeById(totalGHGEmissions, req.params.modelId);
        } else if (req.params.model === "single") {
            modelOfInterest = await GeneralService.findNodeById(singleGHGEmissions, req.params.modelId);
        } else if (req.params.model === "reason") {
            modelOfInterest = await GeneralService.findNodeById(ReasonOfGHGEmsission, req.params.modelId);
        } else if (req.params.model === "seller") {
            modelOfInterest = await GeneralService.findNodeById(Sellers, req.params.modelId);
        }    
        return modelOfInterest;                                                                
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};

let deletionAfterApproval = async (req:Request, next: NextFunction, approvalStatus: string[]): Promise<any> => {
    try {
        if (req.params.model === "product" && approvalStatus[1] === 'approvedFinal') {
            await GeneralService.deleteNode(Products ,req.params.modelId, next);
            await deleteNodeSST(req);
            return 'Approved Deletion of Product and triggered it in both BE'
        } else if (req.params.model === "seller" && approvalStatus[1] === 'approvedFinal') {
            await GeneralService.deleteNode(Sellers, req.params.modelId, next);
            await deleteNodeSST(req);
            return 'Approved Deletion of Seller and triggered it in both BE' 
        } else if (req.params.model === "total" && approvalStatus[1] === 'approvedFinal') {
            await GeneralService.deleteNode(totalGHGEmissions, req.params.modelId, next);
            await deleteNodeSST(req);
            return 'Approved Deletion of Total GHG Data and triggered it in both BE'
        } else if (req.params.model === "single" && approvalStatus[1] === 'approvedFinal') {
            await GeneralService.deleteNode(singleGHGEmissions, req.params.modelId, next);
            await deleteNodeSST(req);
            return 'Approved Deletion of Single GHG Data and triggered it in both BE'
        } else if (req.params.model === "reason" && approvalStatus[1] === 'approvedFinal') {
            await GeneralService.deleteNode(ReasonOfGHGEmsission, req.params.modelId, next);
            await deleteNodeSST(req);
            return 'Approved Deletion of Reason of GHG Emission and triggered it in both BE'
        }                                                               
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};