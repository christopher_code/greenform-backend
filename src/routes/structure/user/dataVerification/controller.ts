import express from 'express';
import { approveSourceOfModelOfInterest } from "./service";
import { SuccessResponse } from '../../../../middleware/ApiResponse';
import asyncHandler from "../../../../helpers/asyncFunc";
import { checkUserVerifiesOnce } from "../../../../middleware/checkUserVerifiesOnce";
import validator from '../../../../helpers/validator';
import schema from "./schema";
import { BadRequestError } from "../../../../middleware/ApiError";
import {SourcePropertiesI} from "../../../../database/models/Source";

const router = express.Router();

router.put('/:userId/:modelId/:sourceId/:userStatus/:model/:productId/0', 
    validator(schema.updateStatus),
    asyncHandler(async (req, res, next) => {
        const hasNotVerifiedYet: boolean = await checkUserVerifiesOnce(req, next);
        if (!hasNotVerifiedYet) throw new BadRequestError('Same user is not allowed to verify source again or own source.');
        let source:SourcePropertiesI | any = await approveSourceOfModelOfInterest(req, res, next);
        new SuccessResponse('Source updated successfuly', {
            source: source
        }).send(res);
    }
)
)

export default router;