import express from 'express';
import bcrypt from "bcrypt";
import jwt from 'jsonwebtoken';
import validator from '../../../../helpers/validator';
import schema from './schema';
import { SuccessResponse } from '../../../../middleware/ApiResponse';
import { BadRequestError } from '../../../../middleware/ApiError';
import asyncHandler from "../../../../helpers/asyncFunc";
import { checkIsSuperUser } from "../../../../middleware/checkIsSuperUser";
import { convertToSuperUser } from "./service";
import GeneralService from "../../../../database/entityService/GeneralService";
import { authToken } from '../../../../../config';
import { Users } from '../../../../database/models/Users';

const router = express.Router();

router.post('/user',
    validator(schema.login),
    asyncHandler(async (req, res) => {
        const user = await GeneralService.findNodeByEmail(Users, req.body.email);

        if (!user.password) throw new BadRequestError('Data not complete');

        const match = await bcrypt.compare(req.body.password, user.password);
        if (!match) return res.status(401).send("Login failed due to wrong email or password!");

        const token = jwt.sign(
            { theUserMail: user.email },
            authToken,
            { expiresIn: '6h' });

        if (user.roleStatus === 'user') {
            if (checkIsSuperUser(user)) {
                convertToSuperUser(user);
            }
        }
        
        new SuccessResponse('Login Success', {
            id: user.id,
            token: token,
            role: user.roleStatus,
        }).send(res);
    })
)

export default router;
        