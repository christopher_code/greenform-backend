import Joi from '@hapi/joi';

export default {
  login: Joi.object().keys({
    email: Joi.string().email().required().min(5),
    password: Joi.string().required().min(8),
  }),
  signup: Joi.object().keys({
    email: Joi.string().email().required(),
    name: Joi.string().required().min(3),
    company: Joi.string().required().min(2),
    password: Joi.string().required().min(8),
  }),
};
