import { Request } from 'express';
import { Users, RoleStauts } from "../../../../database/models/Users";
import { generateUniqeuId } from "../../../../middleware/generateUuid";
import { BadRequestError, InternalError } from '../../../../middleware/ApiError';
import { generateDate } from "../../../../middleware/generateDate";
import { UsersPropertiesI } from "../../../../database/models/Users";
import bcrypt from "bcrypt";
import GeneralService from "../../../../database/entityService/GeneralService";

export const saveUser = async (req:Request): Promise<UsersPropertiesI | any> => {
    const saltRounds = 12;
    let passwordHash:string;

    try {
        passwordHash = await bcrypt.hash(req.body.password, saltRounds);
    } 
    catch (err) {
        throw new InternalError("Something went wrong!");
    }

    try {
        let input = {id: generateUniqeuId(),
            email: req.body.email,
            name: req.body.name,
            company: req.body.company,
            password: passwordHash,
            roleStatus: RoleStauts.NEW_USER,
            approvals: 0,
            createdAt: generateDate()}

        let user = await GeneralService.createNode(Users, input);

        return user;
    }
    catch (err) {
        throw new BadRequestError('Creation of user failed.');
    }   
};

export const convertToSuperUser = async (user: any): Promise<void> => {
    try {
        user.roleStatus = 'superuser'
        user.save();
    } catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};