import express from 'express';
import validator from '../../../../helpers/validator';
import schema from './schema';
import { SuccessResponse } from '../../../../middleware/ApiResponse';
import { saveUser } from "./service";
import asyncHandler from "../../../../helpers/asyncFunc";
import { checkUniqueData } from "../../../../middleware/checkUniqueData";
import { DataAlredayExistsError } from "../../../../middleware/ApiError";

const router = express.Router();

router.post('/user',
    validator(schema.signup),
    asyncHandler(async (req, res) => {
        const isNotUnique: boolean = await checkUniqueData(req.body.email ,"User");
        if (isNotUnique) throw new DataAlredayExistsError('User with this email does already exists.')
        const user = await saveUser(req);
        new SuccessResponse('Signup Success', {
            user: user,
            }).send(res);
    })
)

export default router;