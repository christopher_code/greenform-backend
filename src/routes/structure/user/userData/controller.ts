import express from 'express';
import { SuccessResponse } from '../../../../middleware/ApiResponse';
import { getUserData } from "./service";
import asyncHandler from "../../../../helpers/asyncFunc";

const router = express.Router();

router.get('/data/:id',
    asyncHandler(async (req, res) => {
        const user = await getUserData(req.params.id);
        new SuccessResponse('Source saved with success', {
            name: user.name,
            email: user.email,
            role: user.roleStatus,
            createdAt: user.createdAt,
            company: user.company,
            id: user.id,
            approvals: user.approvals,
            }).send(res);
    })
)

export default router;