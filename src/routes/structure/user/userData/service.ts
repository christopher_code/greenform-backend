import { BadRequestError, InternalError } from '../../../../middleware/ApiError';
import { UsersPropertiesI, Users } from "../../../../database/models/Users";
import GeneralService from "../../../../database/entityService/GeneralService"

export let getUserData = async (userId: string): Promise<UsersPropertiesI | any> => {
    try {
        let user:UsersPropertiesI | any = await GeneralService.findNodeById(Users, userId);
        if (!user) {throw new BadRequestError("User not found.");}
        return user;
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};