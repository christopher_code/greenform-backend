import express from 'express';
import { totalGHGEmissions } from "../../../../database/models/GHG/totalGHG";
import { singleGHGEmissions } from "../../../../database/models/GHG/singleGHG";
import { ReasonOfGHGEmsission } from "../../../../database/models/GHG/reasonOfGHG";
import { createProduct, updateProductRelationships, updateProductFields, requestProductDeletion } from "./service";
import { SuccessResponse } from '../../../../middleware/ApiResponse';
import asyncHandler from "../../../../helpers/asyncFunc";
import validator from '../../../../helpers/validator';
import schema from "./schema";
import { checkUniqueData } from "../../../../middleware/checkUniqueData";
import { BadRequestError, DataAlredayExistsError } from "../../../../middleware/ApiError";
import ProductService from "../../../../database/entityService/ProductService";
import { Sellers } from '../../../../database/models/Seller';
import { Products, ProductPropertiesI } from '../../../../database/models/Product';
import { deleteProductRelationshipSST } from '../../sst/serviceDelete';
import { checkRelationshipExists } from "../../../../middleware/checkRelationshipExists";
import { checkSourceApproved } from "../../../../middleware/checkSourceApproved";
import { addRelationsInSST } from "../../sst/serviceUpdate";
import GeneralService from "../../../../database/entityService/GeneralService"

const router = express.Router();

router.post('/new/:userId',
    validator(schema.createNew),
    asyncHandler(async (req, res) => {
        const isNotUnique: boolean = await checkUniqueData(req.body.name ,"Products");
        if (isNotUnique) throw new DataAlredayExistsError('Product does already exists, please rename it or edit existing product.')
        const product = await createProduct(req.params.userId, req);
        new SuccessResponse('Product saved with success', {
            id: product.id,
            product: product,
            }).send(res);
        }
    )
)

router.put('/update/fields/:productId/:userId', 
    validator(schema.update),
    asyncHandler(async (req, res) => {
        let originalProduct: ProductPropertiesI | any = await GeneralService.findNodeById(Products, req.params.productId);
        let product: ProductPropertiesI | any = await updateProductFields(req.params.productId, originalProduct, req);
        new SuccessResponse('Product updated successfuly', {
            product: product
        }).send(res);
    }
)
)

router.put('/update/first/relationship/:name/:productId/:changeId', 
    asyncHandler(async (req, res, next) => {
        let exists = await checkRelationshipExists(req.params.productId, req.params.changeId, req.params.name, next);
        if (exists) {throw new BadRequestError("Relationship already exists")};

        let changeName: string = req.params.name;
        await updateProductRelationships(req, changeName);

        new SuccessResponse('Product updated successfuly', {}).send(res);
        }
    )
)

router.put('/update/relationship/:name/:productId/:changeId', 
    asyncHandler(async (req, res, next) => {
        let exists = await checkRelationshipExists(req.params.productId, req.params.changeId, req.params.name, next);
        if (exists) {throw new BadRequestError("Relationship already exists")};

        let productApproved = await checkSourceApproved(req.params.productId, "product");
        let relApproved = await checkSourceApproved(req.params.changeId, req.params.name);
        if (productApproved === false || relApproved === false) {throw new BadRequestError("You can only related approved data.")}

        let changeName: string = req.params.name;
        await updateProductRelationships(req, changeName);

        await addRelationsInSST(req.params.productId, req.params.changeId, req.params.name);

        new SuccessResponse('Product updated successfuly', {}).send(res);
        }
    )
)

router.put('/request/deletion/:productId/:userId', 
    validator(schema.requestDeletion),
    asyncHandler(async (req, res) => {
        await requestProductDeletion(req);
        new SuccessResponse('Product request to delte was successful!', {}).send(res);
        }
    )
)

router.delete('/delete/rel/:productId/:otherId/:relType', 
    asyncHandler(async (req, res) => {
        let alias: string = ''
        let model: any;
        if (req.params.relType === 'seller') {alias = 'CanBeBoughtFrom'; model = Sellers}
        else if (req.params.relType === 'reason') {alias = 'EmitsBecauseOf'; model = ReasonOfGHGEmsission}
        else if (req.params.relType === 'single') {alias = 'EmitsGHG'; model = singleGHGEmissions;}
        else if (req.params.relType === 'total') {alias = 'EmitsInTotal'; model = totalGHGEmissions}
        else if (req.params.relType === 'alternative') {alias = 'HasAlternativeProduct'; model = Products}
        await ProductService.deleteProductRelationship(alias, model, req);
        await deleteProductRelationshipSST(req);
        new SuccessResponse('Deletion of relationship was successful!', {}).send(res);
        }
    )
)

export default router;
