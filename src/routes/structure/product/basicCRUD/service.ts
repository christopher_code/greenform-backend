import { Request, NextFunction } from 'express';
import { Products, ProductPropertiesI } from "../../../../database/models/Product";
import { Source, ApprovalStatus } from "../../../../database/models/Source";
import { Users } from "../../../../database/models/Users";
import { totalGHGEmissions } from "../../../../database/models/GHG/totalGHG";
import { singleGHGEmissions } from "../../../../database/models/GHG/singleGHG";
import { ReasonOfGHGEmsission } from "../../../../database/models/GHG/reasonOfGHG";
import { generateUniqeuId } from "../../../../middleware/generateUuid";
import { BadRequestError, InternalError } from '../../../../middleware/ApiError';
import { generateDate } from "../../../../middleware/generateDate";
import { Categories } from "../../../../database/models/category";
import { Sellers } from "../../../../database/models/Seller";
import SourceService from "../../../../database/entityService/SourceService";
import GeneralService from "../../../../database/entityService/GeneralService";

export let createProduct = async (userId:string, req:Request): Promise<ProductPropertiesI> => {
    try {
        let id:string = generateUniqeuId();
        let source: any = await SourceService.saveSource(req, 'new','product', id, id);
        if (!source) {throw new BadRequestError("Source not saved.");}

        let input = {id: id,
            name: req.body.name,
            description: req.body.description,
            linkToBuy: req.body.linkToBuy,
            costPerProduct: req.body.costPerProduct,
            createdAt: generateDate(),
            approvalStatus: ApprovalStatus.NEW_SOURCE,
            sourceId: source.id,
            HasSource: {where: {params: {id: source.id}}},
            AddedBy: {where: {params: {id: userId}}}}

        let product:ProductPropertiesI = await GeneralService.createNode(Products, input);
        if (!product) {throw new BadRequestError("Product not saved.");}
        
        return product;
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};

export let updateProductFields = async (productId: string, fetchedProduct: any, req:Request): Promise<ProductPropertiesI | any> => {
    try {
        let source: any;
        let changedField: string = '';
        let chnagedValue: any = '';
        let originalValue: any = '';
        if (req.body.sourceTitle) {
            source = await SourceService.saveSource(req, 'update', 'product', productId, productId);
            if (!source) {throw new BadRequestError("Source not saved.");}
        }
        let product:ProductPropertiesI | any = await GeneralService.findNodeById(Products, productId);
        if (!product) {throw new BadRequestError("Product not found.");}
        if (req.body.name) {
            product.name = req.body.name;
            changedField = 'name';
            chnagedValue = req.body.name;
            originalValue = fetchedProduct.dataValues.name;
        } else if (req.body.costPerProduct) {
            product.costPerProduct = req.body.costPerProduct;
            changedField = 'costPerProduct';
            chnagedValue = req.body.costPerProduct;
            originalValue = fetchedProduct.dataValues.costPerProduct;
        } else if (req.body.description) {
            product.description = req.body.description;
            changedField = 'description';
            chnagedValue = req.body.description;
            originalValue = fetchedProduct.dataValues.description;
        } else if (req.body.linkToBuy) {
            product.linkToBuy = req.body.linkToBuy;
            changedField = 'linkToBuy';
            chnagedValue = req.body.linkToBuy;
            originalValue = fetchedProduct.dataValues.linkToBuy;
        }  
        if (req.body.sourceTitle) {

            await GeneralService.relateNodes(product, "HasUpdateSource", source.id, {
                ChangedField: changedField,
                    OriginalValue: originalValue,
                    NewValue: chnagedValue,
                    Date: generateDate(),
                    UserId: req.params.userId,
            });
        }
        await product.save();
        return product
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};

export let updateProductRelationships = async (req:Request, changeName: string): Promise<ProductPropertiesI | any> => {
    try {
        let product:ProductPropertiesI | any = await GeneralService.findNodeById(Products, req.params.productId);
        if (!product) {throw new BadRequestError("Product not found.");}
        if (changeName === "seller") {
            await GeneralService.relateNodes(product, "CanBeBoughtFrom", req.params.changeId, undefined);
        } else if (changeName === "category") {
            await GeneralService.relateNodes(product, "BelongsToCategory", req.params.changeId, undefined);
        } else if (changeName === "totalGHG") {
            await GeneralService.relateNodes(product, "EmitsInTotal", req.params.changeId, undefined);
        } else if (changeName  === "singleGHG") {
            await GeneralService.relateNodes(product, "EmitsGHG", req.params.changeId, undefined);
        } else if (changeName === "reasonGHG") {
            await GeneralService.relateNodes(product, "EmitsBecauseOf", req.params.changeId, undefined);
        } else if (changeName === "alternative") {
            await GeneralService.relateNodes(product, "HasAlternativeProduct", req.params.changeId, undefined);
        } else if (changeName === "rawmaterial") {
            await GeneralService.relateNodes(product, "MadeOfRawMaterial", req.params.changeId, undefined);
        } 
        return "success"
    }
    catch (err) {
        throw new InternalError("Something went wrong!" + err);
    }  
};

export let getProductCard = async (next:NextFunction, productId: string): Promise<{} | any> => {
    try {
        let product:ProductPropertiesI | any = await GeneralService.findNodeById(Products, productId);
        if (!product) {throw new BadRequestError("Product not found.");}
        let productAlternative: any = await GeneralService.findNodeAnyRelations(productId,
                                                                                Products, Products,
                                                                                'HasAlternativeProduct',
                                                                                next
                                                                            );                                                                                                                                                                                                                                                                          
        let productCategory: any = await GeneralService.findNodeAnyRelations(productId,
                                                                                Products, Categories,
                                                                                'BelongsToCategory',
                                                                                next
                                                                            );   
        let productTotalEmission: any = await GeneralService.findNodeAnyRelations(productId,
                                                                                Products, totalGHGEmissions,
                                                                                'EmitsInTotal',
                                                                                next
                                                                            );
        let productSingleEmission: any = await GeneralService.findNodeAnyRelations(productId,
                                                                                Products, singleGHGEmissions,
                                                                                'EmitsGHG',
                                                                                next
                                                                            ); 
        let productEmissionReason: any = await GeneralService.findNodeAnyRelations(productId,
                                                                                Products, ReasonOfGHGEmsission,
                                                                                'EmitsBecauseOf',
                                                                                next
                                                                            );    
        let addedBy: any = await GeneralService.findNodeAnyRelations(productId,
                                                                                Products, Users,
                                                                                'AddedBy',
                                                                                next
                                                                            );                                                                                                                                                                                                                                                                                                                                    
        let response: object = {
            product,
            productAlternative,
            productCategory,
            productTotalEmission,
            productSingleEmission,
            productEmissionReason,
            addedBy,
        }
        return response
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};


export let getFullProduct = async (req:Request, next:NextFunction): Promise<{} | any> => {
    try {
        let product:ProductPropertiesI | any = await GeneralService.findNodeById(Products, req.params.productId);
        if (!product) {throw new BadRequestError("Product not found.");}
        let productSource: any = await GeneralService.findNodeAnyRelations(req.params.productId, Products,
                                                                                Source,
                                                                                'HasSource',
                                                                                next
                                                                            );
        let productSeller: any = await GeneralService.findNodeAnyRelations(req.params.productId,Products,
                                                                                Sellers,
                                                                                'CanBeBoughtFrom',
                                                                                next
                                                                            );   
        let productAlternative: any = await GeneralService.findNodeAnyRelations(req.params.productId,Products,
                                                                                Products,
                                                                                'HasAlternativeProduct',
                                                                                next
                                                                            );     
        let productMadeOfRawMaterial: any = await GeneralService.findNodeAnyRelations(req.params.productId, Products,
                                                                                Products,
                                                                                'MadeOfRawMaterial',
                                                                                next
                                                                            );  
        let productAddedBy: any = await GeneralService.findNodeAnyRelations(req.params.productId, Products,
                                                                                Users,
                                                                                'AddedBy',
                                                                                next
                                                                            );                                                                                                                                                                                                                                                                                                                                                                                      
        let productCategory: any = await GeneralService.findNodeAnyRelations(req.params.productId,Products,
                                                                                Categories,
                                                                                'BelongsToCategory',
                                                                                next
                                                                            );    
        let productTotalEmission: any = await GeneralService.findNodeAnyRelations(req.params.productId,Products,
                                                                                totalGHGEmissions,
                                                                                'EmitsInTotal',
                                                                                next
                                                                            );
        let productSingleEmission: any = await GeneralService.findNodeAnyRelations(req.params.productId,Products,
                                                                                singleGHGEmissions,
                                                                                'EmitsGHG',
                                                                                next
                                                                            ); 
        let productEmissionReason: any = await GeneralService.findNodeAnyRelations(req.params.productId,Products,
                                                                                ReasonOfGHGEmsission,
                                                                                'EmitsBecauseOf',
                                                                                next
                                                                            );                                                                                                                                                                                                                                                                   
        let responseObj: object = {
            product,
            productSource,
            productSeller,
            productAddedBy,
            productAlternative,
            productMadeOfRawMaterial,
            productCategory,
            productTotalEmission,
            productSingleEmission,
            productEmissionReason,
        }
        return responseObj
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};

export let getProductSources = async (req:Request, next:NextFunction): Promise<{} | any> => {
    try {
        let product:ProductPropertiesI | any = await GeneralService.findNodeById(Products, req.params.productId);
        if (!product) {throw new BadRequestError("Product not found.");}
        let productSource: any = await GeneralService.findNodeAnyRelations(req.params.productId, Products,
                                                                                Source,
                                                                                'HasSource',
                                                                                next
                                                                            );   
        let productUpdateSource: any = await GeneralService.findNodeAnyRelationsWithProperties(req.params.productId,
                                                                                req.params.sourceId, Products,
                                                                                Source,
                                                                                'HasUpdateSource',
                                                                                next
                                                                            );  
        let productDeletionSource: any = await GeneralService.findNodeAnyRelationsWithProperties(req.params.productId,
                                                                                req.params.sourceId, Products,
                                                                                Source,
                                                                                'HasRequestDeletionSource',
                                                                                next
                                                                            );                                                                                                                                                                                                                                                                                                                                                                                                       
        let responseObj: object = {
            product,
            productSource,
            productUpdateSource,
            productDeletionSource,
        }
        return responseObj
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};

export let requestProductDeletion = async (req:Request): Promise<ProductPropertiesI | any> => {
    try {
        let product:ProductPropertiesI | any = await GeneralService.findNodeById(Products, req.params.productId);
        if (!product) {throw new BadRequestError("No product found")}
        let source: any = await SourceService.saveSource(req, 'delete', 'product', req.params.productId, req.params.productId);
        if (!source) {throw new BadRequestError("No source found")}
        
        await GeneralService.relateNodes(product, "HasRequestDeletionSource", source.id, {
                Date: generateDate(),
                UserId: req.params.userId,
                Name: product.dataValues.name,
                Description: product.dataValues.description,
                LinkToBuy: product.dataValues.linkToBuy,
                CreatedAt: product.dataValues.createdAt,
                approvalStatus: product.dataValues.approvalStatus,
                originalId: product.dataValues.id,
        });

        return product;
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};
