import express from 'express';
import { getFullProduct, getProductCard, getProductSources } from "./service";
import { Products } from "../../../../database/models/Product";
import { SuccessResponse } from '../../../../middleware/ApiResponse';
import { BadRequestError } from '../../../../middleware/ApiError';
import asyncHandler from "../../../../helpers/asyncFunc";
import { ProductPropertiesI } from "../../../../database/models/Product";
import { SourcePropertiesI } from "../../../../database/models/Source";
import GeneralService from "../../../../database/entityService/GeneralService";

const router = express.Router();

router.get('/get/core/:productId', 
    asyncHandler(async (req, res, next) => {
        let repsonse:ProductPropertiesI | any = await getProductCard(next, req.params.productId);
        new SuccessResponse('Product Core received successfuly', {
            product: repsonse.product.dataValues,
            altternativeProduct: repsonse.productAlternative,
            catgeory: repsonse.productCategory,
            totalEmission: repsonse.productTotalEmission,
            singleEmission: repsonse.productSingleEmission,
            emissionReason: repsonse.productEmissionReason,
        }).send(res);
        }
    )
);

router.get('/get/full/:productId', 
    asyncHandler(async (req, res, next) => {
        let responseObj: ProductPropertiesI | any = await getFullProduct(req, next);
        new SuccessResponse('Full Product received successfuly', {
            product: responseObj.product.dataValues,
            source: responseObj.productSource,
            addedBy: [{
                createdAt: responseObj.productAddedBy[0].createdAt,
                name: responseObj.productAddedBy[0].name,
                company: responseObj.productAddedBy[0].company,
                id: responseObj.productAddedBy[0].id,
                email: responseObj.productAddedBy[0].email,
                roleStatus: responseObj.productAddedBy[0].roleStatus
            }],
            seller: responseObj.productSeller,
            altternativeProduct: responseObj.productAlternative,
            productMadeOfRawMaterial: responseObj.productMadeOfRawMaterial,
            catgeory: responseObj.productCategory,
            totalEmission: responseObj.productTotalEmission,
            singleEmission: responseObj.productSingleEmission,
            emissionReason: responseObj.productEmissionReason,
        }).send(res);
        }
    )
)

router.get('/get/source/:productId/:sourceId', 
    asyncHandler(async (req, res, next) => {
        let responseObj: SourcePropertiesI | any = await getProductSources(req, next);
        new SuccessResponse('Full Product received successfuly', {
            product: responseObj.product.dataValues,
            source: responseObj.productSource,
            updateSource: responseObj.productUpdateSource,
            deletionSource: responseObj.productDeletionSource,
        }).send(res);
        }
    )
)

router.get('/get/random', 
    asyncHandler(async (req, res, next) => {
        let totalSamples:number = await GeneralService.getTotalNumberOfSamples('Product');
        if (!totalSamples) {throw new BadRequestError("Total samples not found.");}
        let repsonse:ProductPropertiesI | any = await GeneralService.findRandomNodes(totalSamples, Products);  
        let product1: ProductPropertiesI | any = await getProductCard(next, repsonse[0].dataValues.id);
        let product2: ProductPropertiesI | any = await getProductCard(next, repsonse[1].dataValues.id);
        let product3: ProductPropertiesI | any = await getProductCard(next, repsonse[2].dataValues.id);
        new SuccessResponse('Random Products received successfuly', [
            product1,
            product2,
            product3
        ]).send(res);
        }
    )
);

export default router;