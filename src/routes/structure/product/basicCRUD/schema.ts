import Joi from '@hapi/joi';

export default {
  createNew: Joi.object().keys({
    sourceTitle: Joi.string().required().min(5),
    sourceLink: Joi.string().uri().required().min(5),
    sourceDescription: Joi.string().required().min(20),
    name: Joi.string().required().min(2),
    description: Joi.string().required().min(20),
    linkToBuy: Joi.string().uri().optional().min(5),
    costPerProduct: Joi.number().required(),
  }),
  update: Joi.object().keys({
    sourceTitle: Joi.string().required().min(5),
    sourceLink: Joi.string().uri().required().min(5),
    sourceDescription: Joi.string().required().min(20),
    description: Joi.string().optional().min(20),
    linkToBuy: Joi.string().uri().optional().min(5),
    costPerProduct: Joi.number().optional(),
  }),
  requestDeletion: Joi.object().keys({
    sourceTitle: Joi.string().required().min(5),
    sourceLink: Joi.string().uri().required().min(5),
    sourceDescription: Joi.string().required().min(20),
  }),
};
