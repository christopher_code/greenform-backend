import express from 'express';
import { searchByParam } from "./service";
import { SuccessResponse } from '../../../../../middleware/ApiResponse';
import asyncHandler from "../../../../../helpers/asyncFunc";

const router = express.Router();

router.get('/:searchBy/:parameter/:approval', 
    asyncHandler(async (req, res) => {
        let responseObj: any = await searchByParam(req.params.searchBy, req.params.parameter, req);
        new SuccessResponse('Single GHG change history received successfuly', {
            responseObj
        }).send(res);
        }
    )
)

export default router;