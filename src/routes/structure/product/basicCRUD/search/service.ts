import { Request } from 'express';
import { BadRequestError, InternalError } from '../../../../../middleware/ApiError';
import ProductService from "../../../../../database/entityService/ProductService";
import { ProductPropertiesI } from "../../../../../database/models/Product";

export let searchByParam = async (searchBy: string, parameter: any, req:Request): Promise<ProductPropertiesI | any> => {
    try {
        let resp: ProductPropertiesI | any = await ProductService.findProductByParam(searchBy, parameter, req.params.approval);
        if (!resp) {throw new BadRequestError("Product not found.");}
        return resp;
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};