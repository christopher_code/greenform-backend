import { Request, NextFunction } from 'express';
import { Source } from "../../../../database/models/Source";
import { Users } from "../../../../database/models/Users";
import { BadRequestError, InternalError } from '../../../../middleware/ApiError';
import GeneralService from "../../../../database/entityService/GeneralService";
import { ProductPropertiesI, Products } from "../../../../database/models/Product";
import { SourcePropertiesI } from "../../../../database/models/Source";

export let getChangeHistory = async (req:Request, next:NextFunction): Promise<{} | any> => {
    try {
        let product:ProductPropertiesI | any = await GeneralService.findNodeById(Products, req.params.productId);
        if (!product) {throw new BadRequestError("Product not found.");}
        let productSource: SourcePropertiesI | any = await GeneralService.findNodeAnyRelations(req.params.productId,Products,
                                                                                Source,
                                                                                'HasSource',
                                                                                next
                                                                            );
        let productHasUpdateSource: any | undefined = await GeneralService.findNodeAnyRelationsWithProperties(req.params.productId, undefined, Products,
                                                                                Source,
                                                                                'HasUpdateSource',
                                                                                next
                                                                            );                                                                      
        let productAddedBy: any = await GeneralService.findNodeAnyRelations(req.params.productId,Products,
                                                                                Users,
                                                                                'AddedBy',
                                                                                next
                                                                            );    
        let deletionRequestSource: any = await GeneralService.findNodeAnyRelationsWithProperties(req.params.productId, undefined, Products,
                                                                                Source,
                                                                                'HasRequestDeletionSource',
                                                                                next
                                                                            );                                                                                                                                                                                                                                                                                                                                                                                             
        let responseObj: object = {
            product,
            productSource,
            productAddedBy,
            productHasUpdateSource,
            deletionRequestSource,
        }
        return responseObj
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};