import express from 'express';
import { getChangeHistory } from "./service";
import { SuccessResponse } from '../../../../middleware/ApiResponse';
import asyncHandler from "../../../../helpers/asyncFunc";

const router = express.Router();

router.get('/get/:productId', 
    asyncHandler(async (req, res, next) => {
        let responseObj: any = await getChangeHistory(req, next);
        new SuccessResponse('Full Product received successfuly', {
            product: responseObj.product.dataValues,
            source: responseObj.productSource,
            updateSource: responseObj.productHasUpdateSource,
            addedBy: [{
                createdAt: responseObj.productAddedBy[0].createdAt,
                name: responseObj.productAddedBy[0].name,
                company: responseObj.productAddedBy[0].company,
                id: responseObj.productAddedBy[0].id,
                email: responseObj.productAddedBy[0].email,
                roleStatus: responseObj.productAddedBy[0].roleStatus
            }],
            deletionRequestSource: responseObj.deletionRequestSource,
        }).send(res);
        }
    )
)

export default router;