import express from 'express';
import { getCategoryByName } from "./service";
import { SuccessResponse } from '../../../middleware/ApiResponse';
import asyncHanlder from "../../../helpers/asyncFunc";

const router = express.Router();

router.get('/get/:name',
    asyncHanlder(async (req, res) => {
        const category = await getCategoryByName(req);
        new SuccessResponse('Category was found with success.', {
            category: category,
        }).send(res);
    })
)

export default router;