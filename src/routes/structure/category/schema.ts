import Joi from '@hapi/joi';

export default {
  createNew: Joi.object().keys({
    typeOfCategory: Joi.string().required(),
  }),
};
