import express from 'express';
import { createCategory } from "./service";
import { SuccessResponse } from '../../../middleware/ApiResponse';
import validator from '../../../helpers/validator';
import schema from "./schema";
import asyncHanlder from "../../../helpers/asyncFunc";

const router = express.Router();

router.post('/new/:userId/:productId',
    validator(schema.createNew),
    asyncHanlder(async (req, res) => {
        const category = await createCategory(req);
        new SuccessResponse('Category saved with success', {
            id: category.id,
            category: category,
            }).send(res);
    })
)

export default router;