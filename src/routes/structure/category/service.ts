import { Request } from 'express';
import { Categories, CategoryPropertiesI } from "../../../database/models/category";
import { generateUniqeuId } from "../../../middleware/generateUuid";
import { BadRequestError, InternalError } from '../../../middleware/ApiError';
import { generateDate } from "../../../middleware/generateDate";
import GeneralService from "../../../database/entityService/GeneralService";
import { checkUniqueData } from "../../../middleware/checkUniqueData";
import { DataAlredayExistsError } from "../../../middleware/ApiError";
import CategoryService from "../../../database/entityService/CategoryService";
import { ProductPropertiesI, Products } from "../../../database/models/Product";

export let createCategory = async (req:Request): Promise<CategoryPropertiesI | any> => {
    try {
        const isNotUnique: boolean | any = await checkUniqueData(req.body.typeOfCategory ,"Categories");
        if (isNotUnique) {throw new DataAlredayExistsError('Category does already exists, please rename it or edit existing category.')}

        let input = {   id: generateUniqeuId(),
                        typeOfCategory: req.body.typeOfCategory,
                        createdAt: generateDate(),
                        AddedBy: {where: {params: {id: req.params.userId}}}}

        let categories:CategoryPropertiesI = await GeneralService.createNode(Categories, input);

        if (!categories) {throw new BadRequestError("Category not found.");}
        let product:ProductPropertiesI | any = await GeneralService.findNodeById(Products, req.params.productId);
        if (!product) {throw new BadRequestError("Product not found.");}

        await GeneralService.relateNodes(product, "BelongsToCategory", categories.id, undefined);

        return categories;
    }
    catch (err) {
        throw new InternalError(err);
    }  
};

export let getCategoryByName = async (req: Request): Promise<any> => {
    try {
        const category: CategoryPropertiesI | any = await CategoryService.findCategoryByName(req);
        if (category) {return category} else {return false};
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    };
};
