import express from 'express';
import { getChangeHistory } from "./service";
import { SuccessResponse } from '../../../../middleware/ApiResponse';
import asyncHandler from "../../../../helpers/asyncFunc";

const router = express.Router();

router.get('/get/:id', 
    asyncHandler(async (req, res, next) => {
        let responseObj: any = await getChangeHistory(req, next);
        new SuccessResponse('Seller change history received successfuly', {
            seller: responseObj.seller.dataValues,
            source: responseObj.sellerSource,
            updateSource: responseObj.sellerHasUpdateSource,
            addedBy: {
                createdAt: responseObj.sellerAddedBy[0].createdAt,
                name: responseObj.sellerAddedBy[0].name,
                company: responseObj.sellerAddedBy[0].company,
                id: responseObj.sellerAddedBy[0].id,
                email: responseObj.sellerAddedBy[0].email,
                roleStatus: responseObj.sellerAddedBy[0].roleStatus
            },
            requestDeletion: responseObj.requestDeletion,
        }).send(res);
        }
    )
)

export default router;