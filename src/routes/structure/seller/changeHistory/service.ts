import { Request, NextFunction } from 'express';
import { Source, SourcePropertiesI } from "../../../../database/models/Source";
import { Users } from "../../../../database/models/Users";
import { BadRequestError, InternalError } from '../../../../middleware/ApiError';
import SellerService from "../../../../database/entityService/SellerService";
import GeneralService from "../../../../database/entityService/GeneralService";
import { SellerPropertiesI, Sellers } from "../../../../database/models/Seller"

export let getChangeHistory = async (req:Request, next:NextFunction): Promise<{}|any> => {
    try {
        let seller: SellerPropertiesI | any = await GeneralService.findNodeById(Sellers, req.params.id);
        if (!seller) {throw new BadRequestError("Seller not found.");}
        let sellerSource: SourcePropertiesI | any = await GeneralService.findNodeAnyRelations(seller.dataValues.id, Sellers,
                                                                                Source,
                                                                                'HasSource',
                                                                                next
                                                                            );
        let sellerHasUpdateSource: SourcePropertiesI | any | undefined = await GeneralService.findNodeAnyRelationsWithProperties(req.params.id, undefined, Sellers,
                                                                                Source,
                                                                                'HasUpdateSource',
                                                                                next
                                                                            );                                                                                                                                       
        let sellerAddedBy: SourcePropertiesI | any = await GeneralService.findNodeAnyRelations(req.params.id, Sellers,
                                                                                Users,
                                                                                'AddedBy',
                                                                                next
                                                                            );  
        let requestDeletion: SourcePropertiesI | any = await GeneralService.findNodeAnyRelationsWithProperties(req.params.id, undefined, Sellers,
                                                                                Source,
                                                                                'HasRequestDeletionSource',
                                                                                next
                                                                            );                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
        let responseObj: object = {
            seller,
            sellerSource,
            sellerAddedBy,
            sellerHasUpdateSource,
            requestDeletion,
        }
        return responseObj
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};