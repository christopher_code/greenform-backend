import Joi from '@hapi/joi';

export default {
  createNew: Joi.object().keys({
    sourceTitle: Joi.string().required().min(5),
    sourceLink: Joi.string().uri().required().min(5),
    sourceDescription: Joi.string().required().min(20),
    name: Joi.string().required().min(5),
    link: Joi.string().uri().required().min(5),
  }),
  update: Joi.object().keys({
    sourceTitle: Joi.string().required().min(5),
    sourceLink: Joi.string().uri().required().min(5),
    sourceDescription: Joi.string().required().min(20),
    link: Joi.string().required().min(5),
  }),
  requestDeletion: Joi.object().keys({
    sourceTitle: Joi.string().required().min(5),
    sourceLink: Joi.string().uri().required().min(5),
    sourceDescription: Joi.string().required().min(20),
  }),
};
