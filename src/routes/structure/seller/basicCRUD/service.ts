import { Request, Response, NextFunction } from 'express';
import { Sellers, SellerPropertiesI } from "../../../../database/models/Seller";
import { Products } from "../../../../database/models/Product";
import { ProductPropertiesI } from "../../../../database/models/Product";
import { Source, SourcePropertiesI } from "../../../../database/models/Source";
import { ApprovalStatus } from "../../../../database/models/Source";
import { generateUniqeuId } from "../../../../middleware/generateUuid";
import { BadRequestError, InternalError } from '../../../../middleware/ApiError';
import { generateDate } from "../../../../middleware/generateDate";
import SourceService from "../../../../database/entityService/SourceService";
import GeneralService from "../../../../database/entityService/GeneralService";
import SellerService from "../../../../database/entityService/SellerService";

export let createSeller = async (req:Request, userId:string): Promise<SellerPropertiesI> => {
    try {
        let id:string = generateUniqeuId();
        let source: SourcePropertiesI | any = await SourceService.saveSource(req, 'new', 'seller', id, req.params.productId);
        if (!source) {throw new BadRequestError("Source not saved.");}

        let input = {id: id,
            name: req.body.name,
            link: req.body.link,
            createdAt: generateDate(),
            approvalStatus: ApprovalStatus.NEW_SOURCE,
            sourceId: source.id,
            HasSource: {where: {params: {id: source.id}}},
            AddedBy: {where: {params: {id: userId}}}}

        let seller:SellerPropertiesI = await GeneralService.createNode(Sellers, input);
        if (!seller) {throw new BadRequestError("Seller not saved.");}

        let product:ProductPropertiesI | any = await GeneralService.findNodeById(Products, req.params.productId);
        if (!product) {throw new BadRequestError("Product not found.");}

        await GeneralService.relateNodes(product, "CanBeBoughtFrom", seller.id, undefined);

        return seller;
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }
};


export let updateSeller = async (sellerId: string, req:Request, res:Response): Promise<SellerPropertiesI | any> => {
    try {
        let source: SourcePropertiesI | any;
        let changedField: string = '';
        let chnagedValue: string | number | boolean;
        let originalValue: string | number | boolean;
        if (req.body.sourceTitle) {
            source = await SourceService.saveSource(req,'update', 'seller', sellerId, req.params.productId);
            if (!source) {throw new BadRequestError("Source not found.");}
        }
        let seller:SellerPropertiesI | any = await GeneralService.findNodeById(Sellers, sellerId);
        if (!seller) {throw new BadRequestError("Seller not found.");}
        if (req.body.link) {
            originalValue = seller.dataValues.link;
            changedField = 'link';
            chnagedValue = req.body.link;
            await updateSellerSerive(sellerId, req.body.link)
        } else {throw new BadRequestError("Please provide valid data!")}

        await GeneralService.relateNodes(seller, "HasUpdateSource", source.id, {
            ChangedField: changedField,
                OriginalValue: originalValue,
                NewValue: chnagedValue,
                Date: generateDate(),
                UserId: req.params.userId,
        });

        return seller;
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};

export let getSellerSources = async (req:Request, next:NextFunction): Promise<{} | any> => {
    try {
        let seller:SellerPropertiesI | any = await await GeneralService.findNodeById(Sellers, req.params.sellerId);
        if (!seller) {throw new BadRequestError("Product not found.");}
        let sellerSource: SourcePropertiesI | any = await GeneralService.findNodeAnyRelations(req.params.sellerId, Sellers,
                                                                                Source,
                                                                                'HasSource',
                                                                                next
                                                                            );   
        let sellerUpdateSource: SourcePropertiesI | any = await GeneralService.findNodeAnyRelationsWithProperties(req.params.sellerId,
                                                                                req.params.sourceId, Products,
                                                                                Source,
                                                                                'HasUpdateSource',
                                                                                next
                                                                            );  
        let sellerDeletionSource: SourcePropertiesI | any = await GeneralService.findNodeAnyRelationsWithProperties(req.params.sellerId,
                                                                                req.params.sourceId, Products,
                                                                                Source,
                                                                                'HasRequestDeletionSource',
                                                                                next
                                                                            );                                                                                                                                                                                                                                                                                                                                                                                                       
        let responseObj: object = {
            seller,
            sellerSource,
            sellerUpdateSource,
            sellerDeletionSource,
        }
        return responseObj
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};

export let requestSellerDeletion = async (req:Request): Promise<SellerPropertiesI | any> => {
    try {
        let seller:SellerPropertiesI | any = await await GeneralService.findNodeById(Sellers, req.params.sellerId);
        if (!seller) {throw new BadRequestError("No seller found")}
        let source: SourcePropertiesI | any = await SourceService.saveSource(req, 'delete', 'seller', req.params.sellerId, req.params.productId);
        if (!source) {throw new BadRequestError("Source not saved.");}

        await GeneralService.relateNodes(seller, "HasRequestDeletionSource", source.id, {
                Date: generateDate(),
                UserId: req.params.userId,
                Name: seller.dataValues.name,
                Link: seller.dataValues.link,
                CreatedAt: seller.dataValues.createdAt,
                approvalStatus: seller.dataValues.approvalStatus,
                originalId: seller.dataValues.id,
        });

        return seller;
    }
    catch (err) {
        throw new InternalError("Something went wrong!" + err);
    }  
};

export let getSellerByName = async (req: Request): Promise<SellerPropertiesI | any> => {
    try {
        const seller:SellerPropertiesI | any = await SellerService.findSellerByName(req.params.name);
        if (seller) {return seller} else {return false};
    }
    catch (err) {
        throw new InternalError("Something went wrong! 1");
    };
};

let updateSellerSerive = async (id: string, link: string): Promise<SellerPropertiesI | any> => {
    try {
        await Sellers.update(
            {
                link: link
            },
            {
                where: {
                    id: id
                },
            }
        );
    }
    catch (err) {
        throw new InternalError("Something went wrong!" + err);
    }  
};
