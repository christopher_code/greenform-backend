import express from 'express';
import { createSeller, requestSellerDeletion, updateSeller } from "./service";
import { SuccessResponse } from '../../../../middleware/ApiResponse';
import asyncHanlder from "../../../../helpers/asyncFunc";
import validator from "../../../../helpers/validator";
import schema from "./schema";
import { SellerPropertiesI } from "../../../../database/models/Seller";

const router = express.Router();

router.post('/new/:userId/:productId',
    validator(schema.createNew),
    asyncHanlder(async (req, res) => {
        const seller = await createSeller(req, req.params.userId);
        new SuccessResponse('Seller saved with success', {
            id: seller.id,
            seller: seller,
            }).send(res);
    })
)

router.put('/update/fields/:sellerId/:userId/:productId', 
    validator(schema.update),
    asyncHanlder(async (req, res) => {
        let seller:SellerPropertiesI | any = await updateSeller(req.params.sellerId, req, res);
        new SuccessResponse('Seller updated successfuly', {
            seller: seller
        }).send(res);
    })
)

router.put('/request/deletion/:sellerId/:userId/:productId', 
    validator(schema.requestDeletion),
    asyncHanlder(async (req, res) => {
        let seller = await requestSellerDeletion(req);
        new SuccessResponse('Seller request to delte was successful!', {seller: seller}).send(res);
        }
    )
)

export default router;