import express from 'express';
import { getSellerByName, getSellerSources } from "./service";
import { SourcePropertiesI } from "../../../../database/models/Source"
import { SuccessResponse } from '../../../../middleware/ApiResponse';
import asyncHanlder from "../../../../helpers/asyncFunc";

const router = express.Router();

router.get('/get/:name',
    asyncHanlder(async (req, res) => {
        const seller = await getSellerByName(req);
        new SuccessResponse('Found seller by name.', {
            seller: seller
        }).send(res);
    })
)

router.get('/get/source/:sellerId/:sourceId', 
    asyncHanlder(async (req, res, next) => {
        let responseObj: SourcePropertiesI | any = await getSellerSources(req, next);
        new SuccessResponse('Full Seller sources received successfuly', {
            seller: responseObj.seller.dataValues,
            source: responseObj.sellerSource,
            updateSource: responseObj.sellerUpdateSource,
            deletionSource: responseObj.sellerDeletionSource,
        }).send(res);
        }
    )
)

export default router;