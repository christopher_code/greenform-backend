import Joi from '@hapi/joi';

export default {
  createNew: Joi.object().keys({
    sourceTitle: Joi.string().required().min(5),
    sourceLink: Joi.string().uri().required().min(5),
    sourceDescription: Joi.string().required().min(20),
    ghgName: Joi.string().required().min(3),
    emissionPerKG: Joi.number().required().strict(),
  }),
  update: Joi.object().keys({
    sourceTitle: Joi.string().required().min(5),
    sourceLink: Joi.string().uri().required().min(5),
    sourceDescription: Joi.string().required().min(20),
    emissionPerKG: Joi.number().required().strict(),
  }),
  requestDeletion:  Joi.object().keys({
    sourceTitle: Joi.string().required().min(5),
    sourceLink: Joi.string().uri().required().min(5),
    sourceDescription: Joi.string().required().min(20),
  }),
};
