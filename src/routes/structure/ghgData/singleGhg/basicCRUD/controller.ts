import express from 'express';
import { createSingleGhg, requestSingleGHGDeletion, getSingleSources } from "./service";
import { SourcePropertiesI } from "../../../../../database/models/Source";
import { singleGHGEmissionPropertiesI } from "../../../../../database/models/GHG/singleGHG";
import { SuccessResponse } from '../../../../../middleware/ApiResponse';
import validator from '../../../../../helpers/validator';
import schema from './schema';
import asyncHanlder from "../../../../../helpers/asyncFunc";
import { updateSingleFields } from "./service";

const router = express.Router();

router.post('/new/:userId/:productId',
    validator(schema.createNew),
    asyncHanlder(async (req, res) => {
        const singleGhg = await createSingleGhg(req);
        new SuccessResponse('Single Ghg Data saved with success', {
            title: req.body.sourceTitle,
            link: req.body.sourceLink,
            description: req.body.sourceDescription,
            id: singleGhg.id,
            category: singleGhg,
            }).send(res);
    })
)

router.put('/update/fields/:singleId/:userId/:productId', 
    validator(schema.update),
    asyncHanlder(async (req, res) => {
            let single:singleGHGEmissionPropertiesI | any = await updateSingleFields(req);
            new SuccessResponse('Single ghg data updated successfuly', {
                singleGHG: single
            }).send(res);
        }
    )
)

router.put('/request/deletion/:singleId/:userId/:productId', 
    validator(schema.requestDeletion),
    asyncHanlder(async (req, res) => {
        await requestSingleGHGDeletion(req);
        new SuccessResponse('Single GHG request to delte was successful!', {}).send(res);
        }
    )
)

router.get('/get/source/:singleId/:sourceId', 
    asyncHanlder(async (req, res, next) => {
        let responseObj: SourcePropertiesI | any = await getSingleSources(req, next);
        new SuccessResponse('Single sources received successfuly', {
            single: responseObj.singleGHG.dataValues,
            source: responseObj.singleSource,
            updateSource: responseObj.singleUpdateSource,
            deletionSource: responseObj.singleDeletionSource,
        }).send(res);
        }
    )
)

export default router;