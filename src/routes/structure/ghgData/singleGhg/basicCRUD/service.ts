import { Request, NextFunction } from 'express';
import { singleGHGEmissions, singleGHGEmissionPropertiesI } from "../../../../../database/models/GHG/singleGHG";
import {SourcePropertiesI} from "../../../../../database/models/Source";
import { ProductPropertiesI, Products } from "../../../../../database/models/Product";
import { Source } from "../../../../../database/models/Source";
import { ApprovalStatus } from "../../../../../database/models/Source";
import { generateUniqeuId } from "../../../../../middleware/generateUuid";
import { BadRequestError, InternalError } from '../../../../../middleware/ApiError';
import { generateDate } from "../../../../../middleware/generateDate";
import SourceService from "../../../../../database/entityService/SourceService";
import GeneralService from '../../../../../database/entityService/GeneralService';

export let createSingleGhg = async (req:Request): Promise<singleGHGEmissionPropertiesI> => {
    try {
        let id: string = generateUniqeuId();
        let source: SourcePropertiesI | any = await SourceService.saveSource(req, 'new', 'single', id, req.params.productId);
        if (!source) {throw new BadRequestError("Source not saved.");}

        let input = {id: id,
            ghgName: req.body.ghgName,
            emissionPerKG: req.body.emissionPerKG,
            createdAt: generateDate(),
            approvalStatus: ApprovalStatus.NEW_SOURCE,
            sourceId: source.id,
            HasSource: {where: {params: {id: source.id}}},
            AddedBy: {where: {params: {id: req.params.userId}}}}

        let singleGhg:singleGHGEmissionPropertiesI = await GeneralService.createNode(singleGHGEmissions, input);

        if (!singleGhg) {throw new BadRequestError("Single GHG not saved.");}

        let product:ProductPropertiesI | any = await GeneralService.findNodeById(Products, req.params.productId);
        if (!product) {throw new BadRequestError("Product not found.");}

        await GeneralService.relateNodes(product, "EmitsGHG", singleGhg.id, undefined);

        return singleGhg;
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};

export let updateSingleFields = async (req:Request): Promise<singleGHGEmissionPropertiesI | any> => {
    try {
        let source: SourcePropertiesI | any;
        let changedField: string = '';
        let chnagedValue: number | string | boolean = '';
        let originalValue: number | string | boolean = '';
        if (req.body.sourceTitle) {
            source = await SourceService.saveSource(req, 'update', 'single', req.params.singleId, req.params.productId);
            if (!source) {throw new BadRequestError("Source not saved.");}
        }
        let single: singleGHGEmissionPropertiesI |any = await GeneralService.findNodeById(singleGHGEmissions, req.params.singleId);
        if (!single) {throw new BadRequestError("Single GHG not found.");}
        if (req.body.emissionPerKG) {
            originalValue = single.dataValues.emissionPerKG;
            single.emissionPerKG = req.body.emissionPerKG;
            changedField = 'Emission in kg';
            chnagedValue = req.body.emissionPerKG;
        }

        if (req.body.sourceTitle) {
            await GeneralService.relateNodes(single, "HasUpdateSource", source.id, {
                ChangedField: changedField,
                OriginalValue: originalValue,
                NewValue: chnagedValue,
                Date: generateDate(),
                UserId: req.params.userId,
            });
            
        }
        await single.save();
        return single
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};

export let requestSingleGHGDeletion = async (req:Request): Promise<singleGHGEmissionPropertiesI | any> => {
    try {
        let single:singleGHGEmissionPropertiesI | any = await GeneralService.findNodeById(singleGHGEmissions, req.params.singleId);
        if (!single) {throw new BadRequestError("No single GHG found")}
        let source: SourcePropertiesI | any = await SourceService.saveSource(req, 'delete', 'single', req.params.singleId, req.params.productId);
        
        await GeneralService.relateNodes(single, "HasRequestDeletionSource", source.id, {
            Date: generateDate(),
            UserId: req.params.userId,
            ghgName: single.dataValues.ghgName,
            emissionPerKG: single.dataValues.emissionPerKG,
            createdAt: single.dataValues.createdAt,
            approvalStatus: single.dataValues.approvalStatus,
            originalId: single.dataValues.id,
        });

        return single;
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};

export let getSingleSources = async (req:Request, next:NextFunction): Promise<{} | any> => {
    try {
        let singleGHG:singleGHGEmissionPropertiesI | any = await GeneralService.findNodeById(singleGHGEmissions, req.params.singleId);
        if (!singleGHG) {throw new BadRequestError("Single not found.");}
        let singleSource: SourcePropertiesI | any = await GeneralService.findNodeAnyRelations(req.params.singleId,
                                                                                singleGHGEmissions, Source,
                                                                                'HasSource',
                                                                                next
                                                                            );  
        let singleUpdateSource: SourcePropertiesI | any = await GeneralService.findNodeAnyRelationsWithProperties(req.params.singleId,
                                                                                req.params.sourceId, singleGHGEmissions,
                                                                                Source,
                                                                                'HasUpdateSource',
                                                                                next
                                                                            );  
        let singleDeletionSource: SourcePropertiesI | any = await GeneralService.findNodeAnyRelationsWithProperties(req.params.singleId,
                                                                                req.params.sourceId, singleGHGEmissions,
                                                                                Source,
                                                                                'HasRequestDeletionSource',
                                                                                next
                                                                            );                                                                                                                                                                                                                                                                                                                                                                                                     
        let responseObj: object = {
            singleGHG,
            singleSource,
            singleUpdateSource,
            singleDeletionSource,
        }
        return responseObj
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};
