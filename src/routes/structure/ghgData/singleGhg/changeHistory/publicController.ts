import express from 'express';
import { getChangeHistory } from "./service";
import { SuccessResponse } from '../../../../../middleware/ApiResponse';
import asyncHandler from "../../../../../helpers/asyncFunc";

const router = express.Router();

router.get('/get/:id', 
    asyncHandler(async (req, res, next) => {
        let responseObj: any = await getChangeHistory(req, next);
        new SuccessResponse('Single GHG change history received successfuly', {
            singleEmission: responseObj.singleGHG.dataValues,
            source: responseObj.singleSource,
            updateSource: responseObj.singleHasUpdateSource,
            addedBy: [{
                createdAt: responseObj.singleAddedBy[0].createdAt,
                name: responseObj.singleAddedBy[0].name,
                company: responseObj.singleAddedBy[0].company,
                id: responseObj.singleAddedBy[0].id,
                email: responseObj.singleAddedBy[0].email,
                roleStatus: responseObj.singleAddedBy[0].roleStatus
            }],
            deletionSource: responseObj.singleHasDeletionSource,
        }).send(res);
        }
    )
)

export default router;