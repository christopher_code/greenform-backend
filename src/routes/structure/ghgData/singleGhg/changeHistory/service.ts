import { Request, NextFunction } from 'express';
import { singleGHGEmissionPropertiesI, singleGHGEmissions } from "../../../../../database/models/GHG/singleGHG";
import { Source } from "../../../../../database/models/Source";
import { Users } from "../../../../../database/models/Users";
import { BadRequestError, InternalError } from '../../../../../middleware/ApiError';
import GeneralService from "../../../../../database/entityService/GeneralService";

export let getChangeHistory = async (req:Request, next:NextFunction): Promise<{} | any> => {
    try {
        let singleGHG:singleGHGEmissionPropertiesI | any = await GeneralService.findNodeById(singleGHGEmissions, req.params.id);
        if (!singleGHG) {throw new BadRequestError("Single GHG not found.");}
        let singleSource: any = await GeneralService.findNodeAnyRelations(singleGHG.dataValues.id,
                                                                                singleGHGEmissions,
                                                                                Source,
                                                                                'HasSource',
                                                                                next
                                                                            );
        let singleHasUpdateSource: any | undefined = await GeneralService.findNodeAnyRelationsWithProperties(req.params.id, undefined,
                                                                                singleGHGEmissions, Source, 
                                                                                'HasUpdateSource',
                                                                                next
                                                                            );                                                                                                                                       
        let singleAddedBy: any = await GeneralService.findNodeAnyRelations(req.params.id,
                                                                                singleGHGEmissions,
                                                                                Users,
                                                                                'AddedBy',
                                                                                next
                                                                            );
        let singleHasDeletionSource: any | undefined = await GeneralService.findNodeAnyRelationsWithProperties(req.params.id, undefined,
                                                                                singleGHGEmissions, Source, 
                                                                                'HasRequestDeletionSource',
                                                                                next
                                                                            );                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
        let responseObj: object = {
            singleGHG,
            singleSource,
            singleAddedBy,
            singleHasUpdateSource,
            singleHasDeletionSource,
        }
        return responseObj
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};