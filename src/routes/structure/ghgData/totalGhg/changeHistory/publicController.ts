import express from 'express';
import { getChangeHistory } from "./service";
import { SuccessResponse } from '../../../../../middleware/ApiResponse';
import asyncHandler from "../../../../../helpers/asyncFunc";

const router = express.Router();

router.get('/get/:id', 
    asyncHandler(async (req, res, next) => {
        let responseObj: any = await getChangeHistory(req, next);
        new SuccessResponse('Total GHG change history received successfuly', {
            totalEmission: responseObj.totalGHG.dataValues,
            source: responseObj.totalSource,
            updateSource: responseObj.totalHasUpdateSource,
            deletionSource: responseObj.totalDeletionSource,
            addedBy: [{
                createdAt: responseObj.totalAddedBy[0].createdAt,
                name: responseObj.totalAddedBy[0].name,
                company: responseObj.totalAddedBy[0].company,
                id: responseObj.totalAddedBy[0].id,
                email: responseObj.totalAddedBy[0].email,
                roleStatus: responseObj.totalAddedBy[0].roleStatus
            }],
        }).send(res);
        }
    )
)

export default router;