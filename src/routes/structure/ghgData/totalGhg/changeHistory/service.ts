import { Request, NextFunction } from 'express';
import { Source, SourcePropertiesI } from "../../../../../database/models/Source";
import { Users } from "../../../../../database/models/Users";
import { BadRequestError, InternalError } from '../../../../../middleware/ApiError';
import { totalGHGEmissionsPropertiesI, totalGHGEmissions } from "../../../../../database/models/GHG/totalGHG";
import GeneralService from "../../../../../database/entityService/GeneralService";

export let getChangeHistory = async (req:Request, next:NextFunction): Promise<{} | any> => {
    try {
        let totalGHG:totalGHGEmissionsPropertiesI | any = await GeneralService.findNodeById(totalGHGEmissions, req.params.id);
        if (!totalGHG) {throw new BadRequestError("Total GHG not found.");}
        let totalSource: totalGHGEmissionsPropertiesI | any = await GeneralService.findNodeAnyRelations(totalGHG.dataValues.id, totalGHGEmissions,
                                                                                Source,
                                                                                'HasSource',
                                                                                next
                                                                            );
        let totalHasUpdateSource: SourcePropertiesI | any | undefined = await GeneralService.findNodeAnyRelationsWithProperties(req.params.id, undefined, 
                                                                                totalGHGEmissions, Source,
                                                                                'HasUpdateSource',
                                                                                next
                                                                            );      
        let totalDeletionSource: SourcePropertiesI | any | undefined = await GeneralService.findNodeAnyRelationsWithProperties(req.params.id, undefined, 
                                                                                totalGHGEmissions, Source,
                                                                                'HasRequestDeletionSource',
                                                                                next
                                                                            );                                                                                                                                                                                                        
        let totalAddedBy: totalGHGEmissionsPropertiesI | any = await GeneralService.findNodeAnyRelations(req.params.id, totalGHGEmissions,
                                                                                Users,
                                                                                'AddedBy',
                                                                                next
                                                                            );                                                                                                                                                                                                                                                                                                                                                                                                   
        let responseObj: object = {
            totalGHG,
            totalSource,
            totalAddedBy,
            totalHasUpdateSource,
            totalDeletionSource
        }
        return responseObj
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};