import Joi from '@hapi/joi';

export default {
  createNewAndUpdate: Joi.object().keys({
    sourceTitle: Joi.string().required().min(5),
    sourceLink: Joi.string().uri().required().min(5),
    sourceDescription: Joi.string().required().min(20),
    ghgPerKG: Joi.number().required().strict(),
  }),
  requestDeletion:  Joi.object().keys({
    sourceTitle: Joi.string().required().min(5),
    sourceLink: Joi.string().uri().required().min(5),
    sourceDescription: Joi.string().required().min(20),
  }),
};