import { Request, NextFunction } from 'express';
import { totalGHGEmissions, totalGHGEmissionsPropertiesI } from "../../../../../database/models/GHG/totalGHG";
import { ProductPropertiesI, Products } from "../../../../../database/models/Product";
import { Source, SourcePropertiesI } from "../../../../../database/models/Source";
import { ApprovalStatus } from "../../../../../database/models/Source";
import { generateUniqeuId } from "../../../../../middleware/generateUuid";
import { BadRequestError, InternalError } from '../../../../../middleware/ApiError';
import SourceService from "../../../../../database/entityService/SourceService";
import { generateDate } from "../../../../../middleware/generateDate";
import GeneralService from "../../../../../database/entityService/GeneralService";

export let createTotalGhg = async (req:Request): Promise<totalGHGEmissionsPropertiesI> => {
    try {
        let id:string = generateUniqeuId();
        let source: SourcePropertiesI | any = await SourceService.saveSource(req, 'new', 'total', id, req.params.productId);
        if (!source) {throw new BadRequestError("Source not saved.");}

        let input = {id: id,
            ghgPerKG: req.body.ghgPerKG,
            approvalStatus: ApprovalStatus.NEW_SOURCE,
            sourceId: source.id,
            HasSource: {where: {params: {id: source.id}}},
            AddedBy: {where: {params: {id: req.params.userId}}}}

        let totalGHG:totalGHGEmissionsPropertiesI = await GeneralService.createNode(totalGHGEmissions, input);
        if (!totalGHG) {throw new BadRequestError("Total GHG not saved.");}

        let product:ProductPropertiesI | any = await GeneralService.findNodeById(Products, req.params.id);
        if (!product) {throw new BadRequestError("Product not found.");}

        await GeneralService.relateNodes(product, "EmitsInTotal", totalGHG.id, undefined);

        return totalGHG;
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};

export let updateTotalGHGData = async (totalId: string, req:Request): Promise<totalGHGEmissionsPropertiesI | any> => {
    try {
        let source: SourcePropertiesI | any;
        let changedField: string = '';
        let chnagedValue: number | string | boolean | any;
        let originalValue: number | string | boolean | any;
        if (req.body.sourceTitle) {
            source = await SourceService.saveSource(req, 'update', 'total', totalId, req.params.productId);
            if (!source) {throw new BadRequestError("Source not saved.");}
        }
        let total:totalGHGEmissionsPropertiesI | any = await GeneralService.findNodeById(totalGHGEmissions, totalId);
        if (req.body.ghgPerKG) {
            originalValue = total.dataValues.ghgPerKG;
            total.ghgPerKG = req.body.ghgPerKG;
            changedField = 'ghgPerKG';
            chnagedValue = req.body.ghgPerKG;
        } 
        await total.save();
        if (req.body.sourceTitle) {
            await GeneralService.relateNodes(total, "HasUpdateSource", source.id, {
                ChangedField: changedField,
                OriginalValue: originalValue,
                NewValue: chnagedValue,
                Date: generateDate(),
                UserId: req.params.userId,
            });
        }
        return total
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};

export let requestTotalGHGDeletion = async (req:Request): Promise<totalGHGEmissionsPropertiesI | any> => {
    try {
        let total:totalGHGEmissionsPropertiesI | any = await GeneralService.findNodeById(totalGHGEmissions, req.params.totalId);
        if (!total) {throw new BadRequestError("No product found")}
        let source: SourcePropertiesI | any = await SourceService.saveSource(req, 'delete', 'total', req.params.totalId, req.params.productId);
        if (!source) {throw new BadRequestError("Source not saved.");}
        await GeneralService.relateNodes(total, "HasRequestDeletionSource", source.id, {
            Date: generateDate(),
                UserId: req.params.userId,
                ghgPerKG: total.dataValues.ghgPerKG,
                approvalStatus: total.dataValues.approvalStatus,
                originalId: total.dataValues.id,
        });

        return total;
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};

export let getTotalSources = async (req:Request, next:NextFunction): Promise<{} | any> => {
    try {
        let totalGHG:totalGHGEmissionsPropertiesI | any = await GeneralService.findNodeById(totalGHGEmissions, req.params.totalId);
        if (!totalGHG) {throw new BadRequestError("Product not found.");}
        let totalSource: SourcePropertiesI | any = await GeneralService.findNodeAnyRelations(req.params.totalId, totalGHGEmissions,
                                                                                Source,
                                                                                'HasSource',
                                                                                next
                                                                            );  
        let totalDeletionSource: SourcePropertiesI | any = await GeneralService.findNodeAnyRelationsWithProperties(req.params.totalId,
                                                                                req.params.sourceId, totalGHGEmissions,
                                                                                Source,
                                                                                'HasRequestDeletionSource',
                                                                                next
                                                                            );                                                                                                                                                                                                                                                                                                                                                                                                       
        let responseObj: object = {
            totalGHG,
            totalSource,
            totalDeletionSource,
        }
        return responseObj
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};
