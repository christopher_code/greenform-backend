import express from 'express';
import { createTotalGhg, requestTotalGHGDeletion, getTotalSources } from "./service";
import { SuccessResponse } from '../../../../../middleware/ApiResponse';
import validator from '../../../../../helpers/validator';
import schema from './schema';
import asyncHanlder from "../../../../../helpers/asyncFunc";
import { checkRelationshipExists } from "../../../../../middleware/checkRelationshipExists";
import { updateTotalGHGData } from "./service";
import { totalGHGEmissionsPropertiesI } from "../../../../../database/models/GHG/totalGHG";
import { BadRequestError } from '../../../../../middleware/ApiError';


const router = express.Router();

router.post('/new/:userId/:productId',
    validator(schema.createNewAndUpdate),
    asyncHanlder(async (req, res, next) => {
        let exists = await checkRelationshipExists(req.params.productId, '', 'totalGHG', next)
        if (exists) {throw new BadRequestError("Total GHG Emission already exists.")}
        const totalGhg = await createTotalGhg(req);
        new SuccessResponse('Total GHG Emission Data saved with success', {
            id: totalGhg.id,
            totalGhg: totalGhg
            }).send(res);
    })
)

router.put('/update/fields/:totalId/:userId/:productId', 
    validator(schema.createNewAndUpdate),
    asyncHanlder(async (req, res) => {
        let totalGHGData:totalGHGEmissionsPropertiesI | any = await updateTotalGHGData(req.params.totalId, req);
        new SuccessResponse('Total GHG Data updated successfuly', {
            ghgPerKG: totalGHGData
        }).send(res);
    }
)
)

router.put('/request/deletion/:totalId/:userId/:productId', 
asyncHanlder(async (req, res, next) => {
        await requestTotalGHGDeletion(req);
        new SuccessResponse('Total GHG request to delte was successful!', {}).send(res);
        }
    )
)

router.get('/get/source/:totalId/:sourceId', 
    asyncHanlder(async (req, res, next) => {
        let responseObj: totalGHGEmissionsPropertiesI | any = await getTotalSources(req, next);
        new SuccessResponse('Total sources received successfuly', {
            total: responseObj.totalGHG.dataValues,
            source: responseObj.totalSource,
            deletionSource: responseObj.totalDeletionSource,
        }).send(res);
        }
    )
)

export default router;