import express from 'express';
import { getChangeHistory } from "./service";
import { SuccessResponse } from '../../../../../middleware/ApiResponse';
import asyncHandler from "../../../../../helpers/asyncFunc";

const router = express.Router();

router.get('/get/:id', 
    asyncHandler(async (req, res, next) => {
        let responseObj: any = await getChangeHistory(req, next);
        new SuccessResponse('Reason GHG change history received successfuly', {
            reasonOfEmission: responseObj.reasonGHG.dataValues,
            source: responseObj.reasonSource,
            updateSource: responseObj.reasonHasUpdateSource,
            addedBy: [{
                createdAt: responseObj.reasonAddedBy[0].createdAt,
                name: responseObj.reasonAddedBy[0].name,
                company: responseObj.reasonAddedBy[0].company,
                id: responseObj.reasonAddedBy[0].id,
                email: responseObj.reasonAddedBy[0].email,
                roleStatus: responseObj.reasonAddedBy[0].roleStatus
            }],
            requestDeleteSource: responseObj.deletionRequestSource,
        }).send(res);
        }
    )
)

export default router;