import { Request, NextFunction } from 'express';
import { Source } from "../../../../../database/models/Source";
import { Users } from "../../../../../database/models/Users";
import { reasonOfGGEmissionPropertiesI, ReasonOfGHGEmsission } from "../../../../../database/models/GHG/reasonOfGHG";
import { BadRequestError, InternalError } from '../../../../../middleware/ApiError';
import GeneralService from "../../../../../database/entityService/GeneralService";

export let getChangeHistory = async (req:Request, next:NextFunction): Promise<any> => {
    try {
        let reasonGHG:reasonOfGGEmissionPropertiesI | any = await GeneralService.findNodeById(ReasonOfGHGEmsission, req.params.id);
        if (!reasonGHG) {throw new BadRequestError("Reason not found.");}
        let reasonSource: any = await GeneralService.findNodeAnyRelations(reasonGHG.dataValues.id,
                                                                                ReasonOfGHGEmsission, Source,
                                                                                'HasSource',
                                                                                next
                                                                            );
        let reasonHasUpdateSource: any | undefined = await GeneralService.findNodeAnyRelationsWithProperties(req.params.id,  undefined, ReasonOfGHGEmsission,
                                                                                Source,
                                                                                'HasUpdateSource',
                                                                                next
                                                                            );                                                                                                                                       
        let reasonAddedBy: any = await GeneralService.findNodeAnyRelations(req.params.id,
                                                                                ReasonOfGHGEmsission, Users,
                                                                                'AddedBy',
                                                                                next
                                                                            );       
        let deletionRequestSource: any = await GeneralService.findNodeAnyRelationsWithProperties(req.params.id,  undefined, ReasonOfGHGEmsission,
                                                                                Source,
                                                                                'HasRequestDeletionSource',
                                                                                next
                                                                            );                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
        let responseObj: object = {
            reasonGHG,
            reasonSource,
            reasonAddedBy,
            reasonHasUpdateSource,
            deletionRequestSource,
        }
        return responseObj
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};