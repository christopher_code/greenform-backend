import { Request, NextFunction } from 'express';
import { ReasonOfGHGEmsission, reasonOfGGEmissionPropertiesI } from "../../../../../database/models/GHG/reasonOfGHG";
import { ProductPropertiesI, Products } from "../../../../../database/models/Product";
import { Source, SourcePropertiesI } from "../../../../../database/models/Source";
import { ApprovalStatus } from "../../../../../database/models/Source";
import { generateUniqeuId } from "../../../../../middleware/generateUuid";
import { BadRequestError, InternalError } from '../../../../../middleware/ApiError';
import SourceService from "../../../../../database/entityService/SourceService";
import GeneralService from "../../../../../database/entityService/GeneralService";
import { generateDate } from "../../../../../middleware/generateDate";

export let createReason = async (req:Request, userI: string): Promise<reasonOfGGEmissionPropertiesI> => {
    try {
        let id: string = generateUniqeuId();
        let source: SourcePropertiesI | any = await SourceService.saveSource(req, 'new', 'reason', id, req.params.productId);
        if (!source) {throw new BadRequestError("Source not saved.");}

        let input = {id: id,
            ghgName: req.body.ghgName,
            reasonTitle: req.body.reasonTitle,
            description: req.body.description,
            approvalStatus: ApprovalStatus.NEW_SOURCE,
            sourceId: source.id,
            HasSource: {where: {params: {id: source.id}}},
            AddedBy: {where: {params: {id: userI}}}}

        let reason:reasonOfGGEmissionPropertiesI = await GeneralService.createNode(ReasonOfGHGEmsission, input);

        if (!reason) {throw new BadRequestError("Reason not saved.");}

        let product: ProductPropertiesI | any = await GeneralService.findNodeById(Products, req.params.productId);
        if (!product) {throw new BadRequestError("Product not found.");}

        await GeneralService.relateNodes(product, "EmitsBecauseOf", reason.id, undefined);

        return reason;
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};

export let updateReasonFields = async (req:Request): Promise<reasonOfGGEmissionPropertiesI | any> => {
    try {
        let source: SourcePropertiesI | any;
        let changedField: string = '';
        let chnagedValue: string | number | boolean = '';
        let originalValue: string | number | boolean = '';
        if (req.body.sourceTitle) {
            source = await SourceService.saveSource(req, 'update', 'reason', req.params.reasonId, req.params.productId);
            if (!source) {throw new BadRequestError("Source not saved.");}
        }
        let reason:reasonOfGGEmissionPropertiesI | any = await GeneralService.findNodeById(ReasonOfGHGEmsission, req.params.reasonId);
        if (!reason) {throw new BadRequestError("Single not found.");}
        if (req.body.reasonTitle) {
            originalValue = reason.dataValues.reasonTitle;
            reason.reasonTitle = req.body.reasonTitle;
            changedField = 'Title of Reason';
            chnagedValue = req.body.reasonTitle;
        } else if (req.body.description) {
            originalValue = reason.dataValues.description;
            reason.description = req.body.description;
            changedField = 'Description of the Reason';
            chnagedValue = req.body.description;
        } else if (req.body.ghgName) {
            originalValue = reason.dataValues.ghgName;
            reason.ghgName = req.body.ghgName;
            changedField = 'GHG Name';
            chnagedValue = req.body.ghgName;
        }
        if (req.body.sourceTitle) {
            await GeneralService.relateNodes(reason, "HasUpdateSource", source.id, {
                ChangedField: changedField,
                OriginalValue: originalValue,
                NewValue: chnagedValue,
                Date: generateDate(),
                UserId: req.params.userId,
            });
        }
        await reason.save();
        return reason
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};

export let requestReasonGHGDeletion = async (req:Request): Promise<reasonOfGGEmissionPropertiesI | any> => {
    try {
        let reason:reasonOfGGEmissionPropertiesI | any = await GeneralService.findNodeById(ReasonOfGHGEmsission, req.params.reasonId);
        if (!reason) {throw new BadRequestError("No reason found")}
        let source: SourcePropertiesI | any = await SourceService.saveSource(req, 'delete', 'reason', req.params.reasonId, req.params.productId);

        await GeneralService.relateNodes(reason, "HasRequestDeletionSource", source.id, {
            Date: generateDate(),
            UserId: req.params.userId,
            ghgName: reason.dataValues.ghgName,
            reasonTitle: reason.dataValues.reasonTitle,
            approvalStatus: reason.dataValues.approvalStatus,
            originalId: reason.dataValues.id,
            description: reason.dataValues.description,
        });

        return source;
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};

export let getReasonSources = async (req:Request, next:NextFunction): Promise<reasonOfGGEmissionPropertiesI | any> => {
    try {
        let reason:reasonOfGGEmissionPropertiesI | any = await GeneralService.findNodeById(ReasonOfGHGEmsission, req.params.reasonId);
        if (!reason) {throw new BadRequestError("Product not found.");}
        let reasonSource: reasonOfGGEmissionPropertiesI | any = await GeneralService.findNodeAnyRelations(req.params.reasonId,
                                                                                ReasonOfGHGEmsission ,Source,
                                                                                'HasSource',
                                                                                next
                                                                            );   
        let reasonUpdateSource: reasonOfGGEmissionPropertiesI | any = await GeneralService.findNodeAnyRelationsWithProperties(req.params.reasonId,
                                                                                req.params.sourceId, ReasonOfGHGEmsission,
                                                                                Source,
                                                                                'HasUpdateSource',
                                                                                next
                                                                            );  
        let reasonDeletionSource: reasonOfGGEmissionPropertiesI | any = await GeneralService.findNodeAnyRelationsWithProperties(req.params.reasonId,
                                                                                req.params.sourceId,ReasonOfGHGEmsission,
                                                                                Source,
                                                                                'HasRequestDeletionSource',
                                                                                next
                                                                            );                                                                                                                                                                                                                                                                                                                                                                                                       
        let responseObj: object = {
            reason,
            reasonSource,
            reasonUpdateSource,
            reasonDeletionSource,
        }
        return responseObj
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};
