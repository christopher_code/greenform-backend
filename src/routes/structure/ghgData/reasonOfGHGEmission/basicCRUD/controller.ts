import express from 'express';
import { createReason, updateReasonFields, requestReasonGHGDeletion, getReasonSources } from "./service";
import { reasonOfGGEmissionPropertiesI } from "../../../../../database/models/GHG/reasonOfGHG";
import { SuccessResponse } from '../../../../../middleware/ApiResponse';
import validator from '../../../../../helpers/validator';
import schema from './schema';
import asyncHanlder from "../../../../../helpers/asyncFunc";

const router = express.Router();

router.post('/new/:userId/:productId',
    asyncHanlder(async (req, res) => {
        const singleGhg = await createReason(req, req.params.userId);
        new SuccessResponse('Reason for Ghg Emission saved with success', {
            id: singleGhg.id,
            category: singleGhg,
            }).send(res);
    })
)

router.put('/update/fields/:reasonId/:userId/:productId', 
    validator(schema.updateField),
    asyncHanlder(async (req, res) => {
            let reason:reasonOfGGEmissionPropertiesI | any = await updateReasonFields(req);
            new SuccessResponse('Reason ghg data updated successfuly', {
                reasonGHG: reason
            }).send(res);
        }
    )
)

router.put('/request/deletion/:reasonId/:userId/:productId', 
validator(schema.requestDeletion),
asyncHanlder(async (req, res) => {
        await requestReasonGHGDeletion(req);
        new SuccessResponse('Single GHG request to delte was successful!', {}).send(res);
        }
    )
)

router.get('/get/source/:reasonId/:sourceId', 
    asyncHanlder(async (req, res, next) => {
        let responseObj: reasonOfGGEmissionPropertiesI | any = await getReasonSources(req, next);
        new SuccessResponse('Full Reason sources received successfuly', {
            reason: responseObj.reason.dataValues,
            source: responseObj.reasonSource,
            updateSource: responseObj.reasonUpdateSource,
            deletionSource: responseObj.reasonDeletionSource,
        }).send(res);
        }
    )
)

export default router;