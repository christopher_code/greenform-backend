import Joi from '@hapi/joi';

export default {
  createNew: Joi.object().keys({
    sourceTitle: Joi.string().required().min(5),
    sourceLink: Joi.string().uri().required().min(5),
    sourceDescription: Joi.string().required().min(10),
    ghgName: Joi.string().required().min(2),
    description: Joi.string().required().min(10),
    reasonTitle: Joi.string().optional().min(5),
  }),
  updateField: Joi.object().keys({
    sourceTitle: Joi.string().required().min(5),
    sourceLink: Joi.string().uri().required().min(5),
    sourceDescription: Joi.string().required().min(20),
    ghgName: Joi.string().optional().min(3),
    description: Joi.string().optional().min(20),
    reasonTitle: Joi.string().optional().min(5),
  }),
  requestDeletion:  Joi.object().keys({
    sourceTitle: Joi.string().required().min(5),
    sourceLink: Joi.string().uri().required().min(5),
    sourceDescription: Joi.string().required().min(20),
  }),
};
