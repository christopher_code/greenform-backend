import { Request, Response, NextFunction } from 'express';
import { Source } from "../../../database/models/Source";
import { Users } from "../../../database/models/Users";
import { BadRequestError, InternalError } from '../../../middleware/ApiError';
import SourceService from '../../../database/entityService/SourceService';
import { SourcePropertiesI } from '../../../database/models/Source';
import { sourceRelationship } from '../../../middleware/sourceRelationship';
import GeneralService from "../../../database/entityService/GeneralService";

export let getSourceById = async (next: NextFunction, sourceId: string): Promise<SourcePropertiesI | any> => {
    try {
        let source:SourcePropertiesI | any = await GeneralService.findNodeById(Source ,sourceId);
        if (!source) {throw new BadRequestError("Source not found.");}
        let sourceAddedBy: SourcePropertiesI | any = await GeneralService.findNodeAnyRelations(sourceId, Source, Users, 'AddedBy', next);       
        let sourceApprovedFirstBy: SourcePropertiesI | any = await SourceService.findSourceAnyRelationsWithProoperties(sourceId, Users, 'ApprovedFirstBy', next);   
        let finalApproveBy: SourcePropertiesI | any = await SourceService.findSourceAnyRelationsWithProoperties(sourceId, Users, 'ApprovedFinalBy', next);     
        let approvedByNewUserBy: SourcePropertiesI | any = await SourceService.findSourceAnyRelationsWithProoperties(sourceId, Users, 'ApprovedByNewUser', next);   
        let notApprovedOnceBy: SourcePropertiesI | any = await SourceService.findSourceAnyRelationsWithProoperties(sourceId, Users, 'NotApprovedOnce', next);    
        let notApprovedAgainBy: SourcePropertiesI | any = await SourceService.findSourceAnyRelationsWithProoperties(sourceId, Users, 'NotApprovedTwice', next);   
        let notApprovedByNewUser: SourcePropertiesI | any = await SourceService.findSourceAnyRelationsWithProoperties(sourceId, Users, 'NotApprovedNewUser', next);                                                                                                                                                                                                                                                                                                        
        let response: object = {
            source,
            sourceAddedBy,
            sourceApprovedFirstBy,
            finalApproveBy,
            approvedByNewUserBy,
            notApprovedByNewUser,
            notApprovedOnceBy,
            notApprovedAgainBy,
        }
        return response
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};

export let getSourceByStatus = async (req:Request, res:Response, next:NextFunction, status: string): Promise<SourcePropertiesI | any> => {
    try {
        let reponse:SourcePropertiesI | any = await SourceService.findSourceByStatus(status);    
        if (!reponse) {throw new BadRequestError("Source not found.");}
        return reponse;
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};

export let getRandomSourcesSourceByType = async (req: Request, res:Response, next: NextFunction): Promise<SourcePropertiesI[] | any> => {
    try {
        let sources:SourcePropertiesI[] | any = await SourceService.findRandomSources(req); 
        if (!sources) {throw new BadRequestError("Source not found.");}
        let sourceWithRel: object[] = [];

        for (let i = 0; i<3; i++){
            let sourceArr: object[] = [];
            sourceArr.push(sources[i]);
            let relationNode: SourcePropertiesI | any = await sourceRelationship(sources[i].dataValues.relType, sources[i].dataValues.relId);
            let sourceRes = { 
                source: sources[i].dataValues, 
                sourceRelId: sources[i].dataValues.relId,
                sourceRelType: sources[i].dataValues.relType,
                relationNode: relationNode,
            }
            sourceWithRel.push(sourceRes);
        }                                                                                                                                                                                                                 
        return sourceWithRel;
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};

export let updateSourceStatus = async (id: string, status: string): Promise<SourcePropertiesI | any> => {
    try {
        await Source.update(
            {
                status: status
            },
            {
                where: {
                    id: id
                },
            }
        );
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};
