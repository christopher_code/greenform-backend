import Joi from '@hapi/joi';

export default {
  createNew: Joi.object().keys({
    title: Joi.string().required().min(5),
    link: Joi.string().uri().required().min(5),
    description: Joi.string().required().min(20),
  }),
};