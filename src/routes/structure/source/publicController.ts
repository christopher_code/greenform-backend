import express from 'express';
import { SuccessResponse } from '../../../middleware/ApiResponse';
import { getRandomSourcesSourceByType, getSourceById, getSourceByStatus } from "./service";
import asyncHandler from "../../../helpers/asyncFunc";
import SourceService from "../../../database/entityService/SourceService";
import { Users } from "../../../database/models/Users";
import { SourcePropertiesI } from "../../../database/models/Source"

const router = express.Router();

router.get('/get/id/:sourceId', 
    asyncHandler(async (req, res, next) => {
        let repsonse:SourcePropertiesI | any = await getSourceById(next, req.params.sourceId);
        new SuccessResponse('Source with relationships received successfuly', {
            source: repsonse.source.dataValues,
            addedBy: {addedBy: repsonse.sourceAddedBy[0].name, id: repsonse.sourceAddedBy[0].id},
            approvedFirstBy: repsonse.sourceApprovedFirstBy.user ? repsonse.sourceApprovedFirstBy : {},
            approvedFinalBy: repsonse.finalApproveBy.user ? repsonse.finalApproveBy : {},
            approvedByNewUser: repsonse.approvedByNewUserBy.user ? repsonse.approvedByNewUserBy : {},
            notApprovedByNewUser: repsonse.notApprovedByNewUser.user ? repsonse.notApprovedByNewUser : {},
            notApprovedOnceBy: repsonse.notApprovedOnceBy.user ? repsonse.notApprovedOnceBy:  {},
            notApprovedAgainBy: repsonse.notApprovedAgainBy.user ? repsonse.notApprovedAgainBy : {},
        }).send(res);
        })
);

router.get('/get/status/:status', 
    asyncHandler(async (req, res, next) => {
        let repsonse:SourcePropertiesI | any = await getSourceByStatus(req, res, next, req.params.status);
        new SuccessResponse('Source by status received successfuly', {
            source: repsonse,
        }).send(res);
        })
);

router.get('/get/random/:typeRel/:role/:sourceType',
    asyncHandler(async (req, res, next) => {
        const source: SourcePropertiesI[] | any = await getRandomSourcesSourceByType(req, res, next);
        new SuccessResponse('Source saved with success', [
            source[0],
            source[1],
            source[2],
        ]).send(res);
    })
)

router.get('/get/props/:id/:alias',
    asyncHandler(async (req, res, next) => {
        const source: SourcePropertiesI | any = await SourceService.findSourceAnyRelationsWithProoperties(req.params.id, Users, req.params.alias, next);
        new SuccessResponse('Source found with success', {
            source: source
    }).send(res);
    })
)

export default router;