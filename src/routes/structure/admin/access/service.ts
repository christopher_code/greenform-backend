import { Admin } from "../../../../database/models/Admin";
import { generateUniqeuId } from "../../../../middleware/generateUuid";
import { InternalError } from '../../../../middleware/ApiError';
import { BadRequestError } from '../../../../middleware/ApiError';
import GeneralService from "../../../../database/entityService/GeneralService";
import { Users } from "../../../../database/models/Users";
import bcrypt from "bcrypt";
import {Request} from "express";
import { adminMail } from "../../../../../config";

export const saveAdmin = async (req:Request): Promise<void> => {
    const saltRounds = 12;
    let passwordHash1:string;
    let passwordHash2:string;

    try {
        passwordHash1 = await bcrypt.hash(req.body.password1, saltRounds);
        passwordHash2 = await bcrypt.hash(req.body.password2, saltRounds);

        Admin.createOne({
            id: generateUniqeuId(),
            email: adminMail,
            password1: passwordHash1,
            password2: passwordHash2
        })
    }
    catch (err) {
        throw new BadRequestError();
    }
    
};

export const changeUserRole = async (req: Request): Promise<void> => {
    try {
        let user = await GeneralService.findNodeById(Users, req.params.id);
        user.roleStatus = 'superuser';
        await user.save();
    }   
    catch (err) {
        throw new InternalError();
    }    
};
