import express from 'express';
import { SuccessResponse } from '../../../../middleware/ApiResponse';
import { changeUserRole } from "./service";
import { saveAdmin } from "./service";
import asyncHandler from "../../../../helpers/asyncFunc";

const router = express.Router();

router.put('/make/user/superuser/:id',
    asyncHandler(async (req, res) => {
        await changeUserRole(req);
        new SuccessResponse('User role upgraded successfully.', {}).send(res);
    })
)

export default router;
        