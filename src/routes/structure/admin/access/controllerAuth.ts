import express from 'express';
import { SuccessResponse } from '../../../../middleware/ApiResponse';
import asyncHandler from "../../../../helpers/asyncFunc";
import { adminToken, adminMail } from '../../../../../config';
import GeneralService from '../../../../database/entityService/GeneralService';
import { Admin } from "../../../../database/models/Admin"
import { BadRequestError } from '../../../../middleware/ApiError';
import bcrypt from "bcrypt";
import jwt from 'jsonwebtoken';
import validator from '../../../../helpers/validator';
import schema from './schema';
// import { saveAdmin } from "./service";

const router = express.Router();

router.post('/login',
    validator(schema.login),
    asyncHandler(async (req, res) => {
        const admin = await GeneralService.findNodeByEmail(Admin, req.body.email);

        if (!admin.password1) throw new BadRequestError('Check credentials.');

        const match = await bcrypt.compare(req.body.password1, admin.password1);
        if (!match) return res.status(401).send("Login failed due to wrong email or password!");

        if (!admin.password2) throw new BadRequestError('Check credentials.');

        const matchNum = await bcrypt.compare(req.body.password2, admin.password2);
        if (!matchNum) return res.status(401).send("Login failed due to wrong email or password!");

        const token = jwt.sign(
            { theUserMail: adminMail },
            adminToken);

        new SuccessResponse('Login Success', {
            id: admin.id,
            token: token,
            role: "admin-2021-june-greenformed-#14101998",
        }).send(res);
    })
)

// router.post('/signup',
//     asyncHandler(async (req, res) => {
//         await saveAdmin(req);
//         new SuccessResponse('Signup Success', {}).send(res);
//     })
// )

export default router;