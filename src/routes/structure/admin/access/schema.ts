import Joi from '@hapi/joi';

export default {
  login: Joi.object().keys({
    email: Joi.string().required(),
    password1: Joi.string().required().min(8),
    password2: Joi.string().required().min(8),
  }),
};
