import express from 'express';

import signup from "./structure/user/access/controller";
import login from "./structure/user/access/controllerLogin";
import userData from "./structure/user/userData/controller"; 

import adminAuth from "./structure/admin/access/controllerAuth";
import admin from "./structure/admin/access/controller";

import oSource from "./structure/source/publicController";

import product from "./structure/product/basicCRUD/controller";
import productPublic from "./structure/product/basicCRUD/publicController";
import changeHistoryProduct from "./structure/product/changeHistory/publicController";
import search from "./structure/product/basicCRUD/search/controller";

import category from "./structure/category/controller";
import publicCategory from "./structure/category/publicController";

import seller from "./structure/seller/basicCRUD/controller";
import publicSeller from "./structure/seller/basicCRUD/publicController";
import sellerChangeHistory from "./structure/seller/changeHistory/publicController";

import totalGhgData from "./structure/ghgData/totalGhg/basicCRUD/controller";
import changeHisTotal from "./structure/ghgData/totalGhg/changeHistory/publicController";

import singleGhgData from "./structure/ghgData/singleGhg/basicCRUD/controller";
import singleChangeHistpry from "./structure/ghgData/singleGhg/changeHistory/publicController";

import reasonForGhgData from "./structure/ghgData/reasonOfGHGEmission/basicCRUD/controller";
import changeHisReason from "./structure/ghgData/reasonOfGHGEmission/changeHistory/publicController";

import approveSource from "./structure/user/dataVerification/controller";

import tokenBlacklist from "./structure/blacklisting/controller";

import test from "./structure/sst/controllerRoutes";

import verifyAdminToken from '../middleware/verifyAdminToken';
import rateLimiter from '../middleware/rateLimiter';
import verifyToken from "../middleware/VerifyToken";

const router = express.Router();

router.use('/signup', signup);
router.use('/login', rateLimiter,login);
router.use('/user', userData);

router.use('/admin/action/', verifyAdminToken, admin);
router.use('/admin', adminAuth);

router.use('/o/source', oSource);

router.use('/product', rateLimiter, verifyToken, product);
router.use('/o/product', productPublic);
router.use('/o/changehis', changeHistoryProduct);
router.use('/o/search', search);

router.use('/category', verifyToken, category);
router.use('/o/category', publicCategory);

router.use('/seller', verifyToken, seller);
router.use('/o/seller', publicSeller);
router.use('/o/sel/changehis/', sellerChangeHistory);

router.use('/ghg/total', verifyToken, totalGhgData);
router.use('/o/total/changehis', changeHisTotal);

router.use('/ghg/single', verifyToken, singleGhgData);
router.use('/o/ghg/single/changehis', singleChangeHistpry);

router.use('/ghg/reason', verifyToken, reasonForGhgData);
router.use('/o/reason/changehis', changeHisReason);

router.use('/approval/source', rateLimiter, verifyToken, approveSource);

router.use('/blacklist', tokenBlacklist);

router.use('/encrypt', test);

export default router;