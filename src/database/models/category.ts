import { neogma } from '../../app';
import { ModelFactory, NeogmaInstance, ModelRelatedNodesI } from 'neogma';
import { Users, UsersPropertiesI } from "./Users";

export type CategoryPropertiesI = {
    id: string,
    typeOfCategory: string;
    createdAt: number;
};

export interface CategoryRelatedNodesI {
    AddedBy: ModelRelatedNodesI<typeof Users, UsersPropertiesI>;
}

export type CategoryInstance = NeogmaInstance<
    CategoryPropertiesI,
    CategoryRelatedNodesI
>;

export let Categories = ModelFactory<CategoryPropertiesI, CategoryRelatedNodesI>(
    {
        label: 'Category',
        primaryKeyField: 'typeOfCategory',
        schema: {
            id: {
                type: 'string',
                minLength: 5,
                required: true,
            },
            typeOfCategory : {
                type: 'string',
                minLength: 5,
                required: true,
            },
            createdAt: {
                type: 'number',
                required: true,
            },
        },
        relationships: {
            AddedBy: {
                model: Users,
                direction: 'out',
                name: 'ADDED_BY',
            },
        },
    },
    neogma,
);
