import { neogma } from '../../app';
import { ModelFactory, NeogmaInstance, ModelRelatedNodesI } from 'neogma';
import { Categories, CategoryPropertiesI } from './category';
import { Source, SourcePropertiesI } from "./Source";
import { Sellers, SellerPropertiesI } from "./Seller";
import { Users, UsersPropertiesI } from "./Users";
import { totalGHGEmissions, totalGHGEmissionsPropertiesI } from "./GHG/totalGHG";
import { singleGHGEmissions, singleGHGEmissionPropertiesI } from "./GHG/singleGHG";
import { ReasonOfGHGEmsission, reasonOfGGEmissionPropertiesI } from "./GHG/reasonOfGHG";

export type ProductPropertiesI = {
    id: string, 
    name: string, 
    description: string,
    linkToBuy?: string, 
    costPerProduct?: number,
    createdAt?: number,
    approvalStatus: string,
    sourceId: string,
};

export interface ProductRelatedNodesI {
    HasAlternativeProduct: ModelRelatedNodesI<typeof Products, ProductPropertiesI>;
    MadeOfRawMaterial: ModelRelatedNodesI<typeof Products, ProductPropertiesI>;
    HasSource: ModelRelatedNodesI<typeof Source, SourcePropertiesI>;
    HasUpdateSource:  ModelRelatedNodesI<typeof Source, SourcePropertiesI>;
    HasRequestDeletionSource: ModelRelatedNodesI<typeof Source, SourcePropertiesI>;
    BelongsToCategory: ModelRelatedNodesI<typeof Categories, CategoryPropertiesI>;
    CanBeBoughtFrom: ModelRelatedNodesI<typeof Sellers, SellerPropertiesI>;
    AddedBy: ModelRelatedNodesI<typeof Users, UsersPropertiesI>;
    EmitsInTotal: ModelRelatedNodesI<typeof totalGHGEmissions, totalGHGEmissionsPropertiesI>;
    EmitsGHG: ModelRelatedNodesI<typeof singleGHGEmissions, singleGHGEmissionPropertiesI>;
    EmitsBecauseOf: ModelRelatedNodesI<typeof ReasonOfGHGEmsission, reasonOfGGEmissionPropertiesI>;
}

export type ProductInstance = NeogmaInstance<
    ProductPropertiesI,
    ProductRelatedNodesI
>;

export let Products = ModelFactory<ProductPropertiesI, ProductRelatedNodesI>(
    {
        label: 'Product',
        primaryKeyField: 'name',
        schema: {
            name : {
                type: 'string',
                minLength: 2,
                required: true,
            },
            id: {
                type: 'string',
                minLength: 5,
                required: true,
            },
            description: {
                type: 'string',
                minLength: 20,
                required: true,
            },
            linkToBuy: {
                type: 'string',
                minLength: 5,
            },
            costPerProduct: {
                type: 'number',
                required: true,
            },
            createdAt: {
                type: 'number',
                required: true,
            },
            approvalStatus: {
                type: 'string',
                required: true,
            },
            sourceId: {
                type: 'string',
                required: true,
            }
        },
        relationships: {
            HasAlternativeProduct: {
                model: "self",
                direction: 'out',
                name: 'HAS_ALTERNATIVE_PRODUCT',
            },
            MadeOfRawMaterial: {
                model: "self",
                direction: 'out',
                name: 'MADE_OF_RAW_MATERIAL',
            },
            HasSource: {
                model: Source,
                direction: 'out',
                name: 'HAS_SOURCE',
            },
            HasUpdateSource: {
                model: Source,
                direction: 'out',
                name: 'HAS_UPDATE_SOURCE',
                properties: {
                    ChangedField: {
                        property: 'ChangedField',
                        schema: {type: 'string'}
                    },
                    OriginalValue: {
                        property: 'OriginalValue',
                        schema: {type: 'any'}
                    },
                    NewValue: {
                        property: 'NewValue',
                        schema: {type: 'any'}
                    },
                    Date: {
                        property: 'Date',
                        schema: {type: 'number'}
                    },
                    UserId: {
                        property: 'UserId',
                        schema: {type: 'string'}
                    },
                },
            },
            HasRequestDeletionSource: {
                model: Source,
                direction: 'out',
                name: 'HAS_REQUEST_DELETION_SOURCE',
                properties: {
                    Name: {
                        property: 'OriginalName',
                        schema: {type: 'string'}
                    },
                    Description: {
                        property: 'OriginalDescription',
                        schema: {type: 'string'}
                    },
                    LinkToBuy: {
                        property: 'OriginalLinkToBuy',
                        schema: {type: 'string'}
                    },
                    CreatedAt: {
                        property: 'OriginalCreatedAt',
                        schema: {type: 'number'}
                    },
                    approvalStatus: {
                        property: 'OriginalApprovalStatus',
                        schema: {type: 'string'}
                    },
                    originalId: {
                        property: 'OriginalId',
                        schema: {type: 'string'}
                    },
                    Date: {
                        property: 'Date',
                        schema: {type: 'number'}
                    },
                    UserId: {
                        property: 'UserId',
                        schema: {type: 'string'}
                    },
                },
            },
            BelongsToCategory: {
                model: Categories,
                direction: 'out',
                name: 'BELONGS_TO_CATEGORY',
            },
            CanBeBoughtFrom: {
                model: Sellers,
                direction: 'out',
                name: 'CAN_BE_BOUGHT_FROM',
            },
            AddedBy: {
                model: Users,
                direction: 'out',
                name: 'ADDED_BY',
            },
            EmitsInTotal: {
                model: totalGHGEmissions,
                direction: 'out',
                name: 'EMITS_IN_TOTAL',
            },
            EmitsGHG: {
                model: singleGHGEmissions,
                direction: 'out',
                name: 'EMITS',
            },
            EmitsBecauseOf: {
                model: ReasonOfGHGEmsission,
                direction: 'out',
                name: 'EMITS_BECAUSE_OF',
            },
        },
    },
    neogma,
);