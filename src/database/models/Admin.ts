import { neogma } from '../../app';

import { ModelFactory } from 'neogma';

export type AdminPropertiesI = {
    id: string;
    email: string;
    password1: string;
    password2: string;
};

export interface AdminRelatedNodesI {}

export const Admin = ModelFactory<AdminPropertiesI, AdminRelatedNodesI>(
    {
        label: 'Admin',
        primaryKeyField: 'email',
        schema: {
            id: {
                type: 'string',
                minLength: 5,
                required: true, 
            },
            email: {
                type: 'string',
                minLength: 5,
                required: true, 
            },
            password1: {
                type: 'string',
                required: true,
            },
            password2: {
                type: 'string',
                required: true,
            },
        },
    },
    neogma,
);