import { neogma } from '../../../app';
import { ModelFactory, NeogmaInstance, ModelRelatedNodesI } from 'neogma';
import { Source, SourcePropertiesI } from "../Source";
import { Users, UsersPropertiesI } from "../Users";

export type totalGHGEmissionsPropertiesI = {
    id: string,
    ghgPerKG: number,
    approvalStatus: string,
    sourceId: string,
};

export interface totalGHGEmissionsRelatedNodesI {
    HasSource: ModelRelatedNodesI<typeof Source, SourcePropertiesI>;
    HasUpdateSource: ModelRelatedNodesI<typeof Source, SourcePropertiesI>;
    HasRequestDeletionSource: ModelRelatedNodesI<typeof Source, SourcePropertiesI>;
    AddedBy: ModelRelatedNodesI<typeof Users, UsersPropertiesI>;
    UpdatedBy: ModelRelatedNodesI<typeof Users, UsersPropertiesI>;
}

export type totalGHGEmissionsInstance = NeogmaInstance<
    totalGHGEmissionsPropertiesI,
    totalGHGEmissionsRelatedNodesI
>;

export const totalGHGEmissions = ModelFactory<totalGHGEmissionsPropertiesI, totalGHGEmissionsRelatedNodesI>(
    {
        label: 'TotoalGHGEmissionsPerKG',
        primaryKeyField: 'id',
        schema: { 
            id: {
                type: 'string',
                minLength: 5,
                required: true, 
            },
            ghgPerKG: {
                type: 'number',
                required: true,
            },
            approvalStatus: {
                type: 'string',
                required: true,
            },
            sourceId: {
                type: 'string',
                required: true,
            },
        },
        relationships: {
            HasSource: {
                model: Source,
                direction: 'out',
                name: 'HAS_SOURCE',
            },
            HasUpdateSource: {
                model: Source,
                direction: 'out',
                name: 'HAS_UPDATE_SOURCE',
                properties: {
                    ChangedField: {
                        property: 'ChangedField',
                        schema: {type: 'any'}
                    },
                    OriginalValue: {
                        property: 'OriginalValue',
                        schema: {type: 'any'}
                    },
                    NewValue: {
                        property: 'NewValue',
                        schema: {type: 'any'}
                    },
                    Date: {
                        property: 'Date',
                        schema: {type: 'number'}
                    },
                    UserId: {
                        property: 'UserId',
                        schema: {type: 'string'}
                    },
                },
            },
            HasRequestDeletionSource: {
                model: Source,
                direction: 'out',
                name: 'HAS_REQUEST_DELETION_SOURCE',
                properties: {
                    ghgPerKG: {
                        property: 'ghgPerKG',
                        schema: {type: 'number'}
                    },
                    approvalStatus: {
                        property: 'OriginalApprovalStatus',
                        schema: {type: 'string'}
                    },
                    originalId: {
                        property: 'OriginalId',
                        schema: {type: 'string'}
                    },
                    Date: {
                        property: 'Date',
                        schema: {type: 'number'}
                    },
                    UserId: {
                        property: 'UserId',
                        schema: {type: 'string'}
                    },
                },
            },
            AddedBy: {
                model: Users,
                direction: 'out',
                name: 'ADDED_BY',
            },
        },
    },
    neogma,
);