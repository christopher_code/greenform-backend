import { neogma } from '../../app';

import { ModelFactory } from 'neogma';

export type TokenPropertiesI = {
    token: any
};

export interface TokenRelatedNodesI {}

export const Tokens = ModelFactory<TokenPropertiesI, TokenRelatedNodesI>(
    {
        label: 'Token',
        primaryKeyField: 'token',
        schema: {
            token: {
                type: 'any',
                minLength: 20,
                required: true, 
            },
        },
    },
    neogma,
);