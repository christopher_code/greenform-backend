import { neogma } from '../../app';
import { ModelFactory, NeogmaInstance, ModelRelatedNodesI } from 'neogma';
import { Categories, CategoryPropertiesI } from './category';
import { Source, SourcePropertiesI } from "./Source";
import { Users, UsersPropertiesI } from "./Users";

export type SellerPropertiesI = {
    id: string, 
    name: string, 
    link: string,
    createdAt: number,
    approvalStatus: string,
    sourceId: string,
};

export interface SellerRelatedNodesI {
    HasSource: ModelRelatedNodesI<typeof Source, SourcePropertiesI>;
    BelongsToCategory: ModelRelatedNodesI<typeof Categories, CategoryPropertiesI>;
    AddedBy: ModelRelatedNodesI<typeof Users, UsersPropertiesI>;
    UpdatedBy: ModelRelatedNodesI<typeof Users, UsersPropertiesI>;
    HasUpdateSource: ModelRelatedNodesI<typeof Source, SourcePropertiesI>;
    HasRequestDeletionSource: ModelRelatedNodesI<typeof Source, SourcePropertiesI>;
}

export type SellerInstance = NeogmaInstance<
    SellerPropertiesI,
    SellerRelatedNodesI
>;

export const Sellers = ModelFactory<SellerPropertiesI, SellerRelatedNodesI>(
    {
        label: 'Seller',
        primaryKeyField: 'name',
        schema: {
            name : {
                type: 'string',
                minLength: 5,
                required: true,
            },
            id: {
                type: 'string',
                minLength: 5,
                required: true,
            },
            link: {
                type: 'string',
                minLength: 5,
                required: true,
            },
            createdAt: {
                type: 'number',
                required: true,
            },
            approvalStatus: {
                type: 'string',
                required: true,
            },
            sourceId: {
                type: 'string',
                required: true,
            },
        },
        relationships: {
            HasSource: {
                model: Source,
                direction: 'out',
                name: 'HAS_SOURCE',
            },
            HasUpdateSource: {
                model: Source,
                direction: 'out',
                name: 'HAS_UPDATE_SOURCE',
                properties: {
                    ChangedField: {
                        property: 'ChangedField',
                        schema: {type: 'string'}
                    },
                    OriginalValue: {
                        property: 'OriginalValue',
                        schema: {type: 'any'}
                    },
                    NewValue: {
                        property: 'NewValue',
                        schema: {type: 'any'}
                    },
                    Date: {
                        property: 'Date',
                        schema: {type: 'number'}
                    },
                    UserId: {
                        property: 'UserId',
                        schema: {type: 'string'}
                    },
                },
            },
            HasRequestDeletionSource: {
                model: Source,
                direction: 'out',
                name: 'HAS_REQUEST_DELETION_SOURCE',
                properties: {
                    Name: {
                        property: 'OriginalName',
                        schema: {type: 'string'}
                    },
                    Link: {
                        property: 'OriginalLink',
                        schema: {type: 'string'}
                    },
                    CreatedAt: {
                        property: 'OriginalCreatedAt',
                        schema: {type: 'number'}
                    },
                    approvalStatus: {
                        property: 'OriginalApprovalStatus',
                        schema: {type: 'string'}
                    },
                    originalId: {
                        property: 'OriginalId',
                        schema: {type: 'string'}
                    },
                    Date: {
                        property: 'Date',
                        schema: {type: 'number'}
                    },
                    UserId: {
                        property: 'UserId',
                        schema: {type: 'string'}
                    },
                },
            },
            BelongsToCategory: {
                model: Categories,
                direction: 'out',
                name: 'BELONGS_TO_CATEGORY',
            },
            AddedBy: {
                model: Users,
                direction: 'out',
                name: 'ADDED_BY',
            },
        },
    },
    neogma,
);