import { neogma } from '../../app';
import { ModelFactory, NeogmaInstance, ModelRelatedNodesI } from 'neogma';
import { Users, UsersPropertiesI } from './Users';

export enum ApprovalStatus {
    APPROVED_FINAL_BY = "approvedFinal",
    APPROVED_FIRST_BY = 'approvedFirst',
    APPROVED_BY_NEW_USER = 'approvedByNewUser',
    NOT_APPROVED_ONCE_BY = "notApprovedOnce",
    NOT_APPROVED_AGAIN_BY = "notApprovedAgain",
    NOT_APPROVED_NEW_USER = "notApprovedNewUser",
    NEW_SOURCE = 'newSource',
    REQ_CHECK = 'requestToCheck',
}

export type SourcePropertiesI = {
    id: string,
    title: string,
    link: string;
    description: string,
    status: string,
    createdAt?: number,
    sourceType: string,
    relType: string,
    relId: string,
    productId: string,
};

export interface SourceRelatedNodesI {
    AddedBy: ModelRelatedNodesI<typeof Users, UsersPropertiesI>;
    ApprovedByNewUser: ModelRelatedNodesI<typeof Users, UsersPropertiesI>;
    ApprovedFirstBy: ModelRelatedNodesI<typeof Users, UsersPropertiesI>;
    ApprovedFinalBy: ModelRelatedNodesI<typeof Users, UsersPropertiesI>;
    NotApprovedOnce: ModelRelatedNodesI<typeof Users, UsersPropertiesI>;
    NotApprovedTwice: ModelRelatedNodesI<typeof Users, UsersPropertiesI>;
    NotApprovedNewUser: ModelRelatedNodesI<typeof Users, UsersPropertiesI>;
    CheckRequestBy: ModelRelatedNodesI<typeof Users, UsersPropertiesI>;
}

export type SourceInstance = NeogmaInstance<
    SourcePropertiesI,
    SourceRelatedNodesI
>;

export const Source = ModelFactory<SourcePropertiesI, SourceRelatedNodesI>(
    {
        label: 'Source',
        primaryKeyField: 'title',
        schema: {
            id: {
                type: 'string',
                minLength: 5,
                required: true,
            },
            title : {
                type: 'string',
                minLength: 5,
                required: true,
            },
            link: {
                type: 'string',
                minLength: 5,
                required: true,
            },
            description: {
                type: 'string',
                minLength: 20,
                required: true,
            },
            status: {
                type: 'string',
                minLength: 8,
                required: true,
            },
            createdAt: {
                type: 'number',
                required: true,
            },
            sourceType: {
                type: 'string',
                required: true,
            },
            relType: {
                type: 'string',
                required: true,
            },
            relId: {
                type: 'string',
                required: true,
            },
            productId: {
                type: 'string',
                required: true,
            }
        },
        relationships: {
            AddedBy: {
                model: Users,
                direction: 'out',
                name: 'ADDED_BY',
            },
            ApprovedFirstBy: {
                model: Users,
                direction: 'out',
                name: 'APPROVED_FIRST_BY',
                properties: {
                    Date: {
                        property: 'date',
                        schema: {type: 'number'}
                    },
                    Description: {
                        property: 'Description',
                        schema: {type: 'string'}
                    },
                    ApprovalStatus: {
                        property: 'ApprovalStatus',
                        schema: {type: 'boolean'}
                    },
                },
            },
            ApprovedFinalBy: {
                model: Users,
                direction: 'out',
                name: 'APPROVED_FINAL_BY',
                properties: {
                    Date: {
                        property: 'date',
                        schema: {type: 'number'}
                    },
                    Description: {
                        property: 'Description',
                        schema: {type: 'string'}
                    },
                    ApprovalStatus: {
                        property: 'ApprovalStatus',
                        schema: {type: 'boolean'}
                    },
                },
            },
            ApprovedByNewUser: {
                model: Users,
                direction: 'out',
                name: 'APPROVED_BY_NEW_USER',
                properties: {
                    Date: {
                        property: 'date',
                        schema: {type: 'number'}
                    },
                    Description: {
                        property: 'Description',
                        schema: {type: 'string'}
                    },
                    ApprovalStatus: {
                        property: 'ApprovalStatus',
                        schema: {type: 'boolean'}
                    },
                }

            },
            NotApprovedOnce: {
                model: Users,
                direction: 'out',
                name: 'NOT_APPROVED_ONCE_BY',
                properties: {
                    Date: {
                        property: 'date',
                        schema: {type: 'number'}
                    },
                    Description: {
                        property: 'Description',
                        schema: {type: 'string'}
                    },
                    ApprovalStatus: {
                        property: 'ApprovalStatus',
                        schema: {type: 'boolean'}
                    },
                },
            },
            NotApprovedTwice: {
                model: Users,
                direction: 'out',
                name: 'NOT_APPROVED_AGAIN_BY',
                properties: {
                    Date: {
                        property: 'date',
                        schema: {type: 'number'}
                    },
                    Description: {
                        property: 'Description',
                        schema: {type: 'string'}
                    },
                    ApprovalStatus: {
                        property: 'ApprovalStatus',
                        schema: {type: 'boolean'}
                    },
                },
            },
            NotApprovedNewUser: {
                model: Users,
                direction: 'out',
                name: 'NOT_APPROVED_NEW_USER',
                properties: {
                    Date: {
                        property: 'date',
                        schema: {type: 'number'}
                    },
                    Description: {
                        property: 'Description',
                        schema: {type: 'string'}
                    },
                    ApprovalStatus: {
                        property: 'ApprovalStatus',
                        schema: {type: 'boolean'}
                    },
                },
            },
            CheckRequestBy: {
                model: Users,
                direction: 'out',
                name: 'CHECK_REQUEST_BY',
                properties: {
                    Date: {
                        property: 'Date',
                        schema: {
                            type: 'string'
                        }
                    },
                    Description: {
                        property: 'Description',
                        schema: {
                            type: 'string'
                        }
                    },
                    ApprovalStatus: {
                        property: 'ApprovalStatus',
                        schema: {
                            type: 'boolean'
                        }
                    }
                },
            },
        },
    },
    neogma,
);