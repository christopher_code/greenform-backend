import { neogma } from '../../app';

import { ModelFactory } from 'neogma';

export enum RoleStauts {
    NEW_USER = "user",
    SUPER_USER = 'superuser',
}

export type UsersPropertiesI = {
    id: string,
    email: string;
    name?: string;
    company?: string;
    password: string;
    roleStatus: string;
    approvals: number;
    createdAt?: number;
};

export interface UsersRelatedNodesI {}

export const Users = ModelFactory<UsersPropertiesI, UsersRelatedNodesI>(
    {
        label: 'User',
        primaryKeyField: 'email',
        schema: {
            id: {
                type: 'string',
                minLength: 5,
                required: true, 
            },
            email: {
                type: 'string',
                minLength: 5,
                required: true,
            },
            name: {
                type: 'string',
                minLength: 1,
                required: true,
            },
            company: {
                type: 'string',
                minLength: 2,
                required: false,
            },
            password: {
                type: 'string',
                minLength: 8,
                required: true,
            },
            roleStatus: {
                type: 'string',
                minLength: 2,
                required: true,
            },
            approvals: {
                type: 'number',
                required: true,
            },
            createdAt: {
                type: 'number',
                required: true,
            },
        },
    },
    neogma,
);