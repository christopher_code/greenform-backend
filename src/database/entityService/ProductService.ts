import { neogma } from "../../app";
import { QueryBuilder } from 'neogma';
import { Request } from 'express';
import { Products, ProductPropertiesI } from '../models/Product';
import { Sellers } from "../models/Seller";
import { InternalError } from '../../middleware/ApiError';
import { Categories } from "../models/category";
import GeneralService from "./GeneralService";


export default class ProductService {

    public static async findProductByParam(searchBy: string, parameter: any, approval: string):Promise<ProductPropertiesI> {
        try {
            let resp: any;
            if (approval === 'approved') {
                if (searchBy === 'name') {
                    resp = await Products.findMany({
                        where: {
                            name: parameter,
                            status: 'approvedFinal',
                        },
                    });
            }
        } else {
        if (searchBy === 'name') {
            resp = await Products.findMany({
                where: {
                    name: parameter,
                },
            });
        } else if (searchBy === 'seller') {
            resp = await Sellers.findMany({
                where: {
                    name: parameter,
                },
            })
        } else if (searchBy === 'category') {
            resp = await Categories.findMany({
                where: {
                    name: parameter,
                },
            })
            }
        }
        return resp;
        }catch (err) {
            throw new InternalError('Something went wrong.');
        }
    }

    public static async findRelationshipBetweenTwoGivenNodes(id1: string, id2: string | any, alias: string):Promise<ProductPropertiesI | any> {
        try {
            let product: ProductPropertiesI | any = await GeneralService.findNodeById(Products, id1);
            const relationships:any = product.findRelationships({
                alias: alias,
                where: {
                    target: {
                        id: id2
                    },
                },
                limit: 1
            });
            return relationships;
        } catch (err) {
            throw new InternalError('Something went wrong.');
        }
    }

    public static async deleteProductRelationship(alias: any, OtherModel: any, req: Request):Promise<string | any> {
        try {
            await new QueryBuilder()
            .match({
                related: [
                    {
                        model: Products,
                        where: {
                            id: req.params.productId,
                        },
                    },
                    {
                        ...Products.getRelationshipByAlias(alias),
                        identifier: 'r',
                    },
                    {
                        model: OtherModel,
                         where: {
                             id: req.params.otherId
                          }
                    },
                ],
            })
            .delete({
                identifiers: 'r'
            }).run(neogma.queryRunner);

            return 'success';
        } catch (err) {
            throw new InternalError('Something went wrong.');
        }
    }

}
