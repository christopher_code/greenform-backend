import { Request } from 'express';
import { Categories, CategoryPropertiesI } from '../models/category';
import { BadRequestError } from '../../middleware/ApiError';

export default class CatogeryService {
    public static async findCategoryByName(req: Request):Promise<CategoryPropertiesI> {
        try {
            const category:CategoryPropertiesI | any = await Categories.findOne({
                where: {
                    typeOfCategory: req.params.name,
                },
            });

            return category;
        } catch (err) {
            throw new BadRequestError('Error finding Category.');
        }
    }
}
