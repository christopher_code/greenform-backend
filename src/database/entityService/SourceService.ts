import { neogma } from "../../app";
import { QueryBuilder, QueryRunner } from 'neogma';
import { Request, NextFunction } from 'express';
import { Source, SourcePropertiesI, ApprovalStatus } from '../models/Source';
import { BadRequestError, InternalError } from '../../middleware/ApiError';
import { generateDate } from "../../middleware/generateDate";
import { generateUniqeuId } from "../../middleware/generateUuid";

export default class SourceService {

  public static async saveSource(req:Request, sourceType: string, relType: string, relId: string, productId:string): Promise<SourcePropertiesI> {
    let source: any;
    try {
        source = await Source.createOne({
            id: generateUniqeuId(),
            title: req.body.sourceTitle,
            link: req.body.sourceLink,
            description: req.body.sourceDescription,
            status: ApprovalStatus.NEW_SOURCE,
            createdAt: generateDate(),
            sourceType: sourceType,
            relType: relType,
            relId: relId,
            productId: productId,
            AddedBy: {
                where: {
                    params: {
                        id: req.params.userId
                    },
                },
            },
        });
        if (!source) throw new BadRequestError('Source does not exist!');
        return source;
    }
    catch (err) {
        throw new InternalError('Something went wrong.');
        } 
    }

    public static async findSourceByStatus(status: string):Promise<SourcePropertiesI> {
        try {
            const source:SourcePropertiesI[] | any = await Source.findMany({
                where: {
                    status: status,
                },
                limit: 5,
                order: [['createdAt', 'DESC']],
            });

            if (!source) throw new BadRequestError('Source does not exist!');
            return source;
        } catch (err) {
            throw new InternalError('Something went wrong.');
        }
    }

    public static async findSourceAnyRelations(id: string, model: any, alias: any, next: NextFunction):Promise<SourcePropertiesI | any> {
        try {
            const getSourceRelationship: any = await new QueryBuilder()
            .match({
                related: [
                    {
                        model: Source,
                        where: {
                            id: id,
                        },
                        identifier: 'source',
                    },
                    {
                        ...Source.getRelationshipByAlias(alias),
                        identifier: alias,
                    },
                    {
                        model: model,
                        identifier: 'model',
                    },
                ],
            })
            .return([
                'source',
                'model',
                alias
            ])
            .run(neogma.queryRunner);

            if (!getSourceRelationship) next();
    
            const data: any = QueryRunner.getResultProperties<any>(
                getSourceRelationship,
                'model',
            );
            
            return data;
        } catch (err) {
            throw new InternalError('Something went wrong.');
        }
    }

    public static async findSourceAnyRelationsWithProoperties(id: string, model: any, alias: any, next: NextFunction):Promise<SourcePropertiesI | any> {
        try {
            const res: any = await new QueryBuilder()
            .match({
                related: [
                    {
                        model: Source,
                        where: {
                            id: id,
                        },
                        identifier: 'source',
                    },
                    {
                        ...Source.getRelationshipByAlias(alias),
                        identifier: alias,
                    },
                    {
                        model: model,
                        identifier: 'model',
                    },
                ],
            })
            .return([
                'source',
                'model',
                alias
            ])
            .run(neogma.queryRunner);

            if (!res) next();

            let result = {
                user: res.records[0]?._fields[1].properties.name,
                approvalProperties: res.records[0]?.get(alias).properties,
            }
            return result;
        } catch (err) {
            throw new InternalError('Something went wrong.');
        }
    }

    public static async findRandomSources(req: Request):Promise<SourcePropertiesI> {
        try {
            let randomNum: number = Math.floor(Math.random() * (5))
            let sources:SourcePropertiesI[] | any;
            if (req.params.role === 'user') {
                if (req.params.sourceType === 'new') {
                    if (req.params.typeRel === 'product') {
                        sources = await Source.findMany({where: {status: 'newSource', relType: 'product', sourceType: 'new'}, skip: randomNum,});
                    }
                }
            } else if (req.params.role !== 'user') {
                let sourceType: string = '';
                if (randomNum === 0) {sourceType = 'newSource'}
                else if (randomNum === 1) {sourceType = 'approvedFirst'}
                else if (randomNum === 2) {sourceType = 'notApprovedOnce'}
                else if (randomNum === 3) {sourceType = 'approvedByNewUser'}
                else if (randomNum === 4) {sourceType = 'notApprovedNewUser'}
            if (req.params.sourceType === 'new') {
                if (req.params.typeRel === 'product') {
                    sources = await Source.findMany({where: {relType: 'product', sourceType: 'new', status: sourceType}, skip: randomNum});
                } else if (req.params.typeRel === 'seller') {
                    sources = await Source.findMany({where: {relType: 'seller', sourceType: 'new', status: sourceType}, skip: randomNum});
                } else if (req.params.typeRel === 'total') {
                    sources = await Source.findMany({where: {relType: 'total', sourceType: 'new', status: sourceType}, skip: randomNum});
                } else if (req.params.typeRel === 'single') {
                    sources = await Source.findMany({where: {relType: 'single', sourceType: 'new', status: sourceType}, skip: randomNum});
                } else if (req.params.typeRel === 'reason') {
                    sources = await Source.findMany({where: {relType: 'reason', sourceType: 'new', status: sourceType}, skip: randomNum});
                } else if (req.params.typeRel === 'alternative') {
                    sources = await Source.findMany({where: {relType: 'alternative', sourceType: 'new', status: sourceType}, skip: randomNum});
                }
            } else if (req.params.sourceType === 'update') {
                if (req.params.typeRel === 'product') {
                    sources = await Source.findMany({where: {relType: 'product', sourceType: 'update', status: sourceType}, skip: randomNum});
                } else if (req.params.typeRel === 'seller') {
                    sources = await Source.findMany({where: {relType: 'seller', sourceType: 'update', status: sourceType}, skip: randomNum});
                } else if (req.params.typeRel === 'single') {
                    sources = await Source.findMany({where: {relType: 'single', sourceType: 'update', status: sourceType}, skip: randomNum});
                } else if (req.params.typeRel === 'reason') {
                    sources = await Source.findMany({where: {relType: 'reason', sourceType: 'update', status: sourceType}, skip: randomNum});
                }
            } else if (req.params.sourceType === 'delete') {
                if (req.params.typeRel === 'product') {
                    sources = await Source.findMany({where: {relType: 'product', sourceType: 'delete', status: sourceType}, skip: randomNum});
                } else if (req.params.typeRel === 'seller') {
                    sources = await Source.findMany({where: {relType: 'seller', sourceType: 'delete', status: sourceType}, skip: randomNum});
                } else if (req.params.typeRel === 'total') {
                    sources = await Source.findMany({where: {relType: 'total', sourceType: 'delete', status: sourceType}, skip: randomNum});
                } else if (req.params.typeRel === 'single') {
                    sources = await Source.findMany({where: {relType: 'single', sourceType: 'delete', status: sourceType}, skip: randomNum});
                } else if (req.params.typeRel === 'reason') {
                    sources = await Source.findMany({where: {relType: 'reason', sourceType: 'delete', status: sourceType}, skip: randomNum});
                }
            }
        }

            if (!sources) throw new BadRequestError('Sources do not exist!');
            return sources;
        } catch (err) {
            throw new InternalError('Something went wrong.');
        }
    }

}