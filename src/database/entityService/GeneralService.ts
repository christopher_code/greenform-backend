import { neogma } from "../../app";
import { QueryBuilder, QueryRunner } from 'neogma';
import { BadRequestError, InternalError } from '../../middleware/ApiError';
import { NextFunction } from "express";

export default class GeneralService {

    public static async createNode(model: any, input: any): Promise<any> {
        try {
            let nodeCreated:any = await model.createOne(input);
            return nodeCreated;
        }
        catch (err) {
            throw new BadRequestError(err);
        }  
    };

    public static async relateNodes(model: any, alias: string, id: string, properties: any):Promise<boolean> {
        try {
            await model.relateTo(
                {
                    alias: alias,
                    where: {
                        id: id
                    },
                    properties: properties
                }
            );
            return true;
        } catch (err) {
            throw new InternalError('Something went wrong.');
        }
    }

    public static async relateNodesModel(Model: any, alias: string, sourceId: string, id: string, properties: any):Promise<boolean> {
        try {
            await Model.relateTo(
                {
                    alias: alias,
                    where: {
                        source: {
                            id: sourceId
                        },
                        target: {
                            id: id,
                        },
                    },
                    properties: properties
                }
            );
            return true;
        } catch (err) {
            throw new InternalError('Something went wrong.');
        }
    }

    public static async findNodeById(Model: any, id: string):Promise<any> {
        try {
            const node:any = await Model.findOne({
                where: {
                    id: id,
                },
            });
            if (!node) throw new BadRequestError('Data does not exist!');
            return node;
        } catch (err) {
            throw new InternalError('Something went wrong.');
        }
    }

    public static async findNodeByEmail(Model: any, email: string):Promise<any> {
        try {
            const node:any = await Model.findOne({
                where: {
                    email: email,
                },
            });

            if (!node) throw new BadRequestError('Data does not exist!');
            return node;
        } catch (err) {
            throw new InternalError('Something went wrong.');
        }
    }

    public static async findNodeByToken(Model: any, token: string):Promise<any> {
        try {
            const node:any = await Model.findOne({
                where: {
                    token: token,
                },
            });

            return node;
        } catch (err) {
            throw new InternalError('Something went wrong.');
        }
    }

    public static async findRandomNodes(total: number, model: any):Promise<any> {
        try {
            let randomNum: number = Math.floor(Math.random() * (total-3))
            if (randomNum < 0) {randomNum = 0};
            const nodes:any = await model.findMany({where: {}, 
                skip: randomNum,
                order: [['createdAt', 'DESC']],
            });

            if (!nodes) throw new BadRequestError('Product does not exists!');
            return nodes;
        } catch (err) {
            throw new InternalError('Something went wrong.');
        }
    }

    public static async findNodeAnyRelations(id: string, model: any, modelRel: any, alias: any, next: NextFunction):Promise<any> {
        try {
            const getNodeRelationship = await new QueryBuilder()
            .match({
                related: [
                    {
                        model: model,
                        where: {
                            id: id,
                        },
                        identifier: 'node',
                    },
                    {
                        ...model.getRelationshipByAlias(alias),
                        identifier: alias,
                    },
                    {
                        model: modelRel,
                        identifier: 'model',
                    },
                ],
            })
            .return([
                'node',
                'model',
            ])
            .run(neogma.queryRunner);

            if (!getNodeRelationship) next();
    
            const data = QueryRunner.getResultProperties<any>(
                getNodeRelationship,
                'model',
            );

            return data;
        } catch (err) {
            throw new InternalError('Something went wrong.');
        }
    }

    public static async findNodeAnyRelationsWithProperties(id: string, id2: any, model: any, modelRel: any, alias: any, next: NextFunction):Promise<any> {
        try {
            const res: any = await new QueryBuilder()
            .match({
                related: [
                    {
                        model: model,
                        where: {
                            id: id,
                        },
                        identifier: 'node',
                    },
                    {
                        ...model.getRelationshipByAlias(alias),
                        identifier: alias,
                    },
                    {
                        model: modelRel,
                        identifier: 'model',
                        where: {
                            id: id2,
                        }
                    },
                ],
            })
            .return([
                'node',
                'model',
                alias
            ])
            .run(neogma.queryRunner);

            if (!res) next();

            let result: object[] = []
            for (let i:number = 0; i < res.records.length; i++){
                result.push({
                    source: res.records[i]?._fields[1].properties,
                    properties: res.records[i]?.get(alias).properties,
                })
            }
            return result;
        } catch (err) {
            throw new InternalError('Something went wrong.');
        }
    }

    public static async getTotalNumberOfSamples(model: string):Promise<number> {
        try {
            const queryRunner = new QueryRunner({
                driver: neogma.driver,
                logger: console.log
            });

            const result = await queryRunner.run(
                `MATCH (n:${model})
                    RETURN count(n) as count`,
            );

            if (!result) throw new BadRequestError('Index of queried does not exist!');
            let zwi: any = result.records[0];
            return zwi._fields[0].low;
        } catch (err) {
            throw new InternalError('Something went wrong.');
        }
    }

    public static async deleteNode(Model: any, id: string, next: NextFunction):Promise<any> {
        try {
            await Model.delete({
                where: {
                    id: id
                },
                detach: true,
            });
            return next();
        } catch (err) {
            throw new InternalError('Something went wrong.');
        }
    }
}
