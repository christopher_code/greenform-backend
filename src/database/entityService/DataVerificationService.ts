import { Request } from 'express';
import { InternalError, BadRequestError } from '../../middleware/ApiError';
import { SourcePropertiesI } from "../../database/models/Source";

export default class DataVerificationService {

  public static async updateStatus(req: Request, source: SourcePropertiesI | any):Promise<any | null> {
     try {
        let relationMethod: any;
        let status: string;
        if (source.status == 'newSource' && req.params.userStatus == 'superuser' && req.body.approval == true) {
            relationMethod='ApprovedFirstBy';
            status='approvedFirst';
        } else if (source.status == 'newSource' && req.params.userStatus == 'admin' && req.body.approval == true) {
            relationMethod='ApprovedFirstBy';
            status='approvedFirst';
        } else if (source.status == 'newSource' && req.params.userStatus == 'user' && req.body.approval == true) {
            relationMethod='ApprovedByNewUser';
            status='approvedByNewUser';
        } else if (source.status == 'approvedFirst' && req.params.userStatus == 'superuser' && req.body.approval == true) {
            relationMethod='ApprovedFinalBy';
            status='approvedFinal';
        } else if (source.status == 'approvedFirst' && req.params.userStatus == 'admin' && req.body.approval == true) {
            relationMethod='ApprovedFinalBy';
            status='approvedFinal';
        } else if (source.status == 'approvedFirst' && req.params.userStatus == 'superuser' && req.body.approval == false) {
            relationMethod='NotApprovedOnce';
            status='notApprovedOnce';
        } else if (source.status == 'approvedFirst' && req.params.userStatus == 'admin' && req.body.approval == false) {
            relationMethod='NotApprovedOnce';
            status='notApprovedOnce';
        } else if (source.status == 'newSource' && req.params.userStatus == 'superuser' && req.body.approval == false) {
            relationMethod='NotApprovedOnce';
            status='notApprovedOnce';
        } else if (source.status == 'newSource' && req.params.userStatus == 'admin' && req.body.approval == false) {
            relationMethod='NotApprovedOnce';
            status='notApprovedOnce';
        } else if (source.status == 'newSource' && req.params.userStatus == 'user' && req.body.approval == false) {
            relationMethod='NotApprovedNewUser';
            status='notApprovedNewUser';
        } else if (source.status == 'notApprovedOnce' && req.params.userStatus == 'superuser' && req.body.approval == false) {
            relationMethod='NotApprovedTwice';
            status='notApprovedAgain';
        } else if (source.status == 'notApprovedOnce' && req.params.userStatus == 'admin' && req.body.approval == false) {
            relationMethod='NotApprovedTwice';
            status='notApprovedAgain';
        } else if (source.status == 'notApprovedOnce' && req.params.userStatus == 'superuser' && req.body.approval == true) {
            relationMethod='ApprovedFirstBy';
            status='approvedFirst';
        } else if (source.status == 'notApprovedOnce' && req.params.userStatus == 'admin' && req.body.approval == true) {
            relationMethod='ApprovedFirstBy';
            status='approvedFirst';
        } else if (source.status == 'approvedByNewUser' && req.params.userStatus == 'superuser' && req.body.approval == true) {
            relationMethod='ApprovedFirstBy';
            status='approvedFirst';
        } else if (source.status == 'approvedByNewUser' && req.params.userStatus == 'user' && req.body.approval == true) {
            relationMethod='ApprovedByNewUser';
            status='approvedByNewUser';
        } else if (source.status == 'approvedByNewUser' && req.params.userStatus == 'admin' && req.body.approval == true) {
            relationMethod='ApprovedFirstBy';
            status='approvedFirst';
        } else if (source.status == 'approvedByNewUser' && req.params.userStatus == 'superuser' && req.body.approval == false) {
            relationMethod='NotApprovedOnce';
            status='notApprovedOnce';
        } else if (source.status == 'approvedByNewUser' && req.params.userStatus == 'admin' && req.body.approval == false) {
            relationMethod='NotApprovedOnce';
            status='notApprovedOnce';
        } else if (source.status == 'notApprovedNewUser' && req.params.userStatus == 'admin' && req.body.approval == false) {
            relationMethod='NotApprovedTwice';
            status='notApprovedAgain';
        } else if (source.status == 'notApprovedNewUser' && req.params.userStatus == 'superuser' && req.body.approval == false) {
            relationMethod='NotApprovedTwice';
            status='notApprovedAgain';
        } else if (source.status == 'notApprovedNewUser' && req.params.userStatus == 'user' && req.body.approval == false) {
            relationMethod='NotApprovedNewUser';
            status='notApprovedNewUser';
        }
        else {
            throw new BadRequestError("An error occured while validating the source");
        }
        let response = [
            relationMethod,
            status
        ]

        return response;
    } catch (err) {
        throw new InternalError('Something went wrong' + err)
    }
  }
  
}
