import { SellerPropertiesI, Sellers } from '../models/Seller';
import { InternalError } from '../../middleware/ApiError';


export default class SellerService {
    public static async findSellerByName(name: string):Promise<SellerPropertiesI | any> {
        try {
            const seller:SellerPropertiesI | any = await Sellers.findOne({
                where: {
                    name: name,
                },
            });

            return seller;
        } catch (err) {
            throw new InternalError('Something went wrong.');
        }
    }
}