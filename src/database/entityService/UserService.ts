import { NextFunction } from 'express';
import { Users, UsersPropertiesI } from '../models/Users';
import { BadRequestError, InternalError } from '../../middleware/ApiError';

export default class UserService {

  public static async checkUniqueEmail(email: string, next:NextFunction):Promise<NextFunction | any> {
    try {
       const user:UsersPropertiesI | any = await Users.findOne({
           where: {
               email: email,
           },
       });
       
       if (!user) return next();
       if (user) throw new BadRequestError('User with this email alreadt exists!');
   } catch (err) {
        throw new InternalError('Something went wrong.');
   }
}

}
