import bcrypt from 'bcrypt';
import { generateUniqeuId } from "../../../../src/middleware/generateUuid";

export const USER_EMAIL = 'test@user.com';
export const USER_PASSWORD = 'password123';
export const USER_PASSWORD_HASH = bcrypt.hashSync(USER_PASSWORD, 12);

type UserInterface = {
    id: string;
    email: string;
    name?: string;
    company?: string;
    password: string;
    roleStatus: string;
    createdAt?: number;
};

export const bcryptCompareSpy = jest.spyOn(bcrypt, 'compare');

export const mockUserFindByEmail = jest.fn(
  async (email: string): Promise<UserInterface | null> => {
    if (email === USER_EMAIL)
      return {
        id: generateUniqeuId(),
        email: USER_EMAIL,
        password: USER_PASSWORD_HASH,
        name: 'abc',
        company: 'test GmbH',
        roleStatus: 'new',
        createdAt: 123456789,
      } as UserInterface;
    return null;
  },
);

jest.mock('../../../../src/database/entityService/UserService.ts', () => ({
  get findUserByEmail() {
    return mockUserFindByEmail;
  },
}));
