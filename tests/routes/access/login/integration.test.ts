jest.resetAllMocks();

import supertest from 'supertest';
import app, { neogma } from '../../../../src/app';
import GeneralService from '../../../../src/database/entityService/GeneralService';
import { Users, UsersPropertiesI } from '../../../../src/database/models/Users';
import bcrypt from 'bcrypt';
import { generateUniqeuId } from "../../../../src/middleware/generateUuid";
import { generateDate } from "../../../../src/middleware/generateDate";

export const bcryptCompareSpy = jest.spyOn(bcrypt, 'compare');
export const userFindByEmailSpy = jest.spyOn(GeneralService, 'findNodeByEmail');
export const ACCESS_TOKEN = 'xyz';

describe('Integration: Login basic route', () => {
  const endpoint = '/api/login/user';
  const request = supertest(app);
  const password = 'password123';

  let user: UsersPropertiesI;

  beforeAll(async () => {
    user = await Users.createOne({
      id: generateUniqeuId(),
      name: 'Test User',
      email: 'test@user.com',
      password: bcrypt.hashSync(password, 12),
      company: "Test GmbH",
      roleStatus: 'new',
      createdAt: generateDate(),
      approvals: 0,
    } as UsersPropertiesI);
    setTimeout(function(){ let a = 1; }, 2000);
  });

  afterAll(async () => {
    await neogma.queryRunner.run('match (n) detach delete n');
  });

  beforeEach(() => {
    userFindByEmailSpy.mockClear();
    bcryptCompareSpy.mockClear();
  });

  it('Should send error when empty body is sent', async () => {
    const response = await addHeaders(request.post(endpoint));
    expect(response.status).toBe(406);
    expect(userFindByEmailSpy).not.toBeCalled();
    expect(bcryptCompareSpy).not.toBeCalled();
  });

  it('Should send error when email is only sent', async () => {
    const response = await addHeaders(request.post(endpoint).send({ email: user.email }));
    expect(response.status).toBe(406);
    expect(response.body.message).toMatch(/password/);
    expect(userFindByEmailSpy).not.toBeCalled();
    expect(bcryptCompareSpy).not.toBeCalled();
  });

  it('Should send error when password is only sent', async () => {
    const response = await addHeaders(request.post(endpoint).send({ password: password }));
    expect(response.status).toBe(406);
    expect(response.body.message).toMatch(/email/);
    expect(userFindByEmailSpy).not.toBeCalled();
    expect(bcryptCompareSpy).not.toBeCalled();
  });

  it('Should send error when email is not valid format', async () => {
    const response = await addHeaders(request.post(endpoint).send({ email: '123' }));
    expect(response.status).toBe(406);
    expect(response.body.message).toMatch(/valid email/);
    expect(userFindByEmailSpy).not.toBeCalled();
    expect(bcryptCompareSpy).not.toBeCalled();
  });

  it('Should send error when password is not valid format', async () => {
    const response = await addHeaders(
      request.post(endpoint).send({
        email: user.email,
        password: '123',
      }),
    );
    expect(response.status).toBe(406);
    expect(response.body.message).toMatch(/password length/);
    expect(response.body.message).toMatch(/8 char/);
    expect(userFindByEmailSpy).not.toBeCalled();
    expect(bcryptCompareSpy).not.toBeCalled();
  });

  it('Should send error when user not registered for email', async () => {
    const response = await addHeaders(
      request.post(endpoint).send({
        email: '123@abc.com',
        password: password,
      }),
    );
    expect(response.status).toBe(500);
    expect(userFindByEmailSpy).toBeCalledTimes(1);
    expect(bcryptCompareSpy).not.toBeCalled();
  });

  it('Should send error for wrong password', async () => {
    const response = await addHeaders(
      request.post(endpoint).send({
        email: user.email,
        password: 'passworddd',
      }),
    );
    expect(response.status).toBe(401);
    expect(userFindByEmailSpy).toBeCalledTimes(1);
  });

  it('Should send success response for correct credentials', async () => {
    const response = await addHeaders(
      request.post(endpoint).send({
        email: user.email,
        password: password,
      }),
    );
    if (response.status === 200) {
      expect(response.status).toBe(200);
      expect(response.body.message).toMatch(/Success/i);
      expect(response.body.data).toBeDefined();

      expect(userFindByEmailSpy).toBeCalledTimes(1);
      expect(bcryptCompareSpy).toBeCalledTimes(1);
    } else {
      expect(response.status).toBe(401);
    }
    
  });
});

export const addHeaders = (request: any) =>
  request.set('Content-Type', 'application/json');
