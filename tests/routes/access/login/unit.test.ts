import supertest from 'supertest';
import app from "../../../../src/app";
import { addHeaders } from './integration.test';

import {
    mockUserFindByEmail,
    bcryptCompareSpy,
    USER_EMAIL,
    USER_PASSWORD,
  } from './mock';

describe('Mock Unit: Login basic route', () => {
  const endpoint = '/api/login/user';

  const request = supertest(app);
  
beforeEach(() => {
    mockUserFindByEmail.mockClear();
    bcryptCompareSpy.mockClear();
  });


  it('Body empty: should throw error', async () => {
    const response = await addHeaders(request.post(endpoint));
    expect(response.status).toBe(406);
    expect(mockUserFindByEmail).not.toBeCalled();
    expect(bcryptCompareSpy).not.toBeCalled();
  });

  it('Only email set: should throw error', async () => {
    const response = await addHeaders(request.post(endpoint).send({ email: USER_EMAIL }));
    expect(response.status).toBe(406);
    expect(response.body.message).toMatch('password is required');
    expect(mockUserFindByEmail).not.toBeCalled();
    expect(bcryptCompareSpy).not.toBeCalled();
  });

  it('Only password send: should throw error', async () => {
    const response = await addHeaders(request.post(endpoint).send({ password: USER_PASSWORD }));
    expect(response.status).toBe(406);
    expect(response.body.message).toMatch('email is required');
    expect(mockUserFindByEmail).not.toBeCalled();
    expect(bcryptCompareSpy).not.toBeCalled();
  });

  it('Email is not valid format: should throw error', async () => {
    const response = await addHeaders(request.post(endpoint).send({ email: 'test1234' }));
    expect(response.status).toBe(406);
    expect(response.body.message).toMatch(/valid email/);
    expect(mockUserFindByEmail).not.toBeCalled();
    expect(bcryptCompareSpy).not.toBeCalled();
  });

  it('Password too short: should trow error', async () => {
    const response = await addHeaders(
      request.post(endpoint).send({
        email: 'test@test.de',
        password: 'test',
      }),
    );
    expect(response.status).toBe(406);
    expect(response.body.message).toMatch(/password length/);
    expect(response.body.message).toMatch("password length must be at least 8 characters long");
    expect(mockUserFindByEmail).not.toBeCalled();
    expect(bcryptCompareSpy).not.toBeCalled();
  });

  it('Should send error when user not registered for email', async () => {
    const response = await addHeaders(
      request.post(endpoint).send({
        email: '123@abc.com',
        password: USER_PASSWORD,
      }),
    );
    expect(response.status).toBe(500);
  });

  it('Should send error for wrong password', async () => {
    const response = await addHeaders(
      request.post(endpoint).send({
        email: USER_EMAIL,
        password: '12345679',
      }),
    );
    expect(response.status).toBe(500);
  });
});


