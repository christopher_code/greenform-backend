import { addHeaders } from '../login/integration.test';
import { mockUserFindByEmail, USER_EMAIL, USER_PASSWORD } from '../login/mock';

import { mockUserCreate, bcryptHashSpy, USER_NAME, USER_COMPANY } from './mock';

import supertest from 'supertest';
import app from '../../../../src/app';

describe('Unit Mock: Signup basic route', () => {
  const endpoint = '/api/signup/user';
  const request = supertest(app);

  beforeEach(() => {
    mockUserFindByEmail.mockClear();
    mockUserCreate.mockClear();
    bcryptHashSpy.mockClear();
  });

  it('Should send error when empty body is sent', async () => {
    const response = await addHeaders(request.post(endpoint));
    
    expect(response.status).toBe(406);
    expect(mockUserFindByEmail).not.toBeCalled();
    expect(bcryptHashSpy).not.toBeCalled();
    expect(mockUserCreate).not.toBeCalled();
  });

  it('Should send error when email is not sent', async () => {
    const response = await addHeaders(
      request.post(endpoint).send({
        name: USER_NAME,
        password: USER_PASSWORD,
        company: USER_COMPANY,
      }),
    );
    expect(response.status).toBe(406);
    expect(response.body.message).toMatch(/email/);
    expect(response.body.message).toMatch(/required/);
    expect(mockUserFindByEmail).not.toBeCalled();
    expect(bcryptHashSpy).not.toBeCalled();
    expect(mockUserCreate).not.toBeCalled();
  });

  it('Should send error when password is not sent', async () => {
    const response = await addHeaders(
      request.post(endpoint).send({
        email: USER_EMAIL,
        name: USER_NAME,
        company: USER_COMPANY,
      }),
    );
    expect(response.status).toBe(406);
    expect(response.body.message).toMatch(/password/);
    expect(response.body.message).toMatch(/required/);
    expect(mockUserFindByEmail).not.toBeCalled();
    expect(bcryptHashSpy).not.toBeCalled();
    expect(mockUserCreate).not.toBeCalled();
  });

  it('Should send error when name is not sent', async () => {
    const response = await addHeaders(
      request.post(endpoint).send({
        email: USER_EMAIL,
        password: USER_PASSWORD,
        company: USER_COMPANY,
      }),
    );
    expect(response.status).toBe(406);
    expect(response.body.message).toMatch(/name/);
    expect(response.body.message).toMatch(/required/);
    expect(mockUserFindByEmail).not.toBeCalled();
    expect(bcryptHashSpy).not.toBeCalled();
    expect(mockUserCreate).not.toBeCalled();
  });

  it('Should send error when company is not sent', async () => {
    const response = await addHeaders(
      request.post(endpoint).send({
        email: USER_EMAIL,
        password: USER_PASSWORD,
        name: USER_NAME,
      }),
    );
    expect(response.status).toBe(406);
    expect(response.body.message).toMatch(/company/);
    expect(response.body.message).toMatch(/required/);
    expect(mockUserFindByEmail).not.toBeCalled();
    expect(bcryptHashSpy).not.toBeCalled();
    expect(mockUserCreate).not.toBeCalled();
  });

  it('Should send error when email is not valid format', async () => {
    const response = await addHeaders(
      request.post(endpoint).send({
        email: '123',
        name: USER_NAME,
        password: USER_PASSWORD,
        company: USER_COMPANY,
      }),
    );
    expect(response.status).toBe(406);
    expect(response.body.message).toMatch(/valid email/);
    expect(mockUserFindByEmail).not.toBeCalled();
    expect(bcryptHashSpy).not.toBeCalled();
    expect(mockUserCreate).not.toBeCalled();
  });

  it('Should send error when password is not valid format', async () => {
    const response = await addHeaders(
      request.post(endpoint).send({
        email: USER_EMAIL,
        name: USER_NAME,
        password: 'short',
        company: USER_COMPANY,
      }),
    );
    expect(response.status).toBe(406);
    expect(response.body.message).toMatch(/password length/);
    expect(response.body.message).toMatch(/8 char/);
    expect(mockUserFindByEmail).not.toBeCalled();
    expect(bcryptHashSpy).not.toBeCalled();
    expect(mockUserCreate).not.toBeCalled();
  });

  it('Should send success response for correct data', async () => {
    const response = await addHeaders(
      request.post(endpoint).send({
        email: USER_EMAIL,
        name: USER_NAME,
        password: USER_PASSWORD,
        company: USER_COMPANY,
      }),
    );
    expect(response.status).toBe(200);
    expect(response.body.message).toMatch(/Success/);
    expect(response.body.data).toBeDefined();

    expect(response).toHaveProperty('statusCode');
    expect(response.body).toHaveProperty('message');
    expect(response.body).toHaveProperty('statusCode');
    expect(response.body).toHaveProperty('data');
    expect(response.body.message).toEqual("Signup Success");
  });
});
