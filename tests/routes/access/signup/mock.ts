import { mockUserFindByEmail } from '../login/mock';
import bcrypt from 'bcrypt';

export const USER_NAME = 'Test User';
export const USER_COMPANY = 'Test GmbH';

export const bcryptHashSpy = jest.spyOn(bcrypt, 'hash');

export const mockUserCreate = jest.fn(
  async (): Promise<Object> => {
    return {
      statusCode: "10000",
      message: "Signup Success",
      data: {}
    };
  },
);

jest.mock('../../../../src/database/entityService/UserService.ts', () => ({
  get findUserByEmail() {
    return mockUserFindByEmail;
  }, 
  get create() {
    return mockUserCreate;
  },
}));
