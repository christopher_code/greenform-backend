jest.resetAllMocks();

import supertest from 'supertest';
import app, { neogma } from '../../../../src/app';
import GeneralService from '../../../../src/database/entityService/GeneralService';
import bcrypt from 'bcrypt';
import { addHeaders } from "../login/integration.test"; 

export const bcryptCompareSpy = jest.spyOn(bcrypt, 'compare');
export const userFindByEmailSpy = jest.spyOn(GeneralService, 'findNodeByEmail');
export const ACCESS_TOKEN = 'xyz';
const EMAIL = 'test@mail.de';
const NAME = 'Test User';
const PASSWORD = 'password123';
const COMPANY = 'Test Comp';

describe('Integration: Signup basic route', () => {
  const endpoint = '/api/signup/user';
  const request = supertest(app);

  afterAll(async () => {
    await neogma.queryRunner.run('match (n) detach delete n');
  });

  beforeEach(() => {
    userFindByEmailSpy.mockClear();
    bcryptCompareSpy.mockClear();
  });

  it('Should send error when empty body is sent', async () => {
    const response = await addHeaders(request.post(endpoint));
    expect(response.status).toBe(406);
    expect(userFindByEmailSpy).not.toBeCalled();
    expect(bcryptCompareSpy).not.toBeCalled();
  });

  it('Should send error when email is only sent', async () => {
    const response = await addHeaders(request.post(endpoint).send({ 
        email: EMAIL,
        name: NAME,
        company: COMPANY
    }));
    expect(response.status).toBe(406);
    expect(response.body.message).toMatch(/password/);
    expect(userFindByEmailSpy).not.toBeCalled();
    expect(bcryptCompareSpy).not.toBeCalled();
  });

  it('Should send error when password is only sent', async () => {
    const response = await addHeaders(request.post(endpoint).send({ password: PASSWORD }));
    expect(response.status).toBe(406);
    expect(response.body.message).toMatch(/email/);
  });

  it('Should send error when email is not valid format', async () => {
    const response = await addHeaders(request.post(endpoint).send({ email: '123' }));
    expect(response.status).toBe(406);
    expect(response.body.message).toMatch(/valid email/);
    expect(userFindByEmailSpy).not.toBeCalled();
  });

  it('Should send error when password is not valid format', async () => {
    const response = await addHeaders(
      request.post(endpoint).send({
        email: EMAIL,
        password: '123',
        name: NAME,
        company: COMPANY,
      }),
    );
    expect(response.status).toBe(406);
    expect(response.body.message).toMatch(/password length/);
    expect(response.body.message).toMatch(/8 char/);
  });

  it('Should send success response when user is sign up successfully', async () => {
    const response = await addHeaders(
      request.post(endpoint).send({
        email: EMAIL,
        password: PASSWORD,
        company: COMPANY,
        name: NAME,
      }),
    );
      expect(response.status).toBe(200);
      expect(response.body.message).toMatch(/Success/i);
      expect(response.body.data).toBeDefined();
  });
});
