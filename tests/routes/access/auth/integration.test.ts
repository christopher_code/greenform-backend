jest.resetAllMocks();

import supertest from 'supertest';
import app, { neogma } from '../../../../src/app';
import GeneralService from '../../../../src/database/entityService/GeneralService';
import bcrypt from 'bcrypt';
import { addHeaders } from "../login/integration.test"; 

export const bcryptCompareSpy = jest.spyOn(bcrypt, 'compare');
export const userFindByEmailSpy = jest.spyOn(GeneralService, 'findNodeByEmail');
export const ACCESS_TOKEN = 'xyz';
const EMAIL = 'test@mail.de';
const NAME = 'Test User';
const PASSWORD = 'password123';
const COMPANY = 'Test Comp';

describe('Integration: Test signup and login combined', () => {
  const endpointSignUp = '/api/signup/user';
  const endpointLogin = '/api/login/user';
  const request = supertest(app);

  afterAll(async () => {
    await neogma.queryRunner.run('match (n) detach delete n');
  });

  beforeEach(() => {
    userFindByEmailSpy.mockClear();
    bcryptCompareSpy.mockClear();
  });

  it('Should send error response when user is login before signup', async () => {
    const response = await addHeaders(
      request.post(endpointLogin).send({
        email: EMAIL,
        password: PASSWORD,
      }),
    );
      expect(response.status).toBe(500);
      expect(response.body.message).toEqual('Something went wrong.');
      expect(response.body.data).not.toBeDefined();
  });

  it('Should send success response when user is sign up successfully', async () => {
    const response = await addHeaders(
      request.post(endpointSignUp).send({
        email: EMAIL,
        password: PASSWORD,
        company: COMPANY,
        name: NAME,
      }),
    );
      expect(response.status).toBe(200);
      expect(response.body.message).toMatch(/Success/i);
      expect(response.body.data).toBeDefined();
  });

  it('Should send success response when user is login successfully', async () => {
    const response = await addHeaders(
      request.post(endpointLogin).send({
        email: EMAIL,
        password: PASSWORD,
      }),
    );
      expect(response.status).toBe(200);
      expect(response.body.message).toMatch(/Success/i);
      expect(response.body.data).toBeDefined();
      expect(response.body.data.id).toBeDefined();
      expect(response.body.data.token).toBeDefined();
      expect(response.body.data.role).toBeDefined();
      expect(response.body.data.role).toEqual('user');
  });

});