jest.resetAllMocks();

import supertest from 'supertest';
import app, { neogma } from '../../../../src/app';
import GeneralService from '../../../../src/database/entityService/GeneralService';
import { UsersPropertiesI } from "../../../../src/database/models/Users";
import bcrypt from 'bcrypt';
import { addHeaders } from "../../access/login/integration.test"; 
import { addAuthHeaders } from "../../access/auth/mock";

export const bcryptCompareSpy = jest.spyOn(bcrypt, 'compare');
export const userFindByEmailSpy = jest.spyOn(GeneralService, 'findNodeByEmail');
const EMAIL = 'test232323@mail.de';
const NAMEUSER = 'Tes2323t User';
const PASSWORD = 'pas2323sword123';
const COMPANY = 'Tes2323t Comp';
const SOURCETITLE = "So232323urce Title";
const SOURCELINK = "https://validLink.de";
const SOURCEDESCRIPTION = 'Sourc2323e Description 12121212121212121212';
const PRODUCTNAME = 'Produc2323t Name';
const DESCRIPTIONPRODUCT = "Produ2323ct Description 121212121212121212";
const LINKTOBUY = "https://validLink.de";
const COSTPERPRODUCT = 122;
const SOURCETOTAL = 'Source232 Totoal';
const SOURCELINKTOTAL = "https://validLink.de";
const DESCRIPTIOTOTAL = "Produ2323ct Descripdstion 121212121212121212";


describe('Integration: Total GHG of product', () => {
  const endpointSignUp = '/api/signup/user';
  const endpointLogin = '/api/login/user';
  const endpointCreateProduct = '/api/product/new/';
  const endpointTotalGHG = '/api/ghg/total/new/';
  const request = supertest(app);
  let user: UsersPropertiesI;
  let userId: string;
  let token: string;

  beforeAll(async () => {
    user = await addHeaders(
        request.post(endpointSignUp).send({
          email: EMAIL,
          password: PASSWORD,
          company: COMPANY,
          name: NAMEUSER,
        }),
      );
    const responseLogin = await addHeaders(
        request.post(endpointLogin).send({
        email: EMAIL,
        password: PASSWORD,
        }),
    );
    userId = responseLogin.body.data.id;
    token = responseLogin.body.data.token;
    setTimeout(function(){ let a = 1; }, 2000);
  });

  afterAll(async () => {
    await neogma.queryRunner.run('match (n) detach delete n');
  });

  beforeEach(() => {
    userFindByEmailSpy.mockClear();
    bcryptCompareSpy.mockClear();
  });


  it('Error: Add Total GHG Data to created Product after login with wrong productId', async () => {
    const responseProductCreation = await addAuthHeaders(
        request.post(endpointCreateProduct + userId).send({
            sourceTitle: SOURCETITLE+'1',
            sourceLink: SOURCELINK+'1',
            sourceDescription: SOURCEDESCRIPTION+'1',
            name: PRODUCTNAME+'1',
            description: DESCRIPTIONPRODUCT+'1',
            linkToBuy: LINKTOBUY+'1',
            costPerProduct: COSTPERPRODUCT,
        }),
        token
      );
    
    expect(responseProductCreation.status).toBe(200);
    expect(responseProductCreation.body.data.id).toBeDefined();
    
    const responseCategoryCreation = await addAuthHeaders(
        request.post(endpointTotalGHG + userId + '/' + responseProductCreation.body.data.name).send({
            sourceTitle: 2,
            sourceLink: SOURCELINKTOTAL,
            sourceDescription: DESCRIPTIOTOTAL,
            ghgPerKG: SOURCETOTAL
        }),
        token
      );
    
    expect(responseCategoryCreation.status).toBe(406);
    expect(responseCategoryCreation.body.data).not.toBeDefined();
  });

});