jest.resetAllMocks();

import supertest from 'supertest';
import app, { neogma } from '../../../../../src/app';
import GeneralService from '../../../../../src/database/entityService/GeneralService';
import { UsersPropertiesI } from "../../../../../src/database/models/Users";
import bcrypt from 'bcrypt';
import { addHeaders } from "../../../access/login/integration.test"; 
import { addAuthHeaders } from "../../../access/auth/mock";

export const bcryptCompareSpy = jest.spyOn(bcrypt, 'compare');
export const userFindByEmailSpy = jest.spyOn(GeneralService, 'findNodeByEmail');
const EMAIL = 'test@mail.de';
const NAMEUSER = 'Test User';
const PASSWORD = 'password123';
const COMPANY = 'Test Comp';
const SOURCETITLE = "Source Title";
const SOURCELINK = "https://validLink.de";
const SOURCEDESCRIPTION = 'Source Description 12121212121212121212';
const PRODUCTNAME = 'Product Name';
const DESCRIPTIONPRODUCT = "Product Description 121212121212121212";
const LINKTOBUY = "https://validLink.de";
const COSTPERPRODUCT = 12;

describe('Integration: Test signup and login combined with total ghg update', () => {
  const endpointSignUp = '/api/signup/user';
  const endpointLogin = '/api/login/user';
  const endpointCreateProduct = '/api/product/new/'
  const endpointCreateTotoal  = '/api/ghg/total/new/'

// check weather url is correct

  const endpointUpdateTotal = '/api/ghg/total/update/fields/'
  const request = supertest(app);
  let user: UsersPropertiesI;
  let userId: string;
  let token: string;
  let userRole: string;

  beforeAll(async () => {
    user = await addHeaders(
        request.post(endpointSignUp).send({
          email: EMAIL,
          password: PASSWORD,
          company: COMPANY,
          name: NAMEUSER,
        }),
      );
    const responseLogin = await addHeaders(
        request.post(endpointLogin).send({
        email: EMAIL,
        password: PASSWORD,
        }),
    );
    userId = responseLogin.body.data.id;
    token = responseLogin.body.data.token;
    userRole = responseLogin.body.data.role;
    setTimeout(function(){ let a = 1; }, 2000);
  });

  afterAll(async () => {
    await neogma.queryRunner.run('match (n) detach delete n');
  });

  beforeEach(() => {
    userFindByEmailSpy.mockClear();
    bcryptCompareSpy.mockClear();
  });

  it('Create total ghg after product creation and update fields', async () => {
    const responseProductCreation = await addAuthHeaders(
        request.post(endpointCreateProduct + userId).send({
            sourceTitle: SOURCETITLE,
            sourceLink: SOURCELINK,
            sourceDescription: SOURCEDESCRIPTION,
            name: PRODUCTNAME,
            description: DESCRIPTIONPRODUCT,
            linkToBuy: LINKTOBUY,
            costPerProduct: COSTPERPRODUCT,
        }),
        token
      );

    expect(responseProductCreation.status).toBe(200);

    const responseTotalCreation = await addAuthHeaders(
        request.post(endpointCreateTotoal + userId + '/' + responseProductCreation.body.data.id).send({
            sourceTitle: SOURCETITLE + '1',
            sourceLink: SOURCELINK,
            sourceDescription: SOURCEDESCRIPTION + '1',
            ghgPerKG: 11,
        }),
        token
      );
    
    expect(responseTotalCreation.status).toBe(200);

    const responseTotalUpdate = await addAuthHeaders(
        request.put(endpointUpdateTotal + responseTotalCreation.body.data.id + '/'+ userId +'/' + responseProductCreation).send({
            sourceTitle: 'sdfsdfsdf sdfsdfsdfsdf sdfsdfsdfsdfsdfsdf',
            sourceLink: 'https://validLink.de',
            sourceDescription: 'Lorem Ipsum dolor sit amet sdsdfh sdsdfsdf sdf  sdf  sdfsdf',
            ghgPerKG: 30,
        }),
        token
      );

      expect(responseTotalUpdate.status).toBe(200);
      expect(responseTotalUpdate.body.message).toMatch(/Success/i);
      expect(responseTotalUpdate.body.data).toBeDefined();
  });

  it('Expect Error: Create Product and total ghg after login and use wrong field while updating', async () => {
    const responseProductCreation = await addAuthHeaders(
        request.post(endpointCreateProduct + userId).send({
            sourceTitle: SOURCETITLE,
            sourceLink: SOURCELINK,
            sourceDescription: SOURCEDESCRIPTION,
            name: 'other name',
            description: DESCRIPTIONPRODUCT,
            linkToBuy: LINKTOBUY,
            costPerProduct: COSTPERPRODUCT,
        }),
        token
      );

    expect(responseProductCreation.status).toBe(200);

    const responseTotalCreation = await addAuthHeaders(
        request.post(endpointCreateTotoal + userId + '/' + responseProductCreation.body.data.id).send({
            sourceTitle: SOURCETITLE + '3',
            sourceLink: SOURCELINK + '3',
            sourceDescription: SOURCEDESCRIPTION + '3',
            ghgPerKG: 10,
        }),
        token
      );
    
    expect(responseTotalCreation.status).toBe(200);

    const responseTotalUpdate = await addAuthHeaders(
        request.put(endpointUpdateTotal + responseProductCreation.body.data.id + '/'+ userId + '/' + + responseProductCreation.body.data.id).send({
            sourceTitle: SOURCETITLE + '2',
            sourceLink: SOURCELINK + '2',
            sourceDescription: SOURCEDESCRIPTION + '2',
            name: 'Is nicht',
        }),
        token
      );

      expect(responseTotalUpdate.status).toBe(406);
  });
});