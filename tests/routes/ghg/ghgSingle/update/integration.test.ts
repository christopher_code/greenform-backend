jest.resetAllMocks();

import supertest from 'supertest';
import app, { neogma } from '../../../../../src/app';
import GeneralService from '../../../../../src/database/entityService/GeneralService';
import bcrypt from 'bcrypt';
import { addHeaders } from "../../../access/login/integration.test"; 
import { addAuthHeaders } from "../../../access/auth/mock";

export const bcryptCompareSpy = jest.spyOn(bcrypt, 'compare');
export const userFindByEmailSpy = jest.spyOn(GeneralService, 'findNodeByEmail');
const EMAIL = 'test@mail.de';
const NAMEUSER = 'Test User';
const PASSWORD = 'password123';
const COMPANY = 'Test Comp';
const SOURCETITLE = "Source Title";
const SOURCELINK = "https://validLink.de";
const SOURCEDESCRIPTION = 'Source Description 12121212121212121212';
const PRODUCTNAME = 'Product Name';
const DESCRIPTIONPRODUCT = "Product Description 121212121212121212";
const LINKTOBUY = "https://validLink.de";
const COSTPERPRODUCT = 12;

describe('Integration: Test signup and login combined with single ghg update', () => {
  const endpointSignUp = '/api/signup/user';
  const endpointLogin = '/api/login/user';
  const endpointCreateProduct = '/api/product/new/'
  const endpointCreateSingle  = '/api/ghg/single/new/'

// check weather url is correct

  const endpointUpdateSingle = '/api/ghg/single/update/fields/'
  const request = supertest(app);
  let userId: string;
  let token: string;

  beforeAll(async () => {
    await addHeaders(
        request.post(endpointSignUp).send({
          email: EMAIL,
          password: PASSWORD,
          company: COMPANY,
          name: NAMEUSER,
        }),
      );
    const responseLogin = await addHeaders(
        request.post(endpointLogin).send({
        email: EMAIL,
        password: PASSWORD,
        }),
    );
    userId = responseLogin.body.data.id;
    token = responseLogin.body.data.token;
    setTimeout(function(){ let a = 1; }, 2000);
  });

  afterAll(async () => {
    await neogma.queryRunner.run('match (n) detach delete n');
  });

  beforeEach(() => {
    userFindByEmailSpy.mockClear();
    bcryptCompareSpy.mockClear();
  });

  it('Create Single ghg after product creation and update fields', async () => {
    const responseProductCreation = await addAuthHeaders(
        request.post(endpointCreateProduct + userId).send({
            sourceTitle: SOURCETITLE,
            sourceLink: SOURCELINK,
            sourceDescription: SOURCEDESCRIPTION,
            name: PRODUCTNAME,
            description: DESCRIPTIONPRODUCT,
            linkToBuy: LINKTOBUY,
            costPerProduct: COSTPERPRODUCT,
        }),
        token
      );

    expect(responseProductCreation.status).toBe(200);

    const responseSingleCreation = await addAuthHeaders(
        request.post(endpointCreateSingle + userId + '/' + responseProductCreation.body.data.id).send({
            sourceTitle: SOURCETITLE + '1',
            sourceLink: SOURCELINK + '1',
            sourceDescription: SOURCEDESCRIPTION + '1',
            ghgName: "GHG Name 1",
            emissionPerKG: 12,
        }),
        token
      );
    
    expect(responseSingleCreation.status).toBe(200);

    const responseSingleUpdate = await addAuthHeaders(
        request.put(endpointUpdateSingle + responseSingleCreation.body.data.id + '/'+ userId + '/' + + responseProductCreation.body.data.id).send({
            sourceTitle: SOURCETITLE + '2',
            sourceLink: SOURCELINK + '2',
            sourceDescription: SOURCEDESCRIPTION + '2',
            emissionPerKG: 18,
        }),
        token
      );

      expect(responseSingleUpdate.status).toBe(200);
      expect(responseSingleUpdate.body.message).toMatch(/Success/i);
      expect(responseSingleUpdate.body.data).toBeDefined();
  });

  it('Expect Error: Create Product and single ghg after login and use wrong field while updating', async () => {
    const responseProductCreation = await addAuthHeaders(
        request.post(endpointCreateProduct + userId).send({
            sourceTitle: SOURCETITLE,
            sourceLink: SOURCELINK,
            sourceDescription: SOURCEDESCRIPTION,
            name: 'other name',
            description: DESCRIPTIONPRODUCT,
            linkToBuy: LINKTOBUY,
            costPerProduct: COSTPERPRODUCT,
        }),
        token
      );

    expect(responseProductCreation.status).toBe(200);

    const responseSingleCreation = await addAuthHeaders(
        request.post(endpointCreateSingle + userId + '/' + responseProductCreation.body.data.id).send({
            sourceTitle: SOURCETITLE + '3',
            sourceLink: SOURCELINK + '3',
            sourceDescription: SOURCEDESCRIPTION + '3',
            ghgName: "GHG Name 1",
            emissionPerKG: 12,
        }),
        token
      );
    
    expect(responseSingleCreation.status).toBe(200);

    const responseSingleUpdate = await addAuthHeaders(
        request.put(endpointUpdateSingle + responseSingleCreation.body.data.id + '/'+ userId + '/' + responseProductCreation.body.data.id).send({
            sourceTitle: SOURCETITLE + '2',
            sourceLink: SOURCELINK + '2',
            sourceDescription: SOURCEDESCRIPTION + '2',
            name: 'Is nicht',
        }),
        token
      );

      expect(responseSingleUpdate.status).toBe(406);
  });
});