import supertest from 'supertest';
import app from "../../../../src/app";
import { addHeaders } from '../../access/login/integration.test'
import { addAuthHeaders } from "../../access/auth/mock";
import { testJWT } from "../../../../config"

const GHG_name = "Methan";
const GHG_KG = 0.1;
const userId = '123';
const productId = '456';
const SOURCE_TITLE = 'Seller Title Test Source';
const SOURCE_LINK = 'https://validLink.de';
const SOURCE_DESCRIPTION = 'SOURCE Seller description lorem ipsum dolr sit amet';

describe('Create GHG Single route', () => {
  const endpoint = `/api/ghg/single/new/${userId}/${productId}`;
  const request = supertest(app);

  it('No single ghg data provided', async () => {
    const response = await addAuthHeaders(
      request.post(endpoint).send({
        sourceTitle: SOURCE_TITLE,
        sourceLink: SOURCE_LINK,
        sourceDescription: SOURCE_DESCRIPTION,
        ghgName: GHG_name,
      }), 
      testJWT
    );

    expect(response.status).toBe(406);

    expect(response.body).toHaveProperty('message');
    expect(response.body.message).toEqual('emissionPerKG is required');

    expect(response.body).not.toHaveProperty('data');
  });

  it('String instead of number provided for single ghg data creation', async () => {
    const response = await addAuthHeaders(
      request.post(endpoint).send({
        sourceTitle: SOURCE_TITLE,
        sourceLink: SOURCE_LINK,
        sourceDescription: SOURCE_DESCRIPTION,
        ghgName: GHG_name,
        emissionPerKG: "1"
      }), 
      testJWT
    );

    expect(response.status).toBe(406);
  });
});
