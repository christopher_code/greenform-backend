import supertest from 'supertest';
import app from "../../../../src/app";
import { addHeaders } from '../../access/login/integration.test'
import { addAuthHeaders } from "../../access/auth/mock";
import { testJWT } from "../../../../config"

const GHG_name = "Methan Emissions";
const reasonTitle = "Lorem Ipsum";
const description = "Lorem Ipsum Dolor Sit Amet";
const userId = '1234567890';
const productId = '1234567890';
const SOURCE_TITLE = 'Seller Title Test Source';
const SOURCE_LINK = 'https://validLink.de';
const SOURCE_DESCRIPTION = 'SOURCE Seller description lorem ipsum dolr sit amet';

describe('Create GHG reason route', () => {
  const endpoint = `/api/ghg/reason/new/${userId}/${productId}`;
  const request = supertest(app);

  it('No name for reason ghg data provided', async () => {
    const response = await addAuthHeaders(
      request.post(endpoint).send({
        sourceTitle: SOURCE_TITLE,
        sourceLink: SOURCE_LINK,
        sourceDescription: SOURCE_DESCRIPTION,
        reasonTitle: reasonTitle,
        description: description
      }), 
      testJWT
    );

    expect(response.status).toBe(500);

    expect(response.body).toHaveProperty('message');
    expect(response.body.message).toEqual('Something went wrong!');

    expect(response.body).not.toHaveProperty('data');
  });
});
