jest.resetAllMocks();

import supertest from 'supertest';
import app, { neogma } from '../../../../../src/app';
import GeneralService from '../../../../../src/database/entityService/GeneralService';
import { UsersPropertiesI } from "../../../../../src/database/models/Users";
import bcrypt from 'bcrypt';
import { addHeaders } from "../../../access/login/integration.test"; 
import { addAuthHeaders } from "../../../access/auth/mock";

export const bcryptCompareSpy = jest.spyOn(bcrypt, 'compare');
export const userFindByEmailSpy = jest.spyOn(GeneralService, 'findNodeByEmail');
const EMAIL = 'test@mail.de';
const NAMEUSER = 'Test User';
const PASSWORD = 'password123';
const COMPANY = 'Test Comp';
const SOURCETITLE = "Source Title";
const SOURCELINK = "https://validLink.de";
const SOURCEDESCRIPTION = 'Source Description 12121212121212121212';
const PRODUCTNAME = 'Product Name';
const DESCRIPTIONPRODUCT = "Product Description 121212121212121212";
const LINKTOBUY = "https://validLink.de";
const COSTPERPRODUCT = 12;

describe('Integration: Test signup and login combined with reason ghg update', () => {
  const endpointSignUp = '/api/signup/user';
  const endpointLogin = '/api/login/user';
  const endpointCreateProduct = '/api/product/new/'
  const endpointCreateReason  = '/api/ghg/reason/new/'

// check weather url is correct

  const endpointUpdateReason = '/api/ghg/reason/update/fields/'
  const request = supertest(app);
  let user: UsersPropertiesI;
  let userId: string;
  let token: string;
  let userRole: string;

  beforeAll(async () => {
    user = await addHeaders(
        request.post(endpointSignUp).send({
          email: EMAIL,
          password: PASSWORD,
          company: COMPANY,
          name: NAMEUSER,
        }),
      );
    const responseLogin = await addHeaders(
        request.post(endpointLogin).send({
        email: EMAIL,
        password: PASSWORD,
        }),
    );
    userId = responseLogin.body.data.id;
    token = responseLogin.body.data.token;
    userRole = responseLogin.body.data.role;
    setTimeout(function(){ let a = 1; }, 2000);
  });

  afterAll(async () => {
    await neogma.queryRunner.run('match (n) detach delete n');
  });

  beforeEach(() => {
    userFindByEmailSpy.mockClear();
    bcryptCompareSpy.mockClear();
  });

  it('Create reason of ghg after product creation and update fields', async () => {
    const responseProductCreation = await addAuthHeaders(
        request.post(endpointCreateProduct + userId).send({
            sourceTitle: SOURCETITLE,
            sourceLink: SOURCELINK,
            sourceDescription: SOURCEDESCRIPTION,
            name: PRODUCTNAME,
            description: DESCRIPTIONPRODUCT,
            linkToBuy: LINKTOBUY,
            costPerProduct: COSTPERPRODUCT,
        }),
        token
      );

    expect(responseProductCreation.status).toBe(200);

    const responseReasonCreation = await addAuthHeaders(
        request.post(endpointCreateReason + userId + '/' + responseProductCreation.body.data.id).send({
            sourceTitle: SOURCETITLE + '1',
            sourceLink: SOURCELINK + '1',
            sourceDescription: SOURCEDESCRIPTION + '1',
            ghgName: 'test name',
            reasonTitle: "title of the reason",
            description: "Lorem ipsum dolor sit amet - Lorem ipsum dolor sit amet - Lorem ipsum dolor sit amet"
        }),
        token
      );
    
    expect(responseReasonCreation.status).toBe(200);

    const responseReasonUpdate = await addAuthHeaders(
        request.put(endpointUpdateReason + responseReasonCreation.body.data.id + '/'+ userId + '/' + responseProductCreation.body.data.id).send({
            sourceTitle: SOURCETITLE + '2',
            sourceLink: SOURCELINK + '2',
            sourceDescription: SOURCEDESCRIPTION + '2',
            description: "Test test test Test test test Test test test st test Test test testst test Test test test"
        
        }),
        token
      );

      expect(responseReasonUpdate.status).toBe(200);
      expect(responseReasonUpdate.body.message).toMatch(/Success/i);
      expect(responseReasonUpdate.body.data).toBeDefined();
  });
});