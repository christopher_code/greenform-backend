import schemaTotalGhg from "../../../src/routes/structure/ghgData/totalGhg/basicCRUD/schema";

const totalSuccess = {
    ghgPerKG: 23
}

const totalFail = {}

describe('Schema test: test if totalGhg schemas work as expected', () => {
    //success
    test('test totalGhg validator for login: success', () => {
        expect(schemaTotalGhg.createNewAndUpdate.validate(totalSuccess)).toBeTruthy();
    });

    //failure
    test('expect fail: required data missing', () => {
        const result = schemaTotalGhg.createNewAndUpdate.validate(totalFail);
        expect(result).rejects;
        expect(result).toMatchObject;
    });
});
