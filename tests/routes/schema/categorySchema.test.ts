import schemaCategory from "../../../src/routes/structure/category/schema";

const categorySuccess = {
    name: "test name",
    typeOfCategory: "Test Cat.",
}

const categoryFail = {
    name: "test name",
    typeOfCategory: "Test Cat.",
}


describe('Schema test: test if category schemas work as expected', () => {
    //success
    test('test category validator for login: success', () => {
        expect(schemaCategory.createNew.validate(categorySuccess)).toBeTruthy();
    });

    //failure
    test('expect fail: add extra field to category schema', () => {
        const result = schemaCategory.createNew.validate(categoryFail);
        expect(result).rejects;
        expect(result).toMatchObject;
    });
});
