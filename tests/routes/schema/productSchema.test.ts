import schemaProduct from "../../../src/routes/structure/product/basicCRUD/schema";

const productSuccess = {
    name: "test name",
    description: "Lorem Ipsum dolr sit amet.",
    linkToBuy: "https://validLink.de",
}

const productFail = {
    name: "test name",
    description: "Lorem Ipsum dolr sit amet.",
    linkToBuy: "https://validLink.de",
}


describe('Schema test: test if product schemas work as expected', () => {
    //success
    test('test product validator for login: success', () => {
        expect(schemaProduct.createNew.validate(productSuccess)).toBeTruthy();
    });

    //failure
    test('expect fail: use string instead of boolean', () => {
        const result = schemaProduct.createNew.validate(productFail);
        expect(result).rejects;
        expect(result).toMatchObject;
    });
});
