import schemaSingleGhg from "../../../src/routes/structure/ghgData/singleGhg/basicCRUD/schema";

const singleSuccess = {
    ghgName: "co2",
    emissionPerKG: 10
}

const singleFail = {
    ghgName: "co2",
    emissionPerKG: "10"
}

describe('Schema test: test if singleGhg schemas work as expected', () => {
    //success
    test('test singleGhg validator for login: success', () => {
        expect(schemaSingleGhg.createNew.validate(singleSuccess)).toBeTruthy();
    });

    //failure
    test('expect fail: wrong datatype', () => {
        const result = schemaSingleGhg.createNew.validate(singleFail);
        expect(result).rejects;
        expect(result).toMatchObject;
    });
});
