import schemaReasonGhg from "../../../src/routes/structure/ghgData/reasonOfGHGEmission/basicCRUD/schema";

const reasonSuccess = {
    ghgName: "co2",
    reasonTitle: "longer",
    description: "Lorem Ipsum dolr sit amet."
}

const reasonFail = {
    ghgName: "co2",
    reasonTitle: "sh",
    description: "Lorem Ipsum dolr sit amet."
}

describe('Schema test: test if reasonGhg schemas work as expected', () => {
    //success
    test('test reasonGhg validator for login: success', () => {
        expect(schemaReasonGhg.createNew.validate(reasonSuccess)).toBeTruthy();
    });

    //failure
    test('expect fail: reasonTitle too short', () => {
        const result = schemaReasonGhg.createNew.validate(reasonFail);
        expect(result).rejects;
        expect(result).toMatchObject;
    });
});
