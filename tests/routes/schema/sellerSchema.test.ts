import schemaSeller from "../../../src/routes/structure/seller/basicCRUD/schema";

const sellerSuccess = {
    name: "test name",
    link: "https://validLink.de",
}

const sellerFail = {
    name: "test name",
    link: "s",
}


describe('Schema test: test if seller schemas work as expected', () => {
    //success
    test('test seller validator for login: success', () => {
        expect(schemaSeller.createNew.validate(sellerSuccess)).toBeTruthy();
    });

    //failure
    test('expect fail: link too short', () => {
        const result = schemaSeller.createNew.validate(sellerFail);
        expect(result).rejects;
        expect(result).toMatchObject;
    });
});
