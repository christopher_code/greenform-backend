import schemaAccess from "../../../src/routes/structure/user/access/schema";

const userLoginSuccess = {
    email: "test@test.de",
    password: "password",
}

const userSignupSuccess = {
    email: "test@te",
    password: "password",
    name: "name",
    company: "Test GmbH"
}

const userLoginEmailMissing = {
    password: "password",
}

describe('Schema test: test if access schemas work as expected', () => {
    //success
    test('test access validator for login: success', () => {
        expect(schemaAccess.login.validate(userLoginSuccess)).toBeTruthy();
    });

    test('test access validator for signup: success', () => {
    expect(schemaAccess.signup.validate(userSignupSuccess)).toBeTruthy();
    });

    //failure
    test('expect fail: use signup schema instead of login', () => {
        const result = schemaAccess.login.validate(userSignupSuccess);
        expect(result).rejects;
        expect(result).toMatchObject;
    });

    test('expect fail: use login schema instead of signup', () => {
        const result = schemaAccess.signup.validate(userLoginSuccess);
        expect(result).rejects;
        expect(result).toMatchObject;
    });

    test('expect fail: email missing', () => {
        const result = schemaAccess.login.validate(userLoginEmailMissing);
        expect(result).toHaveProperty('error');
    });
});
