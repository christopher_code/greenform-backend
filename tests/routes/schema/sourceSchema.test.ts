import schemaSource from "../../../src/routes/structure/source/schema";

const sourceSuccess = {
    title: "test name",
    link: "https://validLink.de",
    description: "Lorem ipsum dolr sit amet."
}

const sourceFail = {
    title: 23,
    link: "https://validLink.de",
    description: "Lorem ipsum dolr sit amet."
}


describe('Schema test: test if source schemas work as expected', () => {
    //success
    test('test source validator for login: success', () => {
        expect(schemaSource.createNew.validate(sourceSuccess)).toBeTruthy();
    });

    //failure
    test('expect fail: wrong data type', () => {
        const result = schemaSource.createNew.validate(sourceFail);
        expect(result).rejects;
        expect(result).toMatchObject;
    });
});
