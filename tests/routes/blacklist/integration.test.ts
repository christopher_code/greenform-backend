jest.resetAllMocks();

import supertest from 'supertest';
import app, { neogma } from '../../../src/app';
import GeneralService from '../../../src/database/entityService/GeneralService';
import { UsersPropertiesI } from "../../../src/database/models/Users";
import bcrypt from 'bcrypt';
import { addHeaders } from "../access/login/integration.test"; 
import { addAuthHeaders } from "../access/auth/mock";

export const bcryptCompareSpy = jest.spyOn(bcrypt, 'compare');
export const userFindByEmailSpy = jest.spyOn(GeneralService, 'findNodeByEmail');
const EMAIL = 'test@mail.de';
const NAMEUSER = 'Test User';
const PASSWORD = 'password123';
const COMPANY = 'Test Comp';
const SOURCETITLE = "Source Title";
const SOURCELINK = "https://validLink.de";
const SOURCEDESCRIPTION = 'Source Description 12121212121212121212';
const PRODUCTNAME = 'Product Name';
const DESCRIPTIONPRODUCT = "Product Description 121212121212121212";
const LINKTOBUY = "https://validLink.de";
const COSTPERPRODUCT = 12;


describe('Integration: Token Blacklisting', () => {
  const endpointSignUp = '/api/signup/user';
  const endpointLogin = '/api/login/user';
  const endpointCreateProduct = '/api/product/new/';
  const endpointBlacklist = '/api/blacklist/add/';
  const request = supertest(app);
  let user: UsersPropertiesI;
  let userId: string;
  let token: string;

  beforeAll(async () => {
    user = await addHeaders(
        request.post(endpointSignUp).send({
          email: EMAIL,
          password: PASSWORD,
          company: COMPANY,
          name: NAMEUSER,
        }),
      );
    const responseLogin = await addHeaders(
        request.post(endpointLogin).send({
        email: EMAIL,
        password: PASSWORD,
        }),
    );
    userId = responseLogin.body.data.id;
    token = responseLogin.body.data.token;
    setTimeout(function(){ let a = 1; }, 2000);
  });

  afterAll(async () => {
    await neogma.queryRunner.run('match (n) detach delete n');
  });

  beforeEach(() => {
    userFindByEmailSpy.mockClear();
    bcryptCompareSpy.mockClear();
  });

  it('Create Product with newly created token', async () => {
      const responseProductCreation = await addAuthHeaders(
          request.post(endpointCreateProduct + userId).send({
              sourceTitle: SOURCETITLE,
              sourceLink: SOURCELINK,
              sourceDescription: SOURCEDESCRIPTION,
              name: PRODUCTNAME,
              description: DESCRIPTIONPRODUCT,
              linkToBuy: LINKTOBUY,
              costPerProduct: COSTPERPRODUCT,
          }),
          token
        );
      
      expect(responseProductCreation.status).toBe(200);
      expect(responseProductCreation.body.data.id).toBeDefined(); 
     
  });

  it('Error: Blacklist token and try to run product creation again', async () => {
    await addHeaders(
        request.post(endpointBlacklist + token).send({}),
      );

    const responseProductCreation = await addAuthHeaders(
        request.post(endpointCreateProduct + userId).send({
            sourceTitle: SOURCETITLE+'1',
            sourceLink: SOURCELINK+'1',
            sourceDescription: SOURCEDESCRIPTION+'1',
            name: PRODUCTNAME+'1',
            description: DESCRIPTIONPRODUCT+'1',
            linkToBuy: LINKTOBUY+'1',
            costPerProduct: COSTPERPRODUCT,
        }),
        token
      );
    
    expect(responseProductCreation.status).toBe(403);
  });

});