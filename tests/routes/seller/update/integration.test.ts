jest.resetAllMocks();

import supertest from 'supertest';
import app, { neogma } from '../../../../src/app';
import GeneralService from '../../../../src/database/entityService/GeneralService';
import { UsersPropertiesI } from "../../../../src/database/models/Users";
import bcrypt from 'bcrypt';
import { addHeaders } from "../../access/login/integration.test"; 
import { addAuthHeaders } from "../../access/auth/mock";

export const bcryptCompareSpy = jest.spyOn(bcrypt, 'compare');
export const userFindByEmailSpy = jest.spyOn(GeneralService, 'findNodeByEmail');
const EMAIL = 'test@mail.de';
const NAMEUSER = 'Test User';
const PASSWORD = 'password123';
const COMPANY = 'Test Comp';
const SOURCETITLE = "Source Title";
const SOURCELINK = "https://validLink.de";
const SOURCEDESCRIPTION = 'Source Description 12121212121212121212';
const PRODUCTNAME = 'Product Name';
const DESCRIPTIONPRODUCT = "Product Description 121212121212121212";
const LINKTOBUY = "https://validLink.de";
const COSTPERPRODUCT = 12;
const SELLERNAME = "Seller des Namens"
const SELLERLINK = 'kttps.link.de'

describe('Integration: Test signup and login combined with product creation', () => {
  const endpointSignUp = '/api/signup/user';
  const endpointLogin = '/api/login/user';
  const endpointCreateProduct = '/api/product/new/'
  const endpointCreateSeller = '/api/seller/new/'
  const endpointUpdateSeller = '/api/seller/update/fields/'
  const request = supertest(app);
  let user: UsersPropertiesI;
  let userId: string;
  let token: string;
  let userRole: string;

  beforeAll(async () => {
    user = await addHeaders(
        request.post(endpointSignUp).send({
          email: EMAIL,
          password: PASSWORD,
          company: COMPANY,
          name: NAMEUSER,
        }),
      );
    const responseLogin = await addHeaders(
        request.post(endpointLogin).send({
        email: EMAIL,
        password: PASSWORD,
        }),
    );
    userId = responseLogin.body.data.id;
    token = responseLogin.body.data.token;
    userRole = responseLogin.body.data.role;
    setTimeout(function(){ let a = 1; }, 2000);
  });

  afterAll(async () => {
    await neogma.queryRunner.run('match (n) detach delete n');
  });

  beforeEach(() => {
    userFindByEmailSpy.mockClear();
    bcryptCompareSpy.mockClear();
  });

  it('Create Seller after product creation and update fields', async () => {
    const responseProductCreation = await addAuthHeaders(
        request.post(endpointCreateProduct + userId).send({
            sourceTitle: SOURCETITLE,
            sourceLink: SOURCELINK,
            sourceDescription: SOURCEDESCRIPTION,
            name: PRODUCTNAME,
            description: DESCRIPTIONPRODUCT,
            linkToBuy: LINKTOBUY,
            costPerProduct: COSTPERPRODUCT,
        }),
        token
      );

    expect(responseProductCreation.status).toBe(200);

    const responseSellerCreation = await addAuthHeaders(
        request.post(endpointCreateSeller + userId + '/' + responseProductCreation.body.data.id).send({
            sourceTitle: SOURCETITLE + '1',
            sourceLink: 'https://validLink.de',
            sourceDescription: SOURCEDESCRIPTION + '1',
            name: SELLERNAME,
            link: 'https://validLink.de',
        }),
        token
      );
    
    expect(responseSellerCreation.status).toBe(200);

    const responseSellerUpdate = await addAuthHeaders(
        // comment: check weather responseSellerCreation.body.data.id is correct
        request.put(endpointUpdateSeller + responseSellerCreation.body.data.id + '/'+ userId + '/' + responseProductCreation.body.data.id).send({
            sourceTitle: SOURCETITLE + '2',
            sourceLink: SOURCELINK,
            sourceDescription: SOURCEDESCRIPTION + '2',
            link: 'https://validLink.de',
        }),
        token
      );

      expect(responseSellerUpdate.status).toBe(200);
      expect(responseSellerUpdate.body.message).toMatch(/Success/i);
      expect(responseSellerUpdate.body.data).toBeDefined();
  });

  it('Expect Error: Create Product and seller after login and use wrong field while updating', async () => {
    const responseProductCreation = await addAuthHeaders(
        request.post(endpointCreateProduct + userId).send({
            sourceTitle: SOURCETITLE,
            sourceLink: SOURCELINK,
            sourceDescription: SOURCEDESCRIPTION,
            name: 'other name',
            description: DESCRIPTIONPRODUCT,
            linkToBuy: LINKTOBUY,
            costPerProduct: COSTPERPRODUCT,
        }),
        token
      );

    expect(responseProductCreation.status).toBe(200);

    const responseSellerCreation = await addAuthHeaders(
        request.post(endpointCreateSeller + userId + '/' + responseProductCreation.body.data.id).send({
            sourceTitle: SOURCETITLE + '3',
            sourceLink: 'https://validLink.de',
            sourceDescription: SOURCEDESCRIPTION + '3',
            name: SELLERNAME + '1',
            link: 'https://validLink.de',
        }),
        token
      );
    
    expect(responseSellerCreation.status).toBe(200);

    const responseSellerUpdate = await addAuthHeaders(
        request.put(endpointUpdateSeller + responseProductCreation.body.data.id + '/'+ userId  + '/' + responseProductCreation.body.data.id).send({
            sourceTitle: SOURCETITLE + '2',
            sourceLink: SOURCELINK + '2',
            sourceDescription: SOURCEDESCRIPTION + '2',
            name: 'Is nicht',
        }),
        token
      );

      expect(responseSellerUpdate.status).toBe(406);
  });
});