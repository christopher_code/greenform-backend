import supertest from 'supertest';
import app from "../../../../src/app";
import { addHeaders } from '../../access/login/integration.test'
import { addAuthHeaders } from "../../access/auth/mock";
import { testJWT } from "../../../../config";

const Seller_Link = "htpps://sellerLink.com";
const userId = "123";
const productId = '1234567890';
const SOURCE_TITLE = 'Seller Title Test Source';
const SOURCE_LINK = 'https://source.link.seller';
const SOURCE_DESCRIPTION = 'SOURCE Seller description lorem ipsum dolr sit amet';

describe('Create Seller route', () => {
  const endpoint = `/api/seller/new/${userId}/${productId}`;
  const request = supertest(app);

  it('No seller name data provided', async () => {
    const response = await addAuthHeaders(
      request.post(endpoint).send({
        sourceTitle: SOURCE_TITLE,
        sourceLink: SOURCE_LINK,
        sourceDescription: SOURCE_DESCRIPTION,
        link: Seller_Link
      }), 
      testJWT
    );

    expect(response.status).toBe(406);

    expect(response.body).toHaveProperty('message');

    expect(response.body).not.toHaveProperty('data');
  });
});
