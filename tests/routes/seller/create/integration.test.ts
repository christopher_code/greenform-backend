jest.resetAllMocks();

import supertest from 'supertest';
import app, { neogma } from '../../../../src/app';
import GeneralService from '../../../../src/database/entityService/GeneralService';
import { UsersPropertiesI } from "../../../../src/database/models/Users";
import bcrypt from 'bcrypt';
import { addHeaders } from "../../access/login/integration.test"; 
import { addAuthHeaders } from "../../access/auth/mock";

export const bcryptCompareSpy = jest.spyOn(bcrypt, 'compare');
export const userFindByEmailSpy = jest.spyOn(GeneralService, 'findNodeByEmail');
const EMAIL = 'test1@mai4l.de';
const NAMEUSER = 'Test14 User';
const PASSWORD = 'passw4ord1123';
const COMPANY = 'Test C4o1mp';
const SOURCETITLE = "So4u1rce Title";
const SOURCELINK = "https://validLink.de";
const SOURCEDESCRIPTION = 'Sour14ce Description 12121212121212121212';
const PRODUCTNAME = 'Product 1Na4me';
const DESCRIPTIONPRODUCT = "Pr1o4duct Description 121212121212121212";
const LINKTOBUY = "https://validLink.de";
const COSTPERPRODUCT = 121;
const SELLERNAME = "Aldi4 Süd";
const SELLERLINK = "http4s://aldi.de";

describe('Integration: Test seller', () => {
  const endpointSignUp = '/api/signup/user';
  const endpointLogin = '/api/login/user';
  const endpointCreateProduct = '/api/product/new/'
  const endpointCreateSeller = '/api/seller/new/'
  const request = supertest(app);
  let user: UsersPropertiesI;
  let userId: string;
  let token: string;
  let productId: string;

  beforeAll(async () => {
    user = await addHeaders(
        request.post(endpointSignUp).send({
          email: EMAIL,
          password: PASSWORD,
          company: COMPANY,
          name: NAMEUSER,
        }),
      );

    const responseLogin = await addHeaders(
        request.post(endpointLogin).send({
        email: EMAIL,
        password: PASSWORD,
        }),
    );
    userId = responseLogin.body.data.id;
    token = responseLogin.body.data.token;
    setTimeout(function(){ let a = 1; }, 2000);
  });

  afterAll(async () => {
    await neogma.queryRunner.run('match (n) detach delete n');
  });

  beforeEach(() => {
    userFindByEmailSpy.mockClear();
    bcryptCompareSpy.mockClear();
  });

  it('Create Seller after product creation', async () => {
    const responseProductCreation = await addAuthHeaders(
        request.post(endpointCreateProduct + userId).send({
            sourceTitle: SOURCETITLE,
            sourceLink: SOURCELINK,
            sourceDescription: SOURCEDESCRIPTION,
            name: PRODUCTNAME,
            description: DESCRIPTIONPRODUCT,
            linkToBuy: LINKTOBUY,
            costPerProduct: COSTPERPRODUCT,
        }),
        token
      );

    expect(responseProductCreation.status).toBe(200);

    const responseSellerCreation = await addAuthHeaders(
        request.post(endpointCreateSeller + userId + '/' + responseProductCreation.body.data.id).send({
            sourceTitle: SOURCETITLE + '1',
            sourceLink: SOURCELINK + '1',
            sourceDescription: SOURCEDESCRIPTION + '1',
            name: SELLERNAME,
            link: SELLERLINK,
        }),
        token
      );
    
    expect(responseSellerCreation.status).toBe(200);
    expect(responseSellerCreation.body.message).toMatch(/Success/i);
    expect(responseSellerCreation.body.data).toBeDefined();
  });

  it('Error: Create Sekker with same product name', async () => {
    const responseProductCreation = await addAuthHeaders(
        request.post(endpointCreateProduct + userId).send({
            sourceTitle: SOURCETITLE,
            sourceLink: SOURCELINK,
            sourceDescription: SOURCEDESCRIPTION,
            name: PRODUCTNAME,
            description: DESCRIPTIONPRODUCT,
            linkToBuy: LINKTOBUY,
            costPerProduct: COSTPERPRODUCT,
        }),
        token
      );

    await addAuthHeaders(
        request.post(endpointCreateSeller + userId + '/' + responseProductCreation.id).send({
            sourceTitle: SOURCETITLE + '2',
            sourceLink: SOURCELINK + '2',
            sourceDescription: SOURCEDESCRIPTION + '2',
            name: "TEst anem",
            link: SELLERLINK,
        }),
        token
      );

      const responseSellerCreation = await addAuthHeaders(
        request.post(endpointCreateSeller + userId + '/' + productId).send({
            sourceTitle: SOURCETITLE + '3',
            sourceLink: SOURCELINK + '2',
            sourceDescription: SOURCEDESCRIPTION + '2',
            name: 'TEst anem',
            link: SELLERLINK,
        }),
        token
      );
    
    expect(responseSellerCreation.status).toBe(500);
    expect(responseSellerCreation.body.data).not.toBeDefined();
  });

  it('Error: Create Product without user id', async () => {
      const responseProductCreation = await addAuthHeaders(
        request.post(endpointCreateProduct + userId).send({
            sourceTitle: SOURCETITLE,
            sourceLink: SOURCELINK,
            sourceDescription: SOURCEDESCRIPTION,
            name: PRODUCTNAME,
            description: DESCRIPTIONPRODUCT,
            linkToBuy: LINKTOBUY,
            costPerProduct: COSTPERPRODUCT,
        }),
        token
      );

    const responseSellerCreation = await addAuthHeaders(
        request.post(endpointCreateSeller + 'test' + '/' + responseProductCreation.id).send({
            sourceTitle: SOURCETITLE,
            sourceLink: SOURCELINK,
            sourceDescription: SOURCEDESCRIPTION,
            name: "TEst anem",
            link: SELLERLINK,
        }),
        token
      );
    
    expect(responseSellerCreation.status).toBe(500);
    expect(responseSellerCreation.body.data).not.toBeDefined();
  });

});