jest.resetAllMocks();

import supertest from 'supertest';
import app, { neogma } from '../../../../src/app';
import GeneralService from '../../../../src/database/entityService/GeneralService';
import bcrypt from 'bcrypt';
import { addHeaders } from "../../access/login/integration.test"; 
import { addAuthHeaders } from "../../access/auth/mock";

export const bcryptCompareSpy = jest.spyOn(bcrypt, 'compare');
export const userFindByEmailSpy = jest.spyOn(GeneralService, 'findNodeByEmail');
const EMAIL = 'test@mail.de';
const NAMEUSER = 'Test User';
const PASSWORD = 'password123';
const COMPANY = 'Test Comp';
const SOURCETITLE = "Source Title";
const SOURCELINK = "https://validLink.de";
const SOURCEDESCRIPTION = 'Source Description 12121212121212121212';
const PRODUCTNAME = 'Product Name';
const DESCRIPTIONPRODUCT = "Product Description 121212121212121212";
const LINKTOBUY = "https://validLink.de";
const COSTPERPRODUCT = 12;

describe('Integration: Read source data', () => {
  const endpointSignUp = '/api/signup/user';
  const endpointLogin = '/api/login/user';
  const endpointCreateProduct = '/api/product/new/';
  const endpointReadRandom = '/api/o/source/get/random/product/superuser/product';
  const request = supertest(app);
  let userId: string;
  let token: string;
  let productId: string;
  let sourceId: string;

  beforeAll(async () => {
    await addHeaders(
        request.post(endpointSignUp).send({
          email: EMAIL,
          password: PASSWORD,
          company: COMPANY,
          name: NAMEUSER,
        }),
      );
    const responseLogin = await addHeaders(
        request.post(endpointLogin).send({
        email: EMAIL,
        password: PASSWORD,
        }),
    );
    userId = responseLogin.body.data.id;
    token = responseLogin.body.data.token;

    const responseProductCreation = await addAuthHeaders(
        request.post(endpointCreateProduct + userId).send({
            sourceTitle: SOURCETITLE,
            sourceLink: SOURCELINK,
            sourceDescription: SOURCEDESCRIPTION,
            name: PRODUCTNAME,
            description: DESCRIPTIONPRODUCT,
            linkToBuy: LINKTOBUY,
            costPerProduct: COSTPERPRODUCT,
        }),
        token
      );

      await addAuthHeaders(
        request.post(endpointCreateProduct + userId).send({
            sourceTitle: SOURCETITLE + 'a',
            sourceLink: SOURCELINK + 'a',
            sourceDescription: SOURCEDESCRIPTION + 'a',
            name: PRODUCTNAME + 'a',
            description: DESCRIPTIONPRODUCT + 'a',
            linkToBuy: LINKTOBUY + 'a',
            costPerProduct: COSTPERPRODUCT + 'a',
        }),
        token
      );

      await addAuthHeaders(
        request.post(endpointCreateProduct + userId).send({
            sourceTitle: SOURCETITLE + 'b',
            sourceLink: SOURCELINK + 'ab',
            sourceDescription: SOURCEDESCRIPTION + 'ab',
            name: PRODUCTNAME + 'ab',
            description: DESCRIPTIONPRODUCT + 'ab',
            linkToBuy: LINKTOBUY + 'ab',
            costPerProduct: COSTPERPRODUCT + 'ab',
        }),
        token
      );

      await addAuthHeaders(
        request.post(endpointCreateProduct + userId).send({
            sourceTitle: SOURCETITLE + 'ac',
            sourceLink: SOURCELINK + 'ac',
            sourceDescription: SOURCEDESCRIPTION + 'ac',
            name: PRODUCTNAME + 'ac',
            description: DESCRIPTIONPRODUCT + 'ac',
            linkToBuy: LINKTOBUY + 'ac',
            costPerProduct: COSTPERPRODUCT + 'ac',
        }),
        token
      );

    productId = responseProductCreation.body.data.id;
    sourceId = responseProductCreation.body.data.sourceId;
  });

  afterAll(async () => {
    await neogma.queryRunner.run('match (n) detach delete n');
  });

  beforeEach(() => {
    userFindByEmailSpy.mockClear();
    bcryptCompareSpy.mockClear();
  });

  it('Error: Read random data of source (just 1 existing)', async () => {
    let random = await addHeaders(
        request.get(endpointReadRandom),
      );
    
    expect(random.status).toBe(500);
  });

});