import supertest from 'supertest';
import app from "../../../../src/app";
import { addHeaders } from '../../access/login/integration.test'
import { addAuthHeaders } from "../../access/auth/mock";
import { testJWT } from "../../../../config";

import {
    mockProductFindById,
    PRODUCT_NAME,
    USER_ID,
    SOURCE_TITLE,
    SOURCE_DESCRIPTION,
    SOURCE_LINK
  } from '../mock';

describe('Create Product route', () => {
  const endpoint = `/api/product/new/${USER_ID}`;
  const request = supertest(app);
  
  beforeEach(() => {
    mockProductFindById.mockClear();
  });

  it('Authorized Access and successful creation of product', async () => {
    const response = await addAuthHeaders(
      request.post(endpoint).send({
        sourceTitle: SOURCE_TITLE,
        sourceLink: SOURCE_LINK,
        sourceDescription: SOURCE_DESCRIPTION,
        name: PRODUCT_NAME,
        description: "Test Test Test Test Test",
        linkToBuy: "https://link.de",
        costPerProduct: 12,
      }), 
      testJWT
    );

    expect(response.status).toBe(200);
    expect(response.body.statusCode).toBe("10000");

    expect(response.body).toHaveProperty('message');
    expect(response.body).toHaveProperty('statusCode');

    expect(response.body).toHaveProperty('data');
    expect(response.body.data.product.dataValues.name).toEqual(PRODUCT_NAME);
  });
});


