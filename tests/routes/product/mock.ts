type ProductInterface = {
    id: string;
    name: string;
    description?: string;
    linkToBuy?: string;
    createdAt?: number;
};

export const PRODUCT_NAME = 'Test Product';
export const PRODUCT_ID = '1234567890';
export const USER_ID = '1234567890';
export const SOURCE_TITLE = 'Title Test Source';
export const SOURCE_LINK = 'https://validLink.de';
export const SOURCE_DESCRIPTION = 'SOURCE description lorem ipsum dolr sit amet';

export const mockProductFindById = jest.fn(
  async (id: string): Promise<ProductInterface | null> => {
    if (id === PRODUCT_ID)
      return {
        id: PRODUCT_ID,
        sourceTitle: SOURCE_TITLE,
        sourceLink: SOURCE_LINK,
        sourceDescription: SOURCE_DESCRIPTION,
        name: PRODUCT_NAME,
        description: 'Lorem Ipsum dolre sit amet.',
        linkToBuy: 'https://validLink.de',
        createdAt: 123456789,
      } as ProductInterface;
    return null;
  },
);

jest.mock('../../../src/database/entityService/ProductService.ts', () => ({
  get findProductById() {
    return mockProductFindById;
  },
}));