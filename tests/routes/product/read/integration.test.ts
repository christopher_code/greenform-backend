jest.resetAllMocks();

import supertest from 'supertest';
import app, { neogma } from '../../../../src/app';
import GeneralService from '../../../../src/database/entityService/GeneralService';
import bcrypt from 'bcrypt';
import { addHeaders } from "../../access/login/integration.test"; 
import { addAuthHeaders } from "../../access/auth/mock";

export const bcryptCompareSpy = jest.spyOn(bcrypt, 'compare');
export const userFindByEmailSpy = jest.spyOn(GeneralService, 'findNodeByEmail');
const EMAIL = 'test@mail.de';
const NAMEUSER = 'Test User';
const PASSWORD = 'password123';
const COMPANY = 'Test Comp';
const SOURCETITLE = "Source Title";
const SOURCELINK = "https://validLink.de";
const SOURCEDESCRIPTION = 'Source Description 12121212121212121212';
const PRODUCTNAME = 'Product Name';
const DESCRIPTIONPRODUCT = "Product Description 121212121212121212";
const LINKTOBUY = "https://validLink.de";
const COSTPERPRODUCT = 12;

describe('Integration: Read product data', () => {
  const endpointSignUp = '/api/signup/user';
  const endpointLogin = '/api/login/user';
  const endpointCreateProduct = '/api/product/new/';
  const endpointReadCore = '/api/o/product/get/core/';
  const endpointReadFull = '/api/o/product/get/full/';
  const endpointReadRandom = '/api/o/product/get/random';
  const endpointCreateSeller = '/api/seller/new/'
  const request = supertest(app);
  let userId: string;
  let token: string;
  let productId: string;

  beforeAll(async () => {
    await addHeaders(
        request.post(endpointSignUp).send({
          email: EMAIL,
          password: PASSWORD,
          company: COMPANY,
          name: NAMEUSER,
        }),
      );
    const responseLogin = await addHeaders(
        request.post(endpointLogin).send({
        email: EMAIL,
        password: PASSWORD,
        }),
    );
    userId = responseLogin.body.data.id;
    token = responseLogin.body.data.token;

    const responseProductCreation = await addAuthHeaders(
        request.post(endpointCreateProduct + userId).send({
            sourceTitle: SOURCETITLE,
            sourceLink: SOURCELINK,
            sourceDescription: SOURCEDESCRIPTION,
            name: PRODUCTNAME,
            description: DESCRIPTIONPRODUCT,
            linkToBuy: LINKTOBUY,
            costPerProduct: COSTPERPRODUCT,
        }),
        token
      );

    productId = responseProductCreation.body.data.id;
  });

  afterAll(async () => {
    await neogma.queryRunner.run('match (n) detach delete n');
  });

  beforeEach(() => {
    userFindByEmailSpy.mockClear();
    bcryptCompareSpy.mockClear();
  });

  it('Error: Read random data of product (just 1 existing)', async () => {
    let random = await addHeaders(
        request.get(endpointReadRandom).send({}),
      );
    
    expect(random.status).toBe(500);
  });

  it('Read core data', async () => {
    let core = await addHeaders(
        request.get(endpointReadCore + productId).send({}),
      );
    
    expect(core.status).toBe(200);
    expect(core.body.message).toMatch(/Success/i);
  });


  it('Read core data', async () => {
    let core = await addHeaders(
        request.get(endpointReadCore + '123').send({}),
      );
    
    expect(core.status).toBe(500);
  });

  it('Read full data', async () => {
    let full = await addHeaders(
        request.get(endpointReadFull + productId).send({}),
      );
    
    expect(full.status).toBe(200);
    expect(full.body.message).toMatch(/Success/i);
  });

  it('Read full data updated', async () => {
    const responseSellerCreation = await addAuthHeaders(
        request.post(endpointCreateSeller + userId + '/' + productId).send({
            sourceTitle: SOURCETITLE + '1',
            sourceLink: SOURCELINK,
            sourceDescription: SOURCEDESCRIPTION + '1',
            name: "Seller 1",
            link: "https://validLink.de",
        }),
        token
      );
    
    expect(responseSellerCreation.status).toBe(200);

    let full = await addHeaders(
        request.get(endpointReadFull + productId).send({}),
      );
    
    expect(full.status).toBe(200);
    expect(full.body.message).toMatch(/Success/i);
    expect(full.body.data).toBeDefined();
    expect(full.body.data.seller).toBeDefined();
    expect(full.body.data.seller[0].id).toEqual(responseSellerCreation.body.data.id);
  });

  it('Error: Read full data updated wrong id', async () => {
    const responseSellerCreation = await addAuthHeaders(
        request.post(endpointCreateSeller + userId + '/' + productId).send({
            sourceTitle: SOURCETITLE + '2',
            sourceLink: SOURCELINK,
            sourceDescription: SOURCEDESCRIPTION + '2',
            name: "Seller 2",
            link: "https://validLink.de",
        }),
        token
      );
    
    expect(responseSellerCreation.status).toBe(200);

    let full = await addHeaders(
        request.get(endpointReadFull + '123').send({}),
      );
    
    expect(full.status).toBe(500);
  });

});