import { generateDate } from "../../src/middleware/generateDate";

const date1 = generateDate();

const date2 = setTimeout(generateDate, 1500);

describe('Test for date generator:', () => {

    test('Expect date to be created', () => {
        expect(typeof date1).toBe("number");
    });

    test('Expect two dates to be different', () => {
        expect(date1).not.toEqual(date2);
    });
});


