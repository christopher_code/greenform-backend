jest.resetAllMocks();

import supertest from 'supertest';
import app, { neogma } from '../../src/app';
import { addAuthHeaders } from "../routes/access/auth/mock";
import { addHeaders } from "../routes/access/login/integration.test"; 
import { UsersPropertiesI } from '../../src/database/models/Users';
import GeneralService from '../../src/database/entityService/GeneralService';
import bcrypt from 'bcrypt';

export const bcryptCompareSpy = jest.spyOn(bcrypt, 'compare');
export const userFindByEmailSpy = jest.spyOn(GeneralService, 'findNodeByEmail');

describe('Integratio: Verify token', () => {
    const request = supertest(app);
    const EMAIL = 'test@mail.de';
    const NAMEUSER = 'Test User';
    const PASSWORD = 'password123';
    const COMPANY = 'Test Comp';
    const SOURCETITLE = "Source Title";
    const SOURCELINK = "https://validLink.de";
    const SOURCEDESCRIPTION = 'Source Description 12121212121212121212';
    const PRODUCTNAME = 'Product Name';
    const DESCRIPTIONPRODUCT = "Product Description 121212121212121212";
    const LINKTOBUY = "https://validLink.de";
    const COSTPERPRODUCT = 12;

    let user: UsersPropertiesI;
    let userId: string;
    let token: string;
    let userRole: string;

    beforeAll(async () => {
        const endpointSignUp = '/api/signup/user';
        const endpointLogin = '/api/login/user';
        user = await addHeaders(
            request.post(endpointSignUp).send({
              email: EMAIL,
              password: PASSWORD,
              company: COMPANY,
              name: NAMEUSER,
            }),
          );
        const responseLogin = await addHeaders(
            request.post(endpointLogin).send({
            email: EMAIL,
            password: PASSWORD,
            }),
        );
        userId = responseLogin.body.data.id;
        token = responseLogin.body.data.token;
        userRole = responseLogin.body.data.role;
        setTimeout(function(){ let a = 1; }, 2000);
      });

      afterAll(async () => {
        await neogma.queryRunner.run('match (n) detach delete n');
      });

      beforeEach(() => {
        userFindByEmailSpy.mockClear();
        bcryptCompareSpy.mockClear();
      });

      it('Verify token with correct token', async () => {
        const responseProductCreation = await addAuthHeaders(
            request.post('/api/product/new/' + userId).send({
              sourceTitle: SOURCETITLE,
              sourceLink: SOURCELINK,
              sourceDescription: SOURCEDESCRIPTION,
              name: PRODUCTNAME,
              description: DESCRIPTIONPRODUCT,
              linkToBuy: LINKTOBUY,
              costPerProduct: COSTPERPRODUCT,
            }),
            token
          );
        
        expect(responseProductCreation.status).toBe(200);
      });
});
