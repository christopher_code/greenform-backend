import { string } from "@hapi/joi";
import { generateUniqeuId } from "../../src/middleware/generateUuid";

const uuid1 = generateUniqeuId();
const uuid2 = generateUniqeuId();

describe('Create unique ids with uuid', () => {

    test('Expect uuid to be created', () => {
        expect(typeof uuid1).toBe("string");
    });

    test('Expect uuids to be different', () => {
        expect(uuid1).not.toEqual(uuid2);
    });
});
