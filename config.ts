require('dotenv').config()

export const environment = process.env.NODE_ENV;

export const port = process.env.PORT;

export const db = {
    db_url: process.env.DATABASE_URL || '',
    db_user: process.env.DATABASE_USER || '',
    db_pw: process.env.DATABASE_PW || ''
  };

export const logDirectory = process.env.LOG_DIR;

export const corsUrl = process.env.CORS_URL;

export const authToken: any = process.env.AUTH_TOKEN;
export const adminToken: any = process.env.AUTH_TOKEN_ADMIN;
export const adminMail: any = process.env.AUTH_MAIL;
export const adminJWT: any = process.env.ADMIN_JWT;

export const testJWT: any = process.env.TEST_JWT;

export const publicKey: any = process.env.PUBLIC_KEY;