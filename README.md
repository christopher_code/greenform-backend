# Greenformed Approval Backend

Approval backend of Greenformed, the global database for GHG data. Managed by the public for the public.
The data is managed in this backend and approved by users. Once approved it is transferred from this backend to the Single Source of Truth (SST) backend.

---
## Requirements

For development, you will need Node.js and npm. 
The dependency installation is explained in section "Getting Started".

To run this backend application, two seperate databse connections are requrired: 
Go on [official Neo$J AURA website](https://neo4j.com/cloud/aura/) to create the Neo4j Aura databases.
The following database connections are requried: 
- Approval Database 
- Test Approval Database

In case you want to have prefilled databases, please get in contact with me so that all functionalities of the application work as expected. You can also run the application with empty databases. However, the application needs a minimum amount of nodes to display all information correctly. 

## Enviornment Variables

To properly connect to the database the DATABSE_URL in the .env and .env.test files need to be adapted to the following format: 
    neo4j://your-url.databases.neo4j.io
Please also provide the correct DATABASE_USER and DATABASE_PW of the databases in the .env and .env.test files.

The PORT can be freely choosen, but needs to be different than the Port of the SST Backend and the Front End of this application.

The LOG_DIR is the folder directory the logs should be stored in (e.g.: /logs)

The CORS_URL specifies the origin urls of the [npm cors package](https://www.npmjs.com/package/cors). The default setting is *.

The AUTH_TOKEN is the secret key of the [JWT](https://jwt.io/introduction) of the Front End to Approval backend Connection and needs to be the same as in the front end.

The AUTH_TOKEN_ADMIN is the secret key of the [JWT](https://jwt.io/introduction) of the Approval Backeend to SST Backend and needs to be the same as in the SST Backend.

The AUTH_MAIL is the a parameter of the [JWT](https://jwt.io/introduction) of the Approval Backeend to SST Backend and needs to be the same as in the SST Backend in order to double check if the Backend is sending the request, or someone else.

The ADMIN_JWT is a pre-made [JWT](https://jwt.io/introduction) to sign the transactions between the SST Backend and the Approval Backend. 
To get the ADMIN_JWT user the following link: [click here](https://jwt.io/). To generate a valid token, please provide a secret key and copy it to the environment vartiables as AUTH_TOKEN_ADMIN. Also add theUserMail to the payload and add the same string as AUTH_MAIL to the environment variables. These exact variables also need to be added to the environment variables of the [official SST Backend](https://gitlab.com/christopher_code/greenformed-sst-backend). Once the token is created add the token as ADMIN_JWT to the environment variables.

The PUBLIC_KEY is the publci key of the proof of concept for the double encoded data. 
Please contact me and will provide you with a public key for the approval Backend and the private key for the SST backend.

The TEST_JWT is a [JWT](https://jwt.io/introduction), for the unit tests of this Backend, in order to reduce the number of JWTs that need to be created. Ypu can create it and copy it then from [the JWT sandbox](https://jwt.io/).

In order to access all capacities of the project locally, please also install and run the following backend (SST Backend): [official SST Backend](https://gitlab.com/christopher_code/greenformed-sst-backend)

### Node
- #### Node installation on Windows

  Just go on [official Node.js website](https://nodejs.org/) and download the installer.
Also, be sure to have `git` available in your PATH, `npm` might need it (You can find git [here](https://git-scm.com/)).

- #### Node installation on Ubuntu

  You can install nodejs and npm easily with apt install, just run the following commands.

      $ sudo apt install nodejs
      $ sudo apt install npm

- #### Other Operating Systems
  You can find more information about the installation on the [official Node.js website](https://nodejs.org/) and the [official NPM website](https://npmjs.org/).

If you need to update `npm`, you can make it using `npm`! After running the following command, just open again the command line.

    $ npm install npm -g

---

## Clone Project

    $ git clone https://gitlab.com/christopher_code/greenform-backend.git
    $ cd PROJECT_TITLE


## Getting Started Using Docker

1. build docker image: docker build -t <NAME_OF_IMAGE> .

2. run docker image: docker run --env-file .env -p 8000:8000 <NAME_OF_IMAGE>

## Getting Started Locally

    $ npm i 
    $ npm start

## Simple build for production

    $ npx tsc
    $ node build/server.js