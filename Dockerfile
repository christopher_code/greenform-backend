FROM node:14

USER node

RUN mkdir -p /home/node/app && chown -R node:node /home/node/app

WORKDIR /home/node/app

COPY package*.json ./

RUN npm install

COPY --chown=node:node . .

RUN npx tsc

ENV PORT=8000
EXPOSE 8000

CMD [ "node", "build/server.js" ]